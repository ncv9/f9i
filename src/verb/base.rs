use f9i_core::{
    assemblage::{
        AStem, Decorated, GlideToOnsetRef, ParsedWord, SimpleSuffixRef, SuffixRef,
        SyllabicToNuclear, Sylls,
    },
    mgc::{Coda, Consonant, Initial, Vowel},
};

use crate::morphophonology::gencons::GenCons;

/// Additional methods for [`Decorated<AStem>`] used for verb conjugation.
pub trait StemExt {
    /// Returns true if a ⟦ħ⟧ should be interleaved if `next_vowel` is appended according to conjugation rules.
    fn interleave_hst(&self, next_vowel: Vowel) -> bool;
    /// Appends a suffix, interleaving ⟦ħ⟧ if necessary.
    ///
    /// If the suffix ends with ⟦f⟧, then this is replaced with the proper phi-consonant for the stem.
    fn append_fin_suffix(self, suffix: SuffixRef) -> Decorated<ParsedWord>;
    /// Appends a vowel, interleaving ⟦ħ⟧ if necessary.
    fn append_fin_vowel(self, v: Vowel) -> Decorated<SyllabicToNuclear>;
    /// Appends a simple suffix, interleaving ⟦ħ⟧ if necessary.
    fn append_fin_ssuffix(self, suffix: SimpleSuffixRef) -> Decorated<Sylls>;
    /// Appends a glide-to-onset assemblage, interleaving ⟦ħ⟧ if necessary.
    fn append_fin_go(self, suffix: GlideToOnsetRef) -> Decorated<AStem>;

    /// Like [`StemExt::interlave_hst`], except that this method takes an optional vowel, returning false for `None`.
    fn interleave_hst_opt(&self, next_vowel: Option<Vowel>) -> bool {
        match next_vowel {
            Some(v) => self.interleave_hst(v),
            None => false,
        }
    }
}

impl StemExt for Decorated<AStem> {
    fn interleave_hst(&self, next_vowel: Vowel) -> bool {
        let last_tile = self.frag.last_tile();
        self.ends_with_empty_bridge() && last_tile.2.hat() == next_vowel.hat()
    }

    fn append_fin_suffix(mut self, suffix: SuffixRef) -> Decorated<ParsedWord> {
        let phi = GenCons::for_stem(&self);
        if self.interleave_hst_opt(suffix.get_vowel(0)) {
            self.frag.remainder.0 = Initial::Single(Consonant::Hst);
        }
        let mut form = self + suffix;
        if form.frag.remainder.3 == Coda::F {
            form.frag.remainder.3 = phi.into();
        }
        form
    }

    fn append_fin_ssuffix(mut self, suffix: SimpleSuffixRef) -> Decorated<Sylls> {
        if self.interleave_hst_opt(suffix.get_vowel(0)) {
            self.frag.remainder.0 = Initial::Single(Consonant::Hst);
        }
        self + suffix
    }

    fn append_fin_vowel(mut self, v: Vowel) -> Decorated<SyllabicToNuclear> {
        if self.interleave_hst(v) {
            self.frag.remainder.0 = Initial::Single(Consonant::Hst);
        }
        self + v
    }

    fn append_fin_go(mut self, suffix: GlideToOnsetRef) -> Decorated<AStem> {
        if self.interleave_hst_opt(suffix.get_vowel(0)) {
            self.frag.remainder.0 = Initial::Single(Consonant::Hst);
        }
        self + suffix
    }
}
