//! Conjugation of verbs in the finite and transfinite forms.
use f9i_core::{
    assemblage::{AStem, Decorated, ParsedWord, ParsedWordRef, SuffixRef, Sylls, SyllsRef},
    category::{
        unpack, Aspect, AxisType, ObjectType, Person, SubjectType, Tense, Transfinite, VNumber,
    },
    mgc::{
        Coda, Consonant, Eclipse, FusionConsonant, Initial, Lenite, Nucleus, Phrase, SimpleCoda,
        Vowel,
    },
};
use f9i_ivm_macro::{b, f, gg, ns, w, x, y};
use itertools::Itertools;

use crate::{
    error::InflectionError,
    morphophonology::{
        fuse::{fuse_d, fuse_with_eps_d},
        gencons::GenCons,
        v2f, v2x, vc2f, vc2x,
    },
    nd_array_type,
    nest::default_boxed_array,
    table::{Table, ToTable},
    verb::base::StemExt,
};

use super::{Material, ResinousIrregular, VerbForm, VitreousIrregular};

// We have to write this in such an ugly way instead of using nd_array_type! or rustc will refuse to apply derive macros.
/// Holds the finite forms of a verb.
pub type Fin<T> = [[[[T; SubjectType::COUNT]; ObjectType::COUNT]; Tense::COUNT]; Aspect::COUNT];
/// Holds the transfinite forms of a verb.
pub type Trf<T> = [[T; ObjectType::COUNT]; Transfinite::COUNT];

/// Helper trait for adding methods to [Fin].
pub trait FinAccess<T> {
    fn form(&self, decl: FiniteVerbCategories) -> &T;
    fn form_mut(&mut self, decl: FiniteVerbCategories) -> &mut T;
}

impl<T> FinAccess<T> for Fin<T> {
    fn form(&self, decl: FiniteVerbCategories) -> &T {
        &self[decl.aspect as usize][decl.tense as usize][decl.obj_marking as usize]
            [decl.subj_marking as usize]
    }
    fn form_mut(&mut self, decl: FiniteVerbCategories) -> &mut T {
        &mut self[decl.aspect as usize][decl.tense as usize][decl.obj_marking as usize]
            [decl.subj_marking as usize]
    }
}
/// Helper trait for adding methods to [Trf].
pub trait TrfAccess<T> {
    fn form(&self, decl: TransfiniteVerbCategories) -> &T;
    fn form_mut(&mut self, decl: TransfiniteVerbCategories) -> &mut T;
}

impl<T> TrfAccess<T> for Trf<T> {
    fn form(&self, decl: TransfiniteVerbCategories) -> &T {
        &self[decl.transfinite as usize][decl.obj_marking as usize]
    }
    fn form_mut(&mut self, decl: TransfiniteVerbCategories) -> &mut T {
        &mut self[decl.transfinite as usize][decl.obj_marking as usize]
    }
}

pub fn table_finite_forms<T, F>(fin: &Fin<T>, td_fin: &mut Vec<Option<Phrase>>, f: F)
where
    F: Fn(&T) -> Phrase,
{
    // fin: aspect tense person number
    //      2      2     4      4
    for tos in fin {
        for os in tos {
            let row = &os[0];
            let mut arr = vec![None; Person::COUNT * VNumber::COUNT];
            for (subj_type_ordinal, e) in IntoIterator::into_iter(row).enumerate() {
                let (p, n) =
                    SubjectType::from_ordinal(subj_type_ordinal as u8).to_person_and_number();
                arr[(p as usize) * VNumber::COUNT + (n as usize)] = Some(f(e));
            }
            td_fin.append(&mut arr);
        }
    }
}

impl ToTable for Fin<Phrase> {
    fn to_table(&self) -> Table<Option<Phrase>> {
        let mut table_storage = vec![];
        table_finite_forms(self, &mut table_storage, |x| x.clone());
        Table {
            axes: unpack(&[
                AxisType::Aspect,
                AxisType::Tense,
                AxisType::SubjPerson,
                AxisType::SubjNumber,
            ]),
            entries: table_storage,
        }
    }
}

impl ToTable for Trf<Phrase> {
    fn to_table(&self) -> Table<Option<Phrase>> {
        let mut table_storage = vec![];

        for row in self.iter() {
            table_storage.push(Some(row[0].clone()));
        }

        Table {
            axes: unpack(&[AxisType::Transfinite]),
            entries: table_storage,
        }
    }
}

/// The grammatical categories relevant to finite verb conjugation.
#[derive(Copy, Clone, Eq, PartialEq, Debug)]
pub struct FiniteVerbCategories {
    pub subj_marking: SubjectType,
    pub obj_marking: ObjectType,
    pub tense: Tense,
    pub aspect: Aspect,
}

/// The grammatical categories relevant to transfinite verb conjugation.
#[derive(Copy, Clone, Eq, PartialEq, Debug)]
pub struct TransfiniteVerbCategories {
    pub transfinite: Transfinite,
    pub obj_marking: ObjectType,
}

struct VitreousStemSuffix<'a> {
    stem: Decorated<AStem>,
    suffix: SuffixRef<'a>,
    // Passed around to avoid recomputation
    p_t: &'a Decorated<AStem>,
}

fn add_vitreous_past_suffix(w: Decorated<ParsedWord>) -> Decorated<ParsedWord> {
    match w.frag.remainder.3 {
        Coda::Empty => w.map(|w| w.peel() + Coda::Th),
        Coda::R => w.map(|w| w.peel() + Coda::RTh),
        _ => w.inject(Consonant::T) + x!("a"),
    }
}

pub trait VitreousResult: Sized {
    fn present(self) -> Decorated<ParsedWord>;
    fn past_split(self) -> Result<Decorated<ParsedWord>, Self>;

    fn inflect(self, tense: Tense, obj_marking: ObjectType) -> Decorated<ParsedWord> {
        match tense {
            Tense::Present => append_obj_suffix(self.present(), obj_marking),
            Tense::Past => match self.past_split() {
                Ok(w) => append_obj_suffix(w, obj_marking),
                Err(e) => {
                    let w = append_obj_suffix(e.present(), obj_marking);
                    add_vitreous_past_suffix(w)
                }
            },
        }
    }
}

impl<'a> VitreousResult for VitreousStemSuffix<'a> {
    fn present(self) -> Decorated<ParsedWord> {
        self.stem.append_fin_suffix(self.suffix)
    }

    fn past_split(self) -> Result<Decorated<ParsedWord>, Self> {
        if !self.suffix.tiles.is_empty() {
            Ok(self.p_t.clone().append_fin_suffix(self.suffix))
        } else {
            Err(self)
        }
    }
}

impl VitreousResult for Decorated<ParsedWord> {
    fn present(self) -> Decorated<ParsedWord> {
        self
    }

    fn past_split(self) -> Result<Decorated<ParsedWord>, Self> {
        Err(self)
    }
}

enum VitreousResultEnum<'a> {
    Partitioned(VitreousStemSuffix<'a>),
    Atomic(Decorated<ParsedWord>),
}

impl<'a> VitreousResultEnum<'a> {
    fn inflect(self, tense: Tense, obj_marking: ObjectType) -> Decorated<ParsedWord> {
        match self {
            VitreousResultEnum::Partitioned(x) => x.inflect(tense, obj_marking),
            VitreousResultEnum::Atomic(x) => x.inflect(tense, obj_marking),
        }
    }
}

impl VitreousIrregular {
    fn conjugate_partial<'a>(
        &self,
        form: &VerbForm,
        cats: FiniteVerbCategories,
        s_suffix: &'a [[SuffixRef; SubjectType::COUNT]; Aspect::COUNT],
        p_t: &'a Decorated<AStem>,
    ) -> VitreousResultEnum<'a> {
        let FiniteVerbCategories {
            subj_marking,
            tense,
            aspect,
            ..
        } = cats;

        let aspect_di = (tense as usize) ^ (aspect as usize);

        match self {
            VitreousIrregular::None => {
                let stem = match tense {
                    Tense::Past => &form.p,
                    Tense::Present => &form.n,
                };
                let s_suffix = s_suffix[aspect_di][subj_marking as usize];
                VitreousResultEnum::Partitioned(VitreousStemSuffix {
                    stem: stem.clone(),
                    suffix: s_suffix,
                    p_t,
                })
            }
            VitreousIrregular::Apn(apn) => {
                let word = apn[aspect_di][subj_marking as usize].clone();
                VitreousResultEnum::Atomic(word)
            }
        }
    }
}

enum ObjAffixMode {
    Post,
    Pre,
    PreEclipse,
}

struct Fr {
    conjunct: Decorated<Sylls>,
    absolute: Option<Decorated<ParsedWord>>,
    obj_affix_mode: ObjAffixMode,
}

impl Fr {
    fn apply_object_marking(&self, obj_marking: ObjectType) -> Decorated<ParsedWord> {
        let Fr {
            conjunct,
            absolute,
            obj_affix_mode,
        } = self;
        match (obj_marking, obj_affix_mode) {
            (ObjectType::None, ObjAffixMode::PreEclipse) => absolute
                .clone()
                .unwrap_or_else(|| conjunct.clone().map(ParsedWord::from_sylls))
                .eclipsed(),
            (ObjectType::None, _) => absolute
                .clone()
                .unwrap_or_else(|| conjunct.clone().map(ParsedWord::from_sylls)),
            (_, ObjAffixMode::Post) => {
                let suffix = match conjunct.frag.last_tile().3 {
                    SimpleCoda::L => &OBJECT_SUFFIXES_RESINOUS_ALT,
                    _ => &OBJECT_SUFFIXES_RESINOUS,
                }[(obj_marking as usize) - 1];
                conjunct.clone() + suffix
            }
            (_, mode) => {
                let prefix = OBJECT_SUFFIXES_RESINOUS[(obj_marking as usize) - 1];
                let res = Decorated {
                    frag: ParsedWord::from_sylls(
                        ParsedWord::from(prefix).truncated() + conjunct.frag.view(),
                    ),
                    decoration: conjunct.decoration,
                };
                match mode {
                    ObjAffixMode::Post => unreachable!(),
                    ObjAffixMode::Pre => res,
                    ObjAffixMode::PreEclipse => res.lenited(),
                }
            }
        }
    }
}

fn k(conjunct: Decorated<Sylls>) -> Fr {
    Fr {
        conjunct,
        absolute: None,
        obj_affix_mode: ObjAffixMode::Post,
    }
}

impl VerbForm {
    fn conjugate_vitreous(
        &self,
        overrides: &VitreousIrregular,
    ) -> Result<Box<Fin<Phrase>>, InflectionError> {
        let s_suffix = match self.theta {
            Vowel::A => FINITE_CONJUGATIONS_VITREOUS_A,
            Vowel::I => FINITE_CONJUGATIONS_VITREOUS_I,
            _ => {
                return Err(InflectionError::InvalidThematicVowel {
                    v0: Nucleus::of(self.theta),
                })
            }
        };
        let p_t = fuse_d(self.p.clone(), FusionConsonant::T);

        let mut result: Box<Fin<Phrase>> = default_boxed_array();

        // Iterating this way to reduce nesting.
        for (aspect, tense) in IntoIterator::into_iter(Aspect::VALUES)
            .cartesian_product(IntoIterator::into_iter(Tense::VALUES))
        {
            for (obj_marking, subj_marking) in IntoIterator::into_iter(ObjectType::VALUES)
                .cartesian_product(IntoIterator::into_iter(SubjectType::VALUES))
            {
                let cats = FiniteVerbCategories {
                    subj_marking,
                    obj_marking,
                    tense,
                    aspect,
                };

                let vre = overrides.conjugate_partial(self, cats, &s_suffix, &p_t);

                let mut form = vre.inflect(tense, obj_marking);

                if aspect == Aspect::Perfective {
                    form.eclipse_terminal();
                }
                *result.form_mut(cats) = Phrase::of(form.to_dw());
            }
        }
        Ok(result)
    }

    fn conjugate_resinous(
        &self,
        _overrides: &ResinousIrregular,
    ) -> Result<Box<Fin<Phrase>>, InflectionError> {
        let phi_n = GenCons::for_stem(&self.n);
        let phi_p = GenCons::for_stem(&self.p);

        let p_t = fuse_d(self.p.clone(), FusionConsonant::T);
        let p_n = fuse_d(self.p.clone(), FusionConsonant::N);
        let p_th = fuse_d(self.p.clone(), FusionConsonant::Th);
        let q_t = fuse_d(self.q.clone(), FusionConsonant::T);

        let objectless_forms: nd_array_type!(Fr, Aspect::COUNT, Tense::COUNT, SubjectType::COUNT) = [
            // Imperfective
            [
                // Present
                [
                    // 1X
                    k(self.n.clone().append_fin_ssuffix(v2f(self.theta.tau()))),
                    k(self
                        .n
                        .clone()
                        .append_fin_ssuffix(vc2f(self.theta.tau().hat(), SimpleCoda::C))),
                    k(self
                        .n
                        .clone()
                        .append_fin_ssuffix(vc2f(self.theta.phi(), SimpleCoda::Th))),
                    k(self
                        .n
                        .clone()
                        .append_fin_ssuffix(vc2f(self.theta.tau(), phi_n.into()))),
                    // 1I
                    k(self
                        .n
                        .clone()
                        .append_fin_ssuffix(vc2f(self.theta.tau(), SimpleCoda::C))),
                    k(self
                        .n
                        .clone()
                        .append_fin_ssuffix(vc2f(self.theta.phi().hat(), SimpleCoda::Th))),
                    // 2
                    k(self
                        .n
                        .clone()
                        .append_fin_ssuffix(vc2f(self.theta.phi(), SimpleCoda::S))),
                    k(self.n.clone().append_fin_vowel(self.theta.phi()) + ns!("vi")),
                    k(self
                        .n
                        .clone()
                        .append_fin_ssuffix(vc2f(self.theta.phi(), SimpleCoda::R))),
                    k(self
                        .n
                        .clone()
                        .append_fin_ssuffix(vc2f(self.theta.phi(), phi_n.into()))),
                    // 3
                    Fr {
                        conjunct: self.n.clone().append_fin_ssuffix(f!("e")),
                        absolute: Some(fuse_with_eps_d(self.n.clone())),
                        obj_affix_mode: ObjAffixMode::Post,
                    },
                    k(fuse_d(self.n.clone(), FusionConsonant::T).append_fin_ssuffix(f!("e"))),
                    k(self.n.clone().append_fin_ssuffix(v2f(self.theta.psi()))),
                    k(self.n.clone().append_fin_ssuffix(f!("u"))),
                ],
                // Past
                [
                    // 1X
                    Fr {
                        conjunct: self
                            .p
                            .clone()
                            .append_fin_ssuffix(vc2f(self.theta.tau(), SimpleCoda::L)),
                        absolute: Some(
                            self.p
                                .clone()
                                .append_fin_suffix(vc2x(self.theta.tau(), Coda::Lt)),
                        ),
                        obj_affix_mode: ObjAffixMode::Post,
                    },
                    k(p_n.clone().append_fin_vowel(self.theta.tau()) + ns!("si")),
                    k(p_n.clone().append_fin_vowel(self.theta.tau()) + ns!("vi")),
                    k((self.n.clone().append_fin_vowel(self.theta.tau())
                        + SimpleCoda::from(phi_n)
                        + y!("ta"))
                    .lenited_partial()),
                    // 1I
                    k(p_n.clone().append_fin_ssuffix(f!("eca"))),
                    k(p_n.append_fin_vowel(self.theta.tau()) + ns!("i")),
                    // 2
                    k(self
                        .p
                        .clone()
                        .append_fin_ssuffix(vc2f(self.theta.phi(), SimpleCoda::CTh))),
                    k(p_th
                        .clone()
                        .append_fin_ssuffix(vc2f(self.theta.phi(), SimpleCoda::C))),
                    k(p_th.clone().append_fin_vowel(self.theta.phi())
                        + b!("ł")
                        + v2f(self.theta.tau())),
                    k((self.n.clone().append_fin_vowel(self.theta.phi())
                        + SimpleCoda::from(phi_n)
                        + y!("ta"))
                    .lenited_partial()),
                    // 3
                    Fr {
                        conjunct: p_th
                            .clone()
                            .append_fin_ssuffix(vc2f(self.theta.phi(), SimpleCoda::R)),
                        absolute: Some(
                            p_th.clone()
                                .append_fin_suffix(vc2x(self.theta.phi(), Coda::L)),
                        ),
                        obj_affix_mode: ObjAffixMode::Post,
                    },
                    Fr {
                        conjunct: p_t
                            .clone()
                            .append_fin_ssuffix(vc2f(self.theta.phi(), SimpleCoda::N)),
                        absolute: Some(
                            p_t.clone()
                                .append_fin_suffix(vc2x(self.theta.phi(), Coda::NTh)),
                        ),
                        obj_affix_mode: ObjAffixMode::Post,
                    },
                    k(p_th.append_fin_ssuffix(vc2f(self.theta.psi(), SimpleCoda::R))),
                    k(self.p.clone().append_fin_ssuffix(f!("oþ"))),
                ],
            ],
            // Perfective
            [
                // Present
                [
                    // 1X
                    k((if self.n.ends_with_empty_bridge() {
                        self.n.clone().map(|f| {
                            f.peel().peel() + Initial::Single(Consonant::P) + v2f(self.theta.tau())
                        })
                    } else {
                        self.n.clone() + self.theta + ns!("e")
                    })
                    .eclipsed()),
                    k(self.n.clone().append_fin_ssuffix(f!("aŋe")).lenited()),
                    k(self
                        .n
                        .clone()
                        .append_fin_ssuffix(vc2f(self.theta.tau(), SimpleCoda::N))
                        .lenited()),
                    k(self
                        .p
                        .clone()
                        .append_fin_ssuffix(vc2f(self.theta.tau(), phi_p.into()))),
                    // 1I
                    k(self.n.clone().append_fin_ssuffix(f!("âge")).lenited()),
                    k(self
                        .n
                        .clone()
                        .append_fin_ssuffix(vc2f(self.theta.tau().hat(), SimpleCoda::R))
                        .lenited()),
                    // 2
                    k(self.n.clone().append_fin_ssuffix(f!("ea")).eclipsed()),
                    k((self.n.clone().append_fin_vowel(self.theta) + ns!("ste")).eclipsed()),
                    k((self.n.clone().append_fin_vowel(self.theta.phi()) + ns!("ris")).eclipsed()),
                    k(self
                        .p
                        .clone()
                        .append_fin_ssuffix(vc2f(self.theta.phi(), phi_p.into()))),
                    // 3
                    Fr {
                        conjunct: self.n.clone().append_fin_ssuffix(v2f(self.theta.phi())),
                        absolute: None,
                        obj_affix_mode: ObjAffixMode::PreEclipse,
                    },
                    k(self
                        .n
                        .clone()
                        .append_fin_ssuffix(vc2f(self.theta.phi(), SimpleCoda::N))
                        .eclipsed()),
                    Fr {
                        conjunct: self.n.clone().append_fin_ssuffix(v2f(self.theta.psi())),
                        absolute: Some(
                            self.n
                                .clone()
                                .append_fin_suffix(v2x(self.theta.psi()))
                                .eclipsed(),
                        ),
                        obj_affix_mode: ObjAffixMode::PreEclipse,
                    },
                    k(self.n.clone().append_fin_ssuffix(f!("os"))),
                ],
                // Past
                [
                    // 1X
                    k((p_t.clone().append_fin_ssuffix(v2f(self.theta.tau()))).eclipsed()),
                    k((q_t.clone().append_fin_ssuffix(f!("ôr"))).eclipsed()),
                    Fr {
                        conjunct: (q_t
                            .clone()
                            .append_fin_ssuffix(vc2f(self.theta.tau().hat(), SimpleCoda::S)))
                        .eclipsed(),
                        absolute: Some(
                            (self
                                .q
                                .clone()
                                .append_fin_suffix(vc2x(self.theta.tau(), Coda::St)))
                            .eclipsed(),
                        ),
                        obj_affix_mode: ObjAffixMode::Post,
                    },
                    k((self
                        .p
                        .clone()
                        .append_fin_ssuffix(vc2f(self.theta.tau(), phi_p.into()))
                        + y!("ta"))
                    .eclipsed()),
                    // 1I
                    k((q_t.clone().append_fin_ssuffix(f!("or"))).eclipsed()),
                    Fr {
                        conjunct: (q_t.clone().append_fin_ssuffix(f!("êłi"))).eclipsed(),
                        absolute: Some((q_t.append_fin_suffix(x!("ełt"))).eclipsed()),
                        obj_affix_mode: ObjAffixMode::Post,
                    },
                    // 2
                    k((p_t
                        .clone()
                        .append_fin_ssuffix(vc2f(self.theta.phi(), SimpleCoda::S)))
                    .eclipsed()),
                    k((p_t.clone().append_fin_ssuffix(f!("eši"))).eclipsed()),
                    k((p_t
                        .clone()
                        .append_fin_ssuffix(vc2f(self.theta.phi().gamma(), SimpleCoda::S)))
                    .eclipsed()),
                    k((self
                        .p
                        .clone()
                        .append_fin_ssuffix(vc2f(self.theta.phi(), phi_p.into()))
                        + y!("ta"))
                    .eclipsed()),
                    // 3
                    k((p_t.clone().append_fin_ssuffix(v2f(self.theta.phi()))).eclipsed()),
                    k((p_t.clone().append_fin_vowel(self.theta.phi()) + ns!("ri")).eclipsed()),
                    Fr {
                        conjunct: (p_t
                            .clone()
                            .append_fin_ssuffix(vc2f(self.theta.phi(), SimpleCoda::L)))
                        .eclipsed(),
                        absolute: Some(
                            (p_t.append_fin_suffix(vc2x(self.theta.phi(), Coda::Lt))).eclipsed(),
                        ),
                        obj_affix_mode: ObjAffixMode::Post,
                    },
                    k((self.p.clone().append_fin_ssuffix(f!("oþ"))).eclipsed()),
                ],
            ],
        ];

        let mut result: Box<Fin<Phrase>> = default_boxed_array();
        for (aspect, tense) in IntoIterator::into_iter(Aspect::VALUES)
            .cartesian_product(IntoIterator::into_iter(Tense::VALUES))
        {
            for (obj_marking, subj_marking) in IntoIterator::into_iter(ObjectType::VALUES)
                .cartesian_product(IntoIterator::into_iter(SubjectType::VALUES))
            {
                let form =
                    &objectless_forms[aspect as usize][tense as usize][subj_marking as usize];
                let form = form.apply_object_marking(obj_marking);
                *result.form_mut(FiniteVerbCategories {
                    subj_marking,
                    obj_marking,
                    tense,
                    aspect,
                }) = Phrase::of(form.to_dw());
            }
        }

        Ok(result)
    }

    pub fn conjugate_finite(&self) -> Result<Box<Fin<Phrase>>, InflectionError> {
        match &self.material {
            Material::Vitreous(overrides) => self.conjugate_vitreous(overrides),
            Material::Resinous(overrides) => self.conjugate_resinous(overrides),
        }
    }

    pub fn conjugate_transfinite(&self) -> Result<Option<Box<Trf<Phrase>>>, InflectionError> {
        Ok(match self.material {
            Material::Vitreous(_) => None,
            Material::Resinous(_) => Some({
                let n_n = fuse_d(self.n.clone(), FusionConsonant::N);
                let n_th = fuse_d(self.n.clone(), FusionConsonant::Th);

                let forms: [Fr; Transfinite::COUNT] =
                    [
                        // 2sg imp.
                        k(n_th.clone().append_fin_ssuffix(f!("au"))),
                        // Supine
                        Fr {
                            conjunct: n_n.clone().append_fin_ssuffix(f!("ase")),
                            absolute: Some(n_n.clone().append_fin_suffix(x!("els"))),
                            obj_affix_mode: ObjAffixMode::Post,
                        },
                        // Active gerundives
                        k(n_n.clone()
                            + gg!("elv")
                            + Nucleus::of(self.theta.phi())
                            + SimpleCoda::Empty),
                        Fr {
                            conjunct: n_n.clone() + f!("ełor"),
                            absolute: None,
                            obj_affix_mode: ObjAffixMode::Pre,
                        },
                        Fr {
                            conjunct: n_n
                                + gg!("elv")
                                + Nucleus::of(self.theta.phi().gamma())
                                + SimpleCoda::N,
                            absolute: None,
                            obj_affix_mode: ObjAffixMode::Pre,
                        },
                        // Passive gerundives
                        k(n_th.clone()
                            + gg!("elv")
                            + Nucleus::of(self.theta.phi())
                            + SimpleCoda::Empty),
                        Fr {
                            conjunct: n_th
                                + gg!("elv")
                                + Nucleus::of(self.theta.phi().gamma())
                                + SimpleCoda::N,
                            absolute: None,
                            obj_affix_mode: ObjAffixMode::Pre,
                        },
                    ];

                let mut result: Box<Trf<Phrase>> = default_boxed_array();

                for (transfinite, obj_marking) in IntoIterator::into_iter(Transfinite::VALUES)
                    .cartesian_product(IntoIterator::into_iter(ObjectType::VALUES))
                {
                    let form = &forms[transfinite as usize];
                    let form = form.apply_object_marking(obj_marking);

                    *result.form_mut(TransfiniteVerbCategories {
                        transfinite,
                        obj_marking,
                    }) = Phrase::of(form.to_dw());
                }

                result
            }),
        })
    }
}

fn append_obj_suffix(w: Decorated<ParsedWord>, obj_marking: ObjectType) -> Decorated<ParsedWord> {
    match obj_marking {
        ObjectType::None => w,
        _ => {
            let suffix = OBJECT_SUFFIXES_VITREOUS[(obj_marking as usize) - 1];
            (w.map(|frag| frag.truncated()) + suffix).map(ParsedWord::from_sylls)
        }
    }
}

static FINITE_CONJUGATIONS_VITREOUS_A: [[SuffixRef; SubjectType::COUNT]; Aspect::COUNT] = [
    [
        x!("a"),
        x!("an"),
        x!("ecþ"),
        x!("af"),
        x!("ân"),
        x!("êcþ"),
        x!("es"),
        x!("ens"),
        x!("er"),
        x!("ef"),
        x!("e"),
        x!("en"),
        x!("i"),
        x!("u"),
    ],
    [
        x!("al"),
        x!("anis"),
        x!("ecþis"),
        x!("af"),
        x!("ânis"),
        x!("êcþis"),
        x!("eres"),
        x!("ensis"),
        x!("eris"),
        x!("ef"),
        x!("el"),
        x!("enis"),
        x!("iris"),
        x!("os"),
    ],
];

static FINITE_CONJUGATIONS_VITREOUS_I: [[SuffixRef; SubjectType::COUNT]; Aspect::COUNT] = [
    [
        x!("e"),
        x!("en"),
        x!("acþ"),
        x!("ef"),
        x!("ên"),
        x!("âcþ"),
        x!("as"),
        x!("ans"),
        x!("ar"),
        x!("af"),
        x!("a"),
        x!("an"),
        x!("o"),
        x!("u"),
    ],
    [
        x!("el"),
        x!("enis"),
        x!("acþis"),
        x!("ef"),
        x!("ênis"),
        x!("âcþis"),
        x!("ares"),
        x!("ansis"),
        x!("aris"),
        x!("af"),
        x!("al"),
        x!("anis"),
        x!("oris"),
        x!("os"),
    ],
];

static OBJECT_SUFFIXES_VITREOUS: [SyllsRef; ObjectType::COUNT - 1] = [
    y!("pe"),
    y!("pjo"),
    y!("po"),
    y!("ten"),
    y!("pjô"),
    y!("pô"),
    y!("ve"),
    y!("vi"),
    y!("vo"),
    y!("ves"),
    y!("le"),
    y!("lu"),
    y!("les"),
    y!("lis"),
    y!("los"),
    y!("li"),
    y!("lo"),
    y!("las"),
    y!("cin"),
    y!("riþ"),
];

static OBJECT_SUFFIXES_RESINOUS: [ParsedWordRef; ObjectType::COUNT - 1] = [
    w!("pe"),
    w!("pal"),
    w!("gins"),
    w!("peł"),
    w!("pâl"),
    w!("gîns"),
    w!("ve"),
    w!("vel"),
    w!("tor"),
    w!("veł"),
    w!("li"),
    w!("rel"),
    w!("li"),
    w!("lec"),
    w!("ljen"),
    w!("lec"),
    w!("lje"),
    w!("sâ"),
    w!("cin"),
    w!("riþ"),
];

static OBJECT_SUFFIXES_RESINOUS_ALT: [ParsedWordRef; ObjectType::COUNT - 1] = [
    w!("pe"),
    w!("pae"),
    w!("gins"),
    w!("peþ"),
    w!("paê"),
    w!("gîns"),
    w!("ve"),
    w!("ven"),
    w!("tor"),
    w!("veþ"),
    w!("gi"),
    w!("rel"),
    w!("gi"),
    w!("geþ"),
    w!("gjan"),
    w!("geþ"),
    w!("gja"),
    w!("sâ"),
    w!("cin"),
    w!("giþ"),
];
