//! Generic tables.

use f9i_core::{
    category::{AxisType, Case, Gender, NNumber, RCase, MAX_CATEGORY_TAGS},
    mgc::Phrase,
};

/// Represents a table with custom axes.
#[derive(Clone, Eq, PartialEq, Debug)]
pub struct Table<T> {
    /// The category for each axis.
    ///
    /// This permits a maximum of [MAX_CATEGORY_TAGS] axes.
    /// If there are fewer than [MAX_CATEGORY_TAGS] axes,
    /// then the rest of the elements are padded with [None].
    pub axes: [Option<AxisType>; MAX_CATEGORY_TAGS],
    /// The entries of the table.
    pub entries: Vec<T>,
}

impl<T> Table<T> {
    /**
    Get an entry in the table.

    `f` accepts a [AxisType] and should return the ordinal value of the corresponding enum value that should be addressed, or 0 if there is none.
    */
    pub fn get_generic<F>(&self, f: F) -> &T
    where
        F: Fn(AxisType) -> usize,
    {
        let linear_index = get_linear_index(&self.axes, f);
        &self.entries[linear_index]
    }
    /**
    Like [Table::get_generic], but returns a mutable reference.
    */
    pub fn get_generic_mut<F>(&mut self, f: F) -> &mut T
    where
        F: Fn(AxisType) -> usize,
    {
        let linear_index = get_linear_index(&self.axes, f);
        &mut self.entries[linear_index]
    }

    pub fn get_v(&self, rcase: RCase, hcase: Case, hgender: Gender, hnumber: NNumber) -> &T {
        self.get_generic(|tag| match tag {
            AxisType::RCase => rcase as usize,
            AxisType::HCase => hcase as usize,
            AxisType::HGender => hgender as usize,
            AxisType::HNumber => hnumber as usize,
            _ => 0,
        })
    }

    pub fn get_v_mut(
        &mut self,
        rcase: RCase,
        hcase: Case,
        hgender: Gender,
        hnumber: NNumber,
    ) -> &mut T {
        self.get_generic_mut(|tag| match tag {
            AxisType::RCase => rcase as usize,
            AxisType::HCase => hcase as usize,
            AxisType::HGender => hgender as usize,
            AxisType::HNumber => hnumber as usize,
            _ => 0,
        })
    }

    pub fn map<U, F>(self, f: F) -> Table<U>
    where
        F: FnMut(T) -> U,
    {
        Table {
            axes: self.axes,
            entries: self.entries.into_iter().map(f).collect(),
        }
    }
    pub fn map_ref<U, F>(&self, f: F) -> Table<U>
    where
        F: FnMut(&T) -> U,
    {
        Table {
            axes: self.axes,
            entries: self.entries.iter().map(f).collect(),
        }
    }
    pub fn try_map<U, F, E>(self, mut f: F) -> Result<Table<U>, E>
    where
        F: FnMut(T) -> Result<U, E>,
    {
        let mut output_vec = Vec::with_capacity(self.entries.len());
        for e in self.entries {
            output_vec.push(f(e)?);
        }
        Ok(Table {
            axes: self.axes,
            entries: output_vec,
        })
    }

    /**
    Iterates through the elements of a table.

    The callback function `f` accepts:

    * a reference to the element being iterated through,
    * and a slice of ordinal values corresponding to the labels for the axes of the current element.
    */
    pub fn for_each<F>(&self, mut f: F)
    where
        F: FnMut(&T, &[u8]),
    {
        let mut axis_idxs = [0u8; MAX_CATEGORY_TAGS + 1];
        let num_axes = self
            .axes
            .iter()
            .position(|a| a.is_none())
            .unwrap_or(MAX_CATEGORY_TAGS);
        for elem in &self.entries {
            f(elem, &axis_idxs[1..]);
            axis_idxs[num_axes] += 1;
            {
                let mut i = num_axes;
                while i > 0
                    && (axis_idxs[i] as usize)
                        == self.axes[i - 1].map(|t| t.get_count()).unwrap_or(0)
                {
                    axis_idxs[i] = 0;
                    axis_idxs[i - 1] += 1;
                    i -= 1;
                }
            }
        }
    }
}

fn get_linear_index_with_stride<F>(axes: &[Option<AxisType>], f: F) -> (usize, usize)
where
    F: Fn(AxisType) -> usize,
{
    match axes.first() {
        None | Some(None) => (0, 1),
        Some(Some(tag)) => {
            let tag_idx = f(*tag);
            let (rest_li, rest_st) = get_linear_index_with_stride(&axes[1..], f);
            let tag_st = tag.get_count();
            (tag_idx * rest_st + rest_li, tag_st * rest_st)
        }
    }
}

fn get_linear_index<F>(axes: &[Option<AxisType>], f: F) -> usize
where
    F: Fn(AxisType) -> usize,
{
    get_linear_index_with_stride(axes, f).0
}

/// Something that holds data that can be shown in a table.
pub trait ToTable<T = Option<Phrase>> {
    fn to_table(&self) -> Table<T>;
}
