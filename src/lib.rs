#![feature(trace_macros)]
#![feature(io_error_more)]
#![feature(array_chunks)]
#![feature(type_alias_impl_trait)]
#![feature(array_try_map)]
#![feature(slice_ptr_get)]
#![feature(let_chains)]
#![feature(impl_trait_in_assoc_type)]

//! The programmatic interface to *f9i*, the fast Ŋarâþ Crîþ v9 inflector.

pub mod analytics;
pub mod collate;
pub mod error;
pub mod headword;
pub mod lexical;
pub mod morphophonology;
pub mod nest;
pub mod noun;
pub mod output;
pub mod parse;
pub mod relational;
pub mod table;
pub mod verb;

#[cfg(test)]
mod tests {
    use super::noun::*;
    use super::parse;
    use super::relational::*;
    use super::verb::finite::*;
    use super::verb::*;
    use crate::noun::fifth::DeclensionV;
    use crate::noun::first::DeclensionI;
    use crate::noun::fourth::DeclensionIV;
    use crate::noun::second::DeclensionII;
    use crate::noun::second::DeclensionIIKind;
    use crate::noun::second::DeclensionIIpKind;
    use crate::noun::second::DeclensionIIuKind;
    use crate::noun::sixth::DeclensionVI;
    use crate::noun::third::DeclensionIII;
    use crate::noun::third::DeclensionIIIKind;
    use crate::verb::participle::I1Subspecies;
    use crate::verb::participle::InstAbessEndings;
    use crate::verb::participle::Species;
    use f9i_core::assemblage::AStem;
    use f9i_core::assemblage::Decorated;
    use f9i_core::category::*;
    use f9i_core::mgc::*;
    use f9i_ivm_macro::{a, decw, phr, w, y};

    #[test]
    #[allow(clippy::unit_cmp)]
    fn test_assemblage_proc_macros() {
        let word_s = y!("celsan");
        assert_eq!(word_s.remainder, ());
        assert_eq!(word_s.tiles.len(), 2);
        assert_eq!(&word_s.tokens_to_string(), "celsan");
        let word_s = w!("rafels");
        assert_eq!(
            word_s.remainder,
            (
                Initial::Single(Consonant::F),
                Glide::None,
                Vowel::E,
                Coda::Ls
            )
        );
        assert_eq!(word_s.tiles.len(), 1);
        assert_eq!(&word_s.tokens_to_string(), "rafels");
    }

    fn test_noun_helper(noun: NounForm, clareth: Clareth, expected_declensions: NounTable<&str>) {
        let expected_declensions = expected_declensions.map_ref(|w| Phrase::parse(w).unwrap());
        assert_eq!(noun.decline(clareth), Ok(expected_declensions))
    }
    #[test]
    fn decline_i_v_noun() {
        test_noun_helper(
            NounForm::I(DeclensionI {
                n: a!("gašid").into(),
                l: a!("gašjod").into(),
                s: a!("gelšid").into(),
                theta: Vowel::A,
                lambda: Vowel::A,
                kind: first::DeclensionIKind::V,
            }),
            Clareth::Collective,
            NounTable::Collective([
                ["gašida", "gašidel", "gašidał"],
                ["gašidan", "gašînes", "gašidełen"],
                ["gašidas", "gašîneþ", "gašidałes"],
                ["gašiden", "ŋgašînen", "gašîneł"],
                ["gašjodas", "gašjodens", "gašjodeł"],
                ["gašjodeca", "gašjodenca", "gašjodełca"],
                ["gašjodeþa", "gašjodenþa", "gašjodełta"],
                ["gelšidit", "gelšidicta", "gelšidicþ"],
            ]),
        );
    }
    #[test]
    fn decline_i_vs_noun() {
        test_noun_helper(
            NounForm::I(DeclensionI {
                n: a!("elt").into(),
                l: a!("ilt").into(),
                s: a!("eld").into(),
                theta: Vowel::E,
                lambda: Vowel::A,
                kind: first::DeclensionIKind::Vs,
            }),
            Clareth::Singular,
            NounTable::Singular([
                ["eltes", "eltec", "eltis", "eltef"],
                ["eltêns", "eltôr", "elter", "eltefen"],
                ["elto", "elettes", "elteri", "eltefes"],
                ["eltin", "Gelettin", "Geltin", "elennef"],
                ["iltas", "iltac", "iltos", "iltef"],
                ["ilteca", "iltecca", "ilteca", "iltefca"],
                ["ilteþa", "iltecþa", "ilteþa", "iltefþa"],
                ["eldit", "eldet", "eldet", "eldicþ"],
            ]),
        );
    }
    #[test]
    fn decline_i_vxth_noun() {
        test_noun_helper(
            NounForm::I(DeclensionI {
                n: a!("ŋar").into(),
                l: a!("ŋôrþ").into(),
                s: a!("ŋal").into(),
                theta: Vowel::A,
                lambda: Vowel::A,
                kind: first::DeclensionIKind::Vxth,
            }),
            Clareth::Singular,
            NounTable::Singular([
                ["ŋarâþ", "ŋarac", "ŋarôþ", "ŋaraf"],
                ["ŋarâns", "ŋarôr", "ŋarar", "ŋarefen"],
                ["ŋaras", "ŋartas", "ŋarasi", "ŋarafes"],
                ["ŋaren", "ŋarten", "ŋarin", "ŋarnef"],
                ["ŋôrþas", "ŋôrþac", "ŋôrþos", "ŋôrþef"],
                ["ŋôrþeca", "ŋôrþecca", "ŋôrþeca", "ŋôrþefca"],
                ["ŋôrteþa", "ŋôrþecþa", "ŋôrteþa", "ŋôrþefþa"],
                ["ŋalit", "ŋalet", "ŋalet", "ŋalicþ"],
            ]),
        );
    }
    #[test]
    fn decline_i_vn_noun() {
        test_noun_helper(
            NounForm::I(DeclensionI {
                n: a!("elt").into(),
                l: a!("eln").into(),
                s: a!("enl").into(),
                theta: Vowel::A,
                lambda: Vowel::A,
                kind: first::DeclensionIKind::Vn,
            }),
            Clareth::Singular,
            NounTable::Singular([
                ["eltan", "eltac", "elton", "eltaf"],
                ["eltanen", "eltôr", "eltar", "eltefen"],
                ["eltas", "elettas", "eltari", "eltafes"],
                ["elten", "Geletten", "Geltin", "elennef"],
                ["elnas", "elnac", "elnos", "elnef"],
                ["elneca", "elnecca", "elnica", "elnefca"],
                ["elneþa", "elnecþa", "elniþa", "elnefþa"],
                ["enlit", "enlet", "enlet", "enlicþ"],
            ]),
        );
    }
    #[test]
    fn decline_iip_in_noun() {
        test_noun_helper(
            NounForm::II(DeclensionII {
                n: a!("leþ").into(),
                l: a!("lis").into(),
                s: a!("leð").into(),
                theta: Vowel::I,
                lambda: Vowel::E,
                kind: DeclensionIIKind::P(DeclensionIIpKind::In, a!("lers").into()),
            }),
            Clareth::Collective,
            NounTable::Collective([
                ["leþin", "leþnen", "leþþes"],
                ["leþne", "leþeħin", "leþþens"],
                ["leþtês", "leþerin", "lesteþ"],
                ["lersen", "leresnes", "leþþest"],
                ["liselt", "lersilten", "lisef"],
                ["liselca", "liselcen", "liselcaf"],
                ["liselþa", "liselþen", "liselþaf"],
                ["leðit", "leðicti", "leðicþ"],
            ]),
        );
    }
    #[test]
    fn decline_iip_is_noun() {
        test_noun_helper(
            NounForm::II(DeclensionII {
                n: a!("cþir").into(),
                l: a!("cþor").into(),
                s: a!("cþil").into(),
                theta: Vowel::I,
                lambda: Vowel::E,
                kind: DeclensionIIKind::P(DeclensionIIpKind::Is, a!("cþis").into()),
            }),
            Clareth::Mass,
            NounTable::Mass([
                ["cþiris", "cþirþes"],
                ["cþirne", "cþirþens"],
                ["cþirtês", "cþirteþ"],
                ["cþisen", "cþirþest"],
                ["cþorelt", "cþoref"],
                ["cþorelca", "cþorelcaf"],
                ["cþorelþa", "cþorelþaf"],
                ["cþilit", "cþilicþ"],
            ]),
        );
    }
    #[test]
    fn decline_iip_vxr_noun() {
        test_noun_helper(
            NounForm::II(DeclensionII {
                n: a!("ant").into(),
                l: a!("ont").into(),
                s: a!("and").into(),
                theta: Vowel::A,
                lambda: Vowel::E,
                kind: DeclensionIIKind::P(DeclensionIIpKind::Vxr, a!("ant").into()),
            }),
            Clareth::Mass,
            NounTable::Mass([
                ["antâr", "anþas"],
                ["annel", "anþans"],
                ["antês", "antaþ"],
                ["antil", "anþast"],
                ["ontelt", "ontef"],
                ["ontelca", "ontelcaf"],
                ["ontelþa", "ontelþaf"],
                ["andit", "andicþ"],
            ]),
        );
    }
    #[test]
    fn decline_iiu_vr_noun() {
        test_noun_helper(
            NounForm::II(DeclensionII {
                n: a!("gal").into(),
                l: a!("gel").into(),
                s: a!("gal").into(),
                theta: Vowel::A,
                lambda: Vowel::E,
                kind: DeclensionIIKind::U(DeclensionIIuKind::Vr),
            }),
            Clareth::Singular,
            NounTable::Singular([
                ["galar", "galac", "galer", "galþas"],
                ["galarin", "galjâr", "galeri", "galþans"],
                ["galals", "galtel", "galari", "galtaþ"],
                ["galai", "ŋgalaci", "ŋgalavi", "galþast"],
                ["gelelt", "geleltac", "galilt", "gelef"],
                ["gelelca", "gelelhac", "gelelco", "gelelcaf"],
                ["gelelþa", "gelelþac", "gelelþo", "gelelþaf"],
                ["galit", "galet", "galet", "galicþ"],
            ]),
        );
    }
    #[test]
    fn decline_iiu_vl_noun() {
        test_noun_helper(
            NounForm::II(DeclensionII {
                n: a!("serf").into(),
                l: a!("šorf").into(),
                s: a!("šelf").into(),
                theta: Vowel::I,
                lambda: Vowel::I,
                kind: DeclensionIIKind::U(DeclensionIIuKind::Vl),
            }),
            Clareth::Mass,
            NounTable::Mass([
                ["serfil", "serefþes"],
                ["serfirin", "serefþens"],
                ["serfils", "serefteþ"],
                ["serfei", "serefþest"],
                ["šorfilt", "šorfił"],
                ["šorfilca", "šorfilcał"],
                ["šorfilþa", "šorfilþał"],
                ["šelfit", "šelficþ"],
            ]),
        );
    }
    #[test]
    fn decline_iiu_vth_noun() {
        test_noun_helper(
            NounForm::II(DeclensionII {
                n: a!("šim").into(),
                l: a!("šelm").into(),
                s: a!("šim").into(),
                theta: Vowel::E,
                lambda: Vowel::I,
                kind: DeclensionIIKind::U(DeclensionIIuKind::Vth),
            }),
            Clareth::Singular,
            NounTable::Singular([
                ["šimeþ", "šimec", "šimoþ", "šifþes"],
                ["šimerin", "šimjêr", "šimeri", "šifþens"],
                ["šimels", "šiftel", "šimari", "šifteþ"],
                ["šimei", "šimeci", "šimevi", "šifþest"],
                ["šelmilt", "šelmiltec", "šimelt", "šelmif"],
                ["šelmilca", "šelmilhac", "šelmilco", "šelmilcaf"],
                ["šelmilþa", "šelmilþac", "šelmilþo", "šelmilþaf"],
                ["šimit", "šimet", "šimet", "šimicþ"],
            ]),
        );
    }
    #[test]
    fn decline_iiu_vrth_noun() {
        test_noun_helper(
            NounForm::II(DeclensionII {
                n: a!("at").into(),
                l: a!("terþ").into(),
                s: a!("aþc").into(),
                theta: Vowel::E,
                lambda: Vowel::E,
                kind: DeclensionIIKind::U(DeclensionIIuKind::Vrth),
            }),
            Clareth::Singular,
            NounTable::Singular([
                ["aterþ", "atec", "atorþ", "atþes"],
                ["aterin", "atjêr", "ateri", "atþens"],
                ["atels", "attel", "atari", "atteþ"],
                ["atei", "Gateci", "Gatevi", "atþest"],
                ["terþelt", "terþeltec", "atilt", "terþef"],
                ["terþelca", "terþelhac", "terþelco", "terþelcaf"],
                ["terþelþa", "terþelþac", "terþelþo", "terþelþaf"],
                ["aþcit", "aþcet", "aþcet", "aþcicþ"],
            ]),
        );
    }
    #[test]
    fn decline_iii_os_noun() {
        test_noun_helper(
            NounForm::III(DeclensionIII {
                n: a!("mort").into(),
                l: a!("mald").into(),
                s: a!("molt").into(),
                kind: DeclensionIIIKind::Os,
            }),
            Clareth::Singular,
            NounTable::Singular([
                ["mortos", "mortoc", "mortor", "mortu"],
                ["morton", "moretton", "moretþon", "mortan"],
                ["mortoþ", "morettoþ", "mortasor", "mortas"],
                ["mortel", "morettel", "mortjel", "morenne"],
                ["maldos", "maldocþ", "maldor", "maldeł"],
                ["cjamaldos", "cjamaldocþ", "cjamaldor", "cjamaldeł"],
                ["þjam·aldos", "þjam·aldocþ", "þjam·aldor", "þjam·aldeł"],
                ["moltot", "moltoctos", "moltet", "moltocþ"],
            ]),
        );
    }
    #[test]
    fn decline_iii_or_noun() {
        test_noun_helper(
            NounForm::III(DeclensionIII {
                n: a!("tfeł").into(),
                l: a!("tfoł").into(),
                s: a!("tfeł").into(),
                kind: DeclensionIIIKind::Or,
            }),
            Clareth::Singular,
            NounTable::Singular([
                ["tfełor", "tfełoc", "tfełosôr", "tfełu"],
                ["tfełon", "tfełton", "tfełton", "tfełan"],
                ["tfełoþ", "tfełtoþ", "tfełasor", "tfełas"],
                ["tfełel", "tfełtel", "tfełjel", "tfełne"],
                ["tfołos", "tfołocþ", "tfołor", "tfołeł"],
                ["cjatfołor", "cjatfołoc", "cjatfołor", "cjatfołeł"],
                ["þjatfołor", "þjatfołoc", "þjatfołor", "þjatfołeł"],
                ["tfełot", "tfełoctos", "tfełet", "tfełocþ"],
            ]),
        );
    }
    #[test]
    fn decline_iii_on_noun() {
        test_noun_helper(
            NounForm::III(DeclensionIII {
                n: a!("cenč").into(),
                l: a!("cinč").into(),
                s: a!("cenč").into(),
                kind: DeclensionIIIKind::On,
            }),
            Clareth::Singular,
            NounTable::Singular([
                ["cenčon", "cenčoc", "cenčor", "cenču"],
                ["cenčanon", "cenčanor", "cenčanor", "cenčan"],
                ["cenčoþ", "cenčitoþ", "cenčasor", "cenčas"],
                ["cenčel", "gcenčitel", "gcenčjel", "cenčine"],
                ["cinčos", "cinčocþ", "cinčor", "cinčef"],
                ["cjacinčon", "cjacinčoc", "cjacinčor", "cjacinčef"],
                ["þjac·inčon", "þjac·inčoc", "þjac·inčor", "þjac·inčef"],
                ["cenčot", "cenčoctos", "cenčet", "cenčocþ"],
            ]),
        );
    }
    #[test]
    fn decline_iii_el_noun() {
        test_noun_helper(
            NounForm::III(DeclensionIII {
                n: a!("łercþ").into(),
                l: a!("łircþ").into(),
                s: a!("łelþ").into(),
                kind: DeclensionIIIKind::El {
                    a: a!("łercþ").into(),
                    g: a!("łircþ").into(),
                },
            }),
            Clareth::Collective,
            NounTable::Collective([
                ["łercþel", "łercþons", "łercþul"],
                ["łercþen", "łercþelt", "łercþan"],
                ["łertoþ", "łereþnes", "łercþas"],
                ["łircþel", "lłireþnel", "łereþne"],
                ["łircþos", "łircþoren", "łircþef"],
                ["cjałircþel", "cjałircþolt", "cjałircþef"],
                ["þjałircþel", "þjałircþolt", "þjałircþef"],
                ["łelþot", "łelþełi", "łelþocþ"],
            ]),
        );
    }
    #[test]
    fn decline_iv_noun() {
        test_noun_helper(
            NounForm::IV(DeclensionIV {
                n: a!("crîþ").into(),
                l: a!("crîlþ").into(),
                s: a!("clîþ").into(),
                theta: Vowel::O,
                lambda: Vowel::E,
            }),
            Clareth::Singular,
            NounTable::Singular([
                ["crîþ", "crîþec", "crîþor", "crîþaf"],
                ["crîþon", "crîþton", "crîþas", "crîþafen"],
                ["crîþe", "crîþic", "crîþir", "crîþafes"],
                ["crîþa", "gcrîþac", "gcrîþo", "crîþnef"],
                ["crîlþes", "crîlþec", "crîlþis", "crîlþef"],
                ["crîlþeca", "crîlþecca", "crîlþeca", "crîlþefca"],
                ["crîlteþa", "crîlþecþa", "crîlteþa", "crîlþefþa"],
                ["clîþot", "clîþoctos", "clîþet", "clîþocþ"],
            ]),
        );
    }
    #[test]
    fn decline_v_noun() {
        test_noun_helper(
            NounForm::V(DeclensionV {
                n: a!("r").into(),
                s: a!("ran").into(),
                theta: Nucleus::O,
                sigma: SimpleCoda::N,
            }),
            Clareth::Mass,
            NounTable::Mass([
                ["ron", "ronþu"],
                ["ronþen", "ronþan"],
                ["ronþer", "ronþas"],
                ["ronþes", "ronþe"],
                ["rinþa", "rinþaf"],
                ["rinþeca", "rinþecaf"],
                ["rinteþa", "rinteþaf"],
                ["ranot", "ranocþ"],
            ]),
        );
    }
    #[test]
    fn decline_vi_noun() {
        test_noun_helper(
            NounForm::VI(DeclensionVI {
                n: a!("relt").into(),
                l: a!("rilt").into(),
                i: a!("rilt").into(),
                i_prime: a!("relt").into(),
                s: a!("relt").into(),
                theta: Vowel::E,
                lambda: Vowel::E,
            }),
            Clareth::Collective,
            NounTable::Collective([
                ["relten", "reltil", "reltu"],
                ["reltena", "reltenan", "reltan"],
                ["reltens", "relennes", "reltas"],
                ["reltil", "relennin", "relenne"],
                ["riltes", "riltins", "riltef"],
                ["cjariltes", "cjareltans", "cjariltef"],
                ["þjariltes", "þjareltans", "þjariltef"],
                ["reltit", "relticte", "relticþ"],
            ]),
        );
    }
    #[test]
    fn decline_compound_noun() {
        let tor = Decoration {
            flags: DecorationFlags::empty(),
            name_marker: NameMarker::Tor,
        };
        let carth = Decoration {
            flags: DecorationFlags::empty(),
            name_marker: NameMarker::Carth,
        };
        let my_name = NounForm::Compound(Box::new([
            NounForm::VI(DeclensionVI {
                n: Decorated {
                    decoration: tor,
                    frag: a!("merl").into(),
                },
                l: Decorated {
                    decoration: tor,
                    frag: a!("mirł").into(),
                },
                i: Decorated {
                    decoration: tor,
                    frag: a!("mirł").into(),
                },
                i_prime: Decorated {
                    decoration: tor,
                    frag: a!("mirł").into(),
                },
                s: Decorated {
                    decoration: tor,
                    frag: a!("merl").into(),
                },
                theta: Vowel::A,
                lambda: Vowel::E,
            }),
            NounForm::I(DeclensionI {
                n: Decorated {
                    decoration: carth,
                    frag: a!("fliror").into(),
                },
                l: Decorated {
                    decoration: carth,
                    frag: a!("flier").into(),
                },
                s: Decorated {
                    decoration: carth,
                    frag: a!("flirol").into(),
                },
                theta: Vowel::A,
                lambda: Vowel::A,
                kind: first::DeclensionIKind::V,
            }),
        ]));
        test_noun_helper(
            my_name,
            Clareth::Singular,
            NounTable::Singular([
                [
                    "+merlan #flirora",
                    "+merljor #flirorac",
                    "+merlin #fliroro",
                    "+merlu #flirorał",
                ],
                [
                    "+merlana #fliroran",
                    "+merelnar #flirorôr",
                    "+merlar #flirorar",
                    "+merlan #flirorełen",
                ],
                [
                    "+merlans #fliroras",
                    "+merlaŋa #flirortas",
                    "+merleri #flirorai",
                    "+merlas #flirorałes",
                ],
                [
                    "+merlil #fliroren",
                    "+mereltil v#flirorten",
                    "+merlevi v#flirorin",
                    "+merelne #flirorneł",
                ],
                [
                    "+mirłes #flieras",
                    "+mirłec #flierac",
                    "+mirłis #flieros",
                    "+mirłeł #fliereł",
                ],
                [
                    "+cjamirłes #fliereca",
                    "+cjamirłecþ #flierecca",
                    "+cjamirło #flierica",
                    "+cjamirłeł #flierełca",
                ],
                [
                    "+þjam·irłes #fliereþa",
                    "+þjam·irłecþ #flierecþa",
                    "+þjam·irło #flieriþa",
                    "+þjam·irłeł #flierełta",
                ],
                [
                    "+merlit #flirolit",
                    "+merlet #flirolet",
                    "+merlet #flirolet",
                    "+merlicþ #flirolicþ",
                ],
            ]),
        );
    }

    fn venrit() -> VerbForm {
        VerbForm {
            i: a!("venr").into(),
            n: a!("venr").into(),
            p: a!("venr").into(),
            r: a!("venr").into(),
            q: a!("venr").into(),
            l: a!("vinr").into(),
            theta: Vowel::I,
            material: Material::Vitreous(VitreousIrregular::None),
            species: Species::I1(I1Subspecies::VxOs, Vowel::A, InstAbessEndings::Beta),
        }
    }

    fn recthit() -> VerbForm {
        VerbForm {
            i: a!("rêcþ").into(),
            n: a!("rêcþ").into(),
            p: a!("rêcþ").into(),
            r: a!("vrêcþ").into(),
            q: a!("vrêcþ").into(),
            l: a!("rôcþ").into(),
            theta: Vowel::I,
            material: Material::Vitreous(VitreousIrregular::None),
            species: Species::I1(I1Subspecies::VxOs, Vowel::A, InstAbessEndings::Beta),
        }
    }

    fn gharat() -> VerbForm {
        VerbForm {
            i: a!("ħar").into(),
            n: a!("ħar").into(),
            p: a!("ħar").into(),
            r: a!("ħar").into(),
            q: a!("ħar").into(),
            l: a!("ħor").into(),
            theta: Vowel::A,
            material: Material::Resinous(ResinousIrregular::None),
            species: Species::I1(I1Subspecies::VxOs, Vowel::E, InstAbessEndings::Alpha),
        }
    }

    fn censit() -> VerbForm {
        VerbForm {
            i: a!("cens").into(),
            n: a!("cins").into(),
            p: a!("cens").into(),
            r: a!("cins").into(),
            q: a!("cens").into(),
            l: a!("cest").into(),
            theta: Vowel::I,
            material: Material::Resinous(ResinousIrregular::None),
            species: Species::I1(I1Subspecies::VxOs, Vowel::A, InstAbessEndings::Alpha),
        }
    }

    #[test]
    fn conjugate_verb_fin_vitreous() {
        let verb = venrit();
        let fin = verb.conjugate_finite().expect("finite conjugation failed");
        assert_eq!(
            *fin.form(FiniteVerbCategories {
                subj_marking: SubjectType::S1,
                obj_marking: ObjectType::None,
                tense: Tense::Present,
                aspect: Aspect::Imperfective,
            }),
            phr!("venre")
        );
        assert_eq!(
            *fin.form(FiniteVerbCategories {
                subj_marking: SubjectType::P1I,
                obj_marking: ObjectType::S2,
                tense: Tense::Present,
                aspect: Aspect::Imperfective,
            }),
            phr!("venrâþfe")
        );
        assert_eq!(
            *fin.form(FiniteVerbCategories {
                subj_marking: SubjectType::S3,
                obj_marking: ObjectType::None,
                tense: Tense::Past,
                aspect: Aspect::Imperfective,
            }),
            phr!("venralta")
        );
        assert_eq!(
            *fin.form(FiniteVerbCategories {
                subj_marking: SubjectType::S2,
                obj_marking: ObjectType::S1,
                tense: Tense::Past,
                aspect: Aspect::Perfective,
            }),
            phr!("venraspeþ")
        );
        assert_eq!(
            *fin.form(FiniteVerbCategories {
                subj_marking: SubjectType::P2,
                obj_marking: ObjectType::None,
                tense: Tense::Past,
                aspect: Aspect::Perfective,
            }),
            phr!("venrarþ")
        );
        assert_eq!(
            *fin.form(FiniteVerbCategories {
                subj_marking: SubjectType::D2,
                obj_marking: ObjectType::Reciprocal,
                tense: Tense::Past,
                aspect: Aspect::Imperfective,
            }),
            phr!("venrêtansisriþ")
        );
    }

    #[test]
    fn conjugate_verb_fin_vitreous_past_suffix_fixes_oc() {
        let verb = recthit();
        let fin = verb.conjugate_finite().expect("finite conjugation failed");
        assert_eq!(
            *fin.form(FiniteVerbCategories {
                subj_marking: SubjectType::S1,
                obj_marking: ObjectType::None,
                tense: Tense::Present,
                aspect: Aspect::Imperfective,
            }),
            phr!("rêcþe")
        );
        assert_eq!(
            *fin.form(FiniteVerbCategories {
                subj_marking: SubjectType::P1I,
                obj_marking: ObjectType::S2,
                tense: Tense::Present,
                aspect: Aspect::Imperfective,
            }),
            phr!("rêcþâþfe")
        );
        assert_eq!(
            *fin.form(FiniteVerbCategories {
                subj_marking: SubjectType::S3,
                obj_marking: ObjectType::None,
                tense: Tense::Past,
                aspect: Aspect::Imperfective,
            }),
            phr!("rêcþalta")
        );
        assert_eq!(
            *fin.form(FiniteVerbCategories {
                subj_marking: SubjectType::S2,
                obj_marking: ObjectType::S1,
                tense: Tense::Past,
                aspect: Aspect::Perfective,
            }),
            phr!("rêcþaspeþ")
        );
        assert_eq!(
            *fin.form(FiniteVerbCategories {
                subj_marking: SubjectType::P2,
                obj_marking: ObjectType::None,
                tense: Tense::Past,
                aspect: Aspect::Perfective,
            }),
            phr!("rêcþarþ")
        );
        assert_eq!(
            *fin.form(FiniteVerbCategories {
                subj_marking: SubjectType::D2,
                obj_marking: ObjectType::Reciprocal,
                tense: Tense::Past,
                aspect: Aspect::Imperfective,
            }),
            phr!("rêcþtansisriþ")
        );
        assert_eq!(
            *fin.form(FiniteVerbCategories {
                subj_marking: SubjectType::S3,
                obj_marking: ObjectType::None,
                tense: Tense::Past,
                aspect: Aspect::Perfective,
            }),
            // We would rather have deduplication produce
            // rêctaþ for this, but this will have to wait
            // until v9n.
            phr!("rêtaþ")
        );
    }

    #[test]
    fn conjugate_verb_fin_resinous() {
        let verb = gharat();
        let fin = verb.conjugate_finite().expect("finite conjugation failed");
        assert_eq!(
            *fin.form(FiniteVerbCategories {
                subj_marking: SubjectType::S1,
                obj_marking: ObjectType::None,
                tense: Tense::Present,
                aspect: Aspect::Imperfective,
            }),
            phr!("ħara")
        );
        assert_eq!(
            *fin.form(FiniteVerbCategories {
                subj_marking: SubjectType::S3,
                obj_marking: ObjectType::None,
                tense: Tense::Present,
                aspect: Aspect::Imperfective,
            }),
            phr!("ħar")
        );
        assert_eq!(
            *fin.form(FiniteVerbCategories {
                subj_marking: SubjectType::S3,
                obj_marking: ObjectType::P1X,
                tense: Tense::Present,
                aspect: Aspect::Imperfective,
            }),
            phr!("ħaregins")
        );
        assert_eq!(
            *fin.form(FiniteVerbCategories {
                subj_marking: SubjectType::D3,
                obj_marking: ObjectType::S3T,
                tense: Tense::Present,
                aspect: Aspect::Imperfective,
            }),
            phr!("ħarterel")
        );
        assert_eq!(
            *fin.form(FiniteVerbCategories {
                subj_marking: SubjectType::S2,
                obj_marking: ObjectType::None,
                tense: Tense::Present,
                aspect: Aspect::Perfective,
            }),
            phr!("ħarea")
        );
        assert_eq!(
            *fin.form(FiniteVerbCategories {
                subj_marking: SubjectType::S3,
                obj_marking: ObjectType::S1,
                tense: Tense::Present,
                aspect: Aspect::Perfective,
            }),
            phr!("p·eħare")
        );
        assert_eq!(
            *fin.form(FiniteVerbCategories {
                subj_marking: SubjectType::S3,
                obj_marking: ObjectType::None,
                tense: Tense::Past,
                aspect: Aspect::Imperfective,
            }),
            phr!("ħarþel")
        );
        assert_eq!(
            *fin.form(FiniteVerbCategories {
                subj_marking: SubjectType::S2,
                obj_marking: ObjectType::G3,
                tense: Tense::Past,
                aspect: Aspect::Perfective,
            }),
            phr!("ħarteþâ")
        );
        assert_eq!(
            *fin.form(FiniteVerbCategories {
                subj_marking: SubjectType::P3,
                obj_marking: ObjectType::S3C,
                tense: Tense::Past,
                aspect: Aspect::Perfective,
            }),
            phr!("ħartelgi")
        );
    }

    #[test]
    fn conjugate_verb_fin_resinous_pfv_pres_3sg_none_is_eclipsed() {
        let verb = censit();
        let fin = verb.conjugate_finite().expect("finite conjugation failed");
        assert_eq!(
            *fin.form(FiniteVerbCategories {
                subj_marking: SubjectType::S3,
                obj_marking: ObjectType::None,
                tense: Tense::Present,
                aspect: Aspect::Perfective,
            }),
            phr!("gcinsa")
        );
    }

    #[test]
    fn conjugate_verb_transfinite() {
        let verb_v = venrit();
        assert_eq!(verb_v.conjugate_transfinite(), Ok(None));

        let verb_r = gharat();
        let trf = verb_r
            .conjugate_transfinite()
            .expect("transfinite conjugation failed")
            .expect("transfinite forms not returned");

        assert_eq!(
            *trf.form(TransfiniteVerbCategories {
                transfinite: Transfinite::Imp2S,
                obj_marking: ObjectType::None
            }),
            phr!("ħarþau")
        );
        assert_eq!(
            *trf.form(TransfiniteVerbCategories {
                transfinite: Transfinite::Imp2S,
                obj_marking: ObjectType::S1
            }),
            phr!("ħarþaupe")
        );
        assert_eq!(
            *trf.form(TransfiniteVerbCategories {
                transfinite: Transfinite::Supine,
                obj_marking: ObjectType::None
            }),
            phr!("ħarnels")
        );
        assert_eq!(
            *trf.form(TransfiniteVerbCategories {
                transfinite: Transfinite::Supine,
                obj_marking: ObjectType::S3H
            }),
            phr!("ħarnaseli")
        );
        assert_eq!(
            *trf.form(TransfiniteVerbCategories {
                transfinite: Transfinite::ActiveGerundiveFin,
                obj_marking: ObjectType::Reflexive
            }),
            phr!("ħarnelvecin")
        );
        assert_eq!(
            *trf.form(TransfiniteVerbCategories {
                transfinite: Transfinite::ActiveGerundiveAttrT,
                obj_marking: ObjectType::Reflexive
            }),
            phr!("cinħarnełor")
        );
        assert_eq!(
            *trf.form(TransfiniteVerbCategories {
                transfinite: Transfinite::ActiveGerundiveAttrC,
                obj_marking: ObjectType::D1X
            }),
            phr!("palħarnelvin")
        );
        assert_eq!(
            *trf.form(TransfiniteVerbCategories {
                transfinite: Transfinite::PassiveGerundiveFin,
                obj_marking: ObjectType::Reflexive
            }),
            phr!("ħarþelvecin")
        );
        assert_eq!(
            *trf.form(TransfiniteVerbCategories {
                transfinite: Transfinite::PassiveGerundiveAttr,
                obj_marking: ObjectType::S2
            }),
            phr!("veħarþelvin")
        );
    }

    #[test]
    fn inflect_relational() {
        let relational = RelationalForm {
            a: a!("ro").into(),
            v: a!("ro").into(),
            n: a!("ron").into(),
            c: y!("ro").into(),
            object_type: RelationalObjectType::Locative,
            nominalization_type: NominalizationType::TypeII,
            ancilliary_object_case: None,
        };
        let inflections = relational.inflect().expect("conjugation failed");
        assert_eq!(
            inflections.get_attr(RelationalCategories {
                attachment: Attachment::Adnominal,
                motion: MotionType::Static,
                object_type: ObjectType::Reflexive,
                ancillary_object_type: ObjectType::G3,
            }),
            &decw!("ceron")
        );
        assert_eq!(
            inflections.get_fin(FiniteRelationalCategories {
                vc: FiniteVerbCategories {
                    subj_marking: SubjectType::D1X,
                    obj_marking: ObjectType::S3C,
                    tense: Tense::Present,
                    aspect: Aspect::Perfective,
                },
                motion: MotionType::Static,
                polarity: Polarity::Negative,
            }),
            &decw!("rocinle")
        );
    }
    #[test]
    fn inflect_relational_dm() {
        let relational = RelationalForm {
            a: a!("il").into(),
            v: a!("il").into(),
            n: a!("ila").into(),
            c: y!("il").into(),
            object_type: RelationalObjectType::DativeMotion,
            nominalization_type: NominalizationType::TypeI,
            ancilliary_object_case: None,
        };
        let inflections = relational.inflect().expect("conjugation failed");
        assert_eq!(
            inflections.get_attr(RelationalCategories {
                attachment: Attachment::Adverbial,
                motion: MotionType::Allative,
                object_type: ObjectType::S1,
                ancillary_object_type: ObjectType::G3,
            }),
            &decw!("eilar")
        );
        assert_eq!(
            inflections.get_fin(FiniteRelationalCategories {
                vc: FiniteVerbCategories {
                    subj_marking: SubjectType::S2,
                    obj_marking: ObjectType::None,
                    tense: Tense::Present,
                    aspect: Aspect::Imperfective,
                },
                motion: MotionType::Static,
                polarity: Polarity::Affirmative,
            }),
            &decw!("ilves")
        );
    }
    #[test]
    fn inflect_relational_syncope() {
        let relational = RelationalForm {
            a: a!("sêna").into(),
            v: a!("sêna").into(),
            n: a!("sênen").into(),
            c: y!("sên").into(),
            object_type: RelationalObjectType::DativeMotion,
            nominalization_type: NominalizationType::TypeII,
            ancilliary_object_case: None,
        };
        let inflections = relational.inflect().expect("conjugation failed");
        assert_eq!(
            inflections.get_attr(RelationalCategories {
                attachment: Attachment::Adnominal,
                motion: MotionType::Allative,
                object_type: ObjectType::None,
                ancillary_object_type: ObjectType::None,
            }),
            &decw!("sênâra")
        );
        assert_eq!(
            inflections.get_attr(RelationalCategories {
                attachment: Attachment::Adverbial,
                motion: MotionType::Ablative,
                object_type: ObjectType::None,
                ancillary_object_type: ObjectType::None,
            }),
            &decw!("sênajas")
        );
    }
    #[test]
    fn inflect_relational_3v() {
        let relational = RelationalForm {
            a: a!("tfel").into(),
            v: a!("tfest").into(),
            n: a!("tfel").into(),
            c: y!("tfel").into(),
            object_type: RelationalObjectType::DativeMotion,
            nominalization_type: NominalizationType::TypeI,
            ancilliary_object_case: Some(Case::Abessive),
        };
        let inflections = relational.inflect().expect("conjugation failed");
        assert_eq!(
            inflections.get_attr(RelationalCategories {
                attachment: Attachment::Adverbial,
                motion: MotionType::Static,
                object_type: ObjectType::S3C,
                ancillary_object_type: ObjectType::S1,
            }),
            &decw!("ertfestef")
        );
        assert_eq!(
            inflections.get_fin(FiniteRelationalCategories {
                vc: FiniteVerbCategories {
                    subj_marking: SubjectType::S2,
                    obj_marking: ObjectType::None,
                    tense: Tense::Present,
                    aspect: Aspect::Imperfective,
                },
                motion: MotionType::Static,
                polarity: Polarity::Affirmative,
            }),
            &decw!("tfelves")
        );
    }
    #[test]
    fn parse_noun_line() {
        let noun_line = "noun celestial singular I elt ilt eld es a";
        let expected_noun = Noun {
            gender: Gender::Celestial,
            clareth: Clareth::Singular,
            form: NounForm::I(DeclensionI {
                n: a!("elt").into(),
                l: a!("ilt").into(),
                s: a!("eld").into(),
                theta: Vowel::E,
                lambda: Vowel::A,
                kind: first::DeclensionIKind::Vs,
            }),
        };
        let actual_noun = parse::noun_line(noun_line);
        assert_eq!(actual_noun, Ok(("", expected_noun)));
    }
    #[test]
    fn parse_my_name() {
        let noun_line =
            "noun human singular (VI +merl +mirł +mirł +mirł +merl an e | I #fliror #flier #flirol a a)";
        let tor = Decoration {
            flags: DecorationFlags::empty(),
            name_marker: NameMarker::Tor,
        };
        let carth = Decoration {
            flags: DecorationFlags::empty(),
            name_marker: NameMarker::Carth,
        };
        let expected_noun = Noun {
            gender: Gender::Human,
            clareth: Clareth::Singular,
            form: NounForm::Compound(Box::new([
                NounForm::VI(DeclensionVI {
                    n: Decorated {
                        decoration: tor,
                        frag: a!("merl").into(),
                    },
                    l: Decorated {
                        decoration: tor,
                        frag: a!("mirł").into(),
                    },
                    i: Decorated {
                        decoration: tor,
                        frag: a!("mirł").into(),
                    },
                    i_prime: Decorated {
                        decoration: tor,
                        frag: a!("mirł").into(),
                    },
                    s: Decorated {
                        decoration: tor,
                        frag: a!("merl").into(),
                    },
                    theta: Vowel::A,
                    lambda: Vowel::E,
                }),
                NounForm::I(DeclensionI {
                    n: Decorated {
                        decoration: carth,
                        frag: a!("fliror").into(),
                    },
                    l: Decorated {
                        decoration: carth,
                        frag: a!("flier").into(),
                    },
                    s: Decorated {
                        decoration: carth,
                        frag: a!("flirol").into(),
                    },
                    theta: Vowel::A,
                    lambda: Vowel::A,
                    kind: first::DeclensionIKind::V,
                }),
            ])),
        };
        let actual_noun = parse::noun_line(noun_line);
        assert_eq!(actual_noun, Ok(("", expected_noun)));
    }
    #[test]
    fn parse_noun_line_irregular() {
        let noun_line = "noun celestial singular { singular elgren elgjor elgrin elgref elgranen elgranor elgrenin elgrenef elgres elgrecþ elgras elgresef elgrer elgreric elgrir elgrerif eši ešic ešin ešif esar esac esor esaf eþa eþac eþar ecþaf elgrit elgricte elgret elgricþ }";
        let expected_noun = Noun {
            gender: Gender::Celestial,
            clareth: Clareth::Singular,
            form: NounForm::Irregular(Box::new(NounTable::Singular([
                [
                    phr!("elgren"),
                    phr!("elgjor"),
                    phr!("elgrin"),
                    phr!("elgref"),
                ],
                [
                    phr!("elgranen"),
                    phr!("elgranor"),
                    phr!("elgrenin"),
                    phr!("elgrenef"),
                ],
                [
                    phr!("elgres"),
                    phr!("elgrecþ"),
                    phr!("elgras"),
                    phr!("elgresef"),
                ],
                [
                    phr!("elgrer"),
                    phr!("elgreric"),
                    phr!("elgrir"),
                    phr!("elgrerif"),
                ],
                [phr!("eši"), phr!("ešic"), phr!("ešin"), phr!("ešif")],
                [phr!("esar"), phr!("esac"), phr!("esor"), phr!("esaf")],
                [phr!("eþa"), phr!("eþac"), phr!("eþar"), phr!("ecþaf")],
                [
                    phr!("elgrit"),
                    phr!("elgricte"),
                    phr!("elgret"),
                    phr!("elgricþ"),
                ],
            ]))),
        };
        let actual_noun = parse::noun_line(noun_line);
        assert_eq!(actual_noun, Ok(("", expected_noun)));
    }
    #[test]
    fn parse_verb_line() {
        let verb_line =
            "verb intransitive / vitreous šile šile šile a / šile šile I1 e os a / šilj";
        let expected_verb = Verb {
            transitivity: Transitivity::Intransitive,
            form: VerbForm {
                i: a!("šile").into(),
                n: a!("šile").into(),
                p: a!("šile").into(),
                r: a!("šile").into(),
                q: a!("šile").into(),
                l: a!("šilj").into(),
                theta: Vowel::A,
                material: Material::Vitreous(VitreousIrregular::None),
                species: Species::I1(I1Subspecies::VxOs, Vowel::E, InstAbessEndings::Alpha),
            },
        };
        let actual_verb = parse::verb_line(verb_line);
        assert_eq!(actual_verb, Ok(("", expected_verb)));
    }
    #[test]
    fn parse_verb_line_irregular() {
        let verb_line = "verb intransitive / vitreous {apn: ve ven vjaþ vef veac vea ves vesen vełar vełaf veła vełan von ver vel vins vjaþis vesif veacel varel verþ vinse veris vełesaf vełal vełans veron vros} eþ verł vepr i / verł vepr I1 a os b / isp";
        let expected_verb = Verb {
            transitivity: Transitivity::Intransitive,
            form: VerbForm {
                i: a!("eþ").into(),
                n: a!("verł").into(),
                p: a!("vepr").into(),
                r: a!("verł").into(),
                q: a!("vepr").into(),
                l: a!("isp").into(),
                theta: Vowel::I,
                material: Material::Vitreous(VitreousIrregular::Apn(Box::new([
                    [
                        w!("ve").into(),
                        w!("ven").into(),
                        w!("vjaþ").into(),
                        w!("vef").into(),
                        w!("veac").into(),
                        w!("vea").into(),
                        w!("ves").into(),
                        w!("vesen").into(),
                        w!("vełar").into(),
                        w!("vełaf").into(),
                        w!("veła").into(),
                        w!("vełan").into(),
                        w!("von").into(),
                        w!("ver").into(),
                    ],
                    [
                        w!("vel").into(),
                        w!("vins").into(),
                        w!("vjaþis").into(),
                        w!("vesif").into(),
                        w!("veacel").into(),
                        w!("varel").into(),
                        w!("verþ").into(),
                        w!("vinse").into(),
                        w!("veris").into(),
                        w!("vełesaf").into(),
                        w!("vełal").into(),
                        w!("vełans").into(),
                        w!("veron").into(),
                        w!("vros").into(),
                    ],
                ]))),
                species: Species::I1(I1Subspecies::VxOs, Vowel::A, InstAbessEndings::Beta),
            },
        };
        let actual_verb = parse::verb_line(verb_line);
        assert_eq!(actual_verb, Ok(("", expected_verb)));
    }
    #[test]
    fn parse_relational_line() {
        let relational_line = "relational I dative_motion il il ila il";
        let expected_relational = RelationalForm {
            a: a!("il").into(),
            v: a!("il").into(),
            n: a!("ila").into(),
            c: y!("il").into(),
            object_type: RelationalObjectType::DativeMotion,
            nominalization_type: NominalizationType::TypeI,
            ancilliary_object_case: None,
        };
        let actual_relational = parse::relational_line(relational_line);
        assert_eq!(actual_relational, Ok(("", expected_relational)));
    }
    #[test]
    fn parse_relational_line_with_fin_lemma() {
        let relational_line = "relational I dative_motion es es esen el";
        let expected_relational = RelationalForm {
            a: a!("es").into(),
            v: a!("es").into(),
            n: a!("esen").into(),
            c: y!("el").into(),
            object_type: RelationalObjectType::DativeMotion,
            nominalization_type: NominalizationType::TypeI,
            ancilliary_object_case: None,
        };
        let actual_relational = parse::relational_line(relational_line);
        assert_eq!(actual_relational, Ok(("", expected_relational)));
    }
}
