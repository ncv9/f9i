/*!

Types and functions related to collation.

See the [section in the ŊCv9 grammar] for more information.

[section in the ŊCv9 grammar]: https://ncv9.flirora.xyz/grammar/phonology/layer1.html#collation

*/

use f9i_core::mgc::{DecoratedWord, Decoration, Mgc, NameMarker, Phrase};

use crate::headword::Headword;

/// A *collation key* derived from a phrase in Ŋarâþ Crîþ.
///
/// Collation keys can be compared to each other; given two phrases
/// `phrase1` and `phrase2`, `CollationKey::of(phrase1).cmp(CollationKey::of(phrase2))`
/// indicates whether `phrase1` should be collated before or after `phrase2`.
#[derive(Clone, Eq, PartialEq, Ord, PartialOrd, Debug, Hash)]
pub struct CollationKey {
    /// The primary key.
    /// This field is compared first.
    pub primary: Vec<u8>,
    /// The secondary key.
    /// If the primary key of two collation keys are equal, then this field is compared to break the tie.
    pub secondary: Vec<u8>,
}

static COLLATION_ORDER: &[([u8; 2], bool)] = &[
    // hyphen
    ([0, 0], false),
    // vowels
    ([2, 0], false),
    ([6, 0], false),
    ([14, 0], false),
    ([22, 0], false),
    ([30, 0], false),
    ([27, 0], false),
    ([28, 0], false),
    ([29, 0], false),
    ([20, 0], false),
    // j
    ([21, 0], false),
    // plain consonants
    ([1, 0], false),
    ([3, 0], false),
    ([4, 0], false),
    ([5, 0], false),
    ([7, 0], false),
    ([8, 0], false),
    ([9, 0], false),
    ([10, 0], false),
    ([11, 0], false),
    ([12, 0], false),
    ([13, 0], false),
    ([15, 0], false),
    ([16, 0], false),
    ([17, 0], false),
    ([18, 0], false),
    ([19, 0], false),
    ([23, 0], false),
    ([24, 0], false),
    ([25, 0], false),
    ([26, 0], false),
    // lenited consonants
    ([17, 0], true),
    ([18, 0], true),
    ([23, 0], true),
    ([19, 0], true),
    ([1, 0], true),
    ([16, 0], true),
    ([13, 0], true),
    ([15, 0], true),
    ([5, 0], true),
    ([24, 0], true),
    // eclipsed consonants
    ([13, 17], false),
    ([5, 17], false),
    ([23, 18], false),
    ([3, 23], false),
    ([16, 1], false),
    ([4, 16], false),
    ([5, 15], false),
    ([24, 8], false),
    ([11, 12], false),
    ([16, 0], false),
];

impl CollationKey {
    fn append_lit(&mut self, val: u8) {
        self.primary.push(val);
        self.secondary.push(2 * val);
    }

    fn append_mgc(&mut self, mgc: Mgc) {
        let (bytes, lenited) = COLLATION_ORDER[mgc as usize];
        if bytes[0] != 0 {
            self.primary.push(bytes[0]);
            self.secondary.push(2 * bytes[0] + (lenited as u8));
        }
        if bytes[1] != 0 {
            self.append_lit(bytes[1]);
        }
    }

    fn append_frag(&mut self, fragment: &[Mgc]) {
        for mgc in fragment {
            self.append_mgc(*mgc);
        }
    }

    fn append_deco(&mut self, decoration: Decoration) {
        if decoration.has_nef() {
            self.append_lit(35);
        }
        match decoration.name_marker {
            NameMarker::None => (),
            NameMarker::Carth => self.append_lit(31),
            NameMarker::Tor => self.append_lit(32),
            NameMarker::Njor => self.append_lit(33),
            NameMarker::Es => self.append_lit(34),
        }
    }

    fn append_word(&mut self, word: &DecoratedWord) {
        self.append_deco(word.decoration());
        self.append_frag(word.letters());
    }

    fn append_phrase(&mut self, phrase: &Phrase) {
        match phrase {
            Phrase::Word(w) => self.append_word(w),
            Phrase::Append(phs) => {
                for subphrase in phs {
                    self.append_phrase(subphrase);
                }
            }
        }
    }

    fn append_headword_lemma(&mut self, headword: &Headword) {
        match headword {
            Headword::Enumerated {
                items,
                exhaustive: _,
            } => self.append_phrase(&items[0]),
            Headword::Compound(elems) => {
                self.append_headword_lemma(&elems[0]);
                self.append_headword_lemma(&elems[1]);
            }
        }
    }

    fn append_headword(&mut self, headword: &Headword) {
        match headword {
            Headword::Enumerated {
                items,
                exhaustive: _,
            } => {
                let mut first = true;
                for item in items {
                    if !first {
                        self.append_lit(0);
                    }
                    first = false;
                    self.append_phrase(item);
                }
            }
            Headword::Compound(elems) => {
                self.append_headword_lemma(headword);
                self.append_lit(0);
                self.append_lit(0);
                self.append_lit(0);
                self.append_headword(&elems[0]);
                self.append_lit(0);
                self.append_lit(0);
                self.append_headword(&elems[1]);
            }
        }
    }

    fn new() -> CollationKey {
        CollationKey {
            primary: Vec::new(),
            secondary: Vec::new(),
        }
    }

    /// Derives a [collation key](CollationKey) from a [phrase](Phrase).
    pub fn of_phrase(phrase: &Phrase) -> CollationKey {
        let mut result = CollationKey::new();
        result.append_phrase(phrase);
        result
    }

    /// Derives a [collation key](CollationKey) from a [headword](Headword).
    pub fn of_headword(headword: &Headword) -> CollationKey {
        let mut result = CollationKey::new();
        result.append_headword(headword);
        result
    }
}
