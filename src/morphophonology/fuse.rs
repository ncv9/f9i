//! Stem-consonant fusion as proposed by [Project Caladrius].
//!
//! [Project Caladrius]: https://ncv9.flirora.xyz/diary/projects/caladrius.html

use std::{convert::TryFrom, fmt::Display, panic::panic_any};

use f9i_core::{
    assemblage::{
        AStem, Decorated, ParsedWord, SyllabicToGlide, SyllabicToGlideRef, SyllabicToNuclear,
        SyllabicToSyllabic,
    },
    mgc::*,
};
use f9i_ivm_macro::{a, f, w, x, y};

#[derive(Debug, Clone)]
pub struct FusionTodo {
    pub stem: SyllabicToGlide,
    pub fusion_consonant: Option<FusionConsonant>,
}

impl Display for FusionTodo {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self.fusion_consonant {
            None => write!(f, "{} × ε", self.stem.tokens_to_string(),),
            Some(c) => write!(f, "{} × {}", self.stem.tokens_to_string(), c),
        }
    }
}

/*

NOTE TO IMPLEMENTORS:

* Use `cargo run -- magic-mirror --full <stem>` to test stem fusion.
    * Any panic of the type `FusionTodo` will be caught by this command. Any other panic will be propagated.
* `fuse_with_eps` (fuse with ε) is a different function from `fuse` (fuse with t, n, or þ) because the return values are structurally different. Make sure they follow similar rules.
* Prefix implementations of each rule with its label.
* You MUST call `resolve_bridge_with_prev_vowel` when joining together arbitrary coda–initial pairs that did not exist in the original stem, either explicitly or implicitly (using the `+` operator).

*/

/// Performs [stem fusion] between a stem and a fusion consonant.
///
/// The stem must contain at least one syllable, and the final initial must not be lenited. All bridges in `stem` must be valid.
///
/// # Panics
///
/// This function panics if the stem does not contain at least one full syllable, or if the final onset is lenited.
///
/// This function may panic when `stem` contains invalid bridges.
///
/// [stem fusion]: https://ncv9.flirora.xyz/grammar/phonology/layer0.html#stem-fusion
pub fn fuse(stem: AStem, c: FusionConsonant) -> AStem {
    // TODO: narrow type of `stem` to rule these cases out
    assert!(
        !stem.remainder.0.is_lenited(),
        "stem must not end with a lenited onset"
    );
    if stem.tiles.is_empty() {
        panic!(
            "stem `{}` too short; must have at least one complete syllable",
            stem.tokens_to_string()
        );
    }

    let (stem, glide) = stem.pop();
    let c_ons = Onset::of(Initial::Single(c.into()));

    // (FinalJ)
    if glide == Glide::J {
        return stem + f!("i") + c_ons;
    }

    let last_tile = stem.last_tile();
    let coda = last_tile.3;
    let initial = stem.remainder.0;

    // (ValidCoda)
    if let Some(coda) = try_bridge_to_simple_coda(coda, initial) {
        // peelx shouldn’t panic – we already checked that there was at least one tile
        return stem.peel().peelx() + coda + c_ons;
    }

    // (Degeminate)
    let coda_mgcs = Coda::from(coda).as_consonant_slice();
    if coda_mgcs.len() == 1 && Some(&coda_mgcs[0]) == initial.as_consonants().first() {
        return stem.peel().peelx()
            + match coda_mgcs[0] {
                Consonant::R => SimpleCoda::L,
                Consonant::S => SimpleCoda::Th,
                _ => coda,
            }
            + c_ons;
    }

    // (Epenthesis-LC)
    if let Some(kappa_iota) = rl_coda_to_onset(coda)
        && let Some(iota_kappa) = onset_to_coda(initial, false)
    {
        let mut stem = stem.peel();
        stem.last_tile_mut().3 = SimpleCoda::Empty;
        return stem * kappa_iota * Glide::None + Vowel::E + iota_kappa + c_ons;
    }

    // (NasalMerge1)
    if coda == SimpleCoda::N {
        match &initial.as_consonants()[..] {
            [Consonant::C | Consonant::G] => {
                let mut stem = stem;
                stem.last_tile_mut().3 = SimpleCoda::Empty;
                stem.remainder.0 = Initial::Single(Consonant::Ng);
                return fuse(stem * Glide::None, c);
            }
            [Consonant::D] => {
                let mut stem = stem;
                stem.last_tile_mut().3 = SimpleCoda::Empty;
                stem.remainder.0 = Initial::Single(Consonant::N);
                return fuse(stem * Glide::None, c);
            }
            [Consonant::C | Consonant::G, i2] => {
                return fuse(
                    replace_final_bridge_with_ng_c(stem, Initial::Single(*i2)),
                    c,
                )
            }
            [Consonant::D, i2] => {
                let mut stem = stem;
                stem.last_tile_mut().3 = SimpleCoda::N;
                stem.remainder.0 = Initial::Single(*i2);
                return fuse(stem * Glide::None, c);
            }
            _ => (),
        }
    }

    // (NasalMerge2)
    if c == FusionConsonant::N {
        match (coda, initial) {
            (SimpleCoda::Empty, Initial::Single(Consonant::M)) => {
                return stem.peel().peelx() * SimpleCoda::N * initial * Glide::None
            }
            (_, Initial::Single(Consonant::N)) => return stem.peel() + a!("enn"),
            (_, Initial::Single(Consonant::M)) => return stem.peel() + a!("ôm"),
            (_, Initial::Single(initial @ (Consonant::D | Consonant::Dh | Consonant::V))) => {
                let mut stem = stem.peel();
                let last_tile = stem.last_tile_mut();
                last_tile.2 = last_tile.2.invert();
                return stem
                    + Onset::from(if initial == Consonant::V {
                        Consonant::M
                    } else {
                        Consonant::N
                    });
            }
            _ => (),
        }
    }

    // (FricMerge1)
    if let Some(new_coda) = try_fricative_merge(coda, initial) {
        return fuse(stem.peel().peelx() * new_coda + Onset::of(initial), c);
    }
    // (FricMerge2)
    {
        let init_mgcs = initial.as_consonants();
        match init_mgcs[..] {
            [i1, i2] if phi(i2, Initial::Single(c.into())) => {
                return fuse(stem.peel() + Onset::of(Initial::Single(i1)), c);
            }
            [i1] if phi(i1, Initial::Single(c.into())) => {
                return stem.peel() + Onset::from(Consonant::from(c));
            }
            _ => (),
        }
    }

    // (FinalDevoice)
    if matches!(c, FusionConsonant::T | FusionConsonant::Th)
        && let Some(new_init) = final_devoice(initial)
    {
        let mut stem = stem;
        stem.remainder.0 = new_init;
        return fuse(stem * Glide::None, c);
    }

    // (Fvm1)
    /*if coda == SimpleCoda::Empty && is_fvm_initial(initial) {
        let mut stem = stem.peel();
        let sc = &mut stem.last_tile_mut().3;
        *sc = SimpleCoda::F;
        return match (initial, c) {
            (Initial::Cf, FusionConsonant::Th) => {
                *sc = SimpleCoda::Empty;
                stem * Onset::of(Initial::Cth)
            }
            (Initial::Gv, FusionConsonant::Th) => {
                *sc = SimpleCoda::Empty;
                stem * Onset::of(Initial::Gdh)
            }
            _ => stem + c_ons,
        };
    }*/

    // (Cl-Meta)
    if let Some((iota, pi, nu, rho)) = check_clmeta_condition(stem.view()) {
        let SyllabicToGlide {
            mut tiles,
            remainder: _,
        } = stem;
        tiles.pop().unwrap();
        tiles.last_mut().unwrap().3 = iota;
        let stem = SyllabicToSyllabic {
            tiles,
            remainder: (),
        };
        return stem + Onset::of(pi) + nu + rho + c_ons;
    }

    if let Some((pi, _rho)) = get_pi_rho_pair(initial) {
        if coda == SimpleCoda::Empty {
            // (Cl-NoCoda-T), (Cl-NoCoda-N), (Cl-NoCoda-Þ)
            let mut stem = stem.peel();
            return match (pi, c) {
                // (Cl-NoCoda-T)
                (Epf::T, FusionConsonant::T) => stem.peelx() + SimpleCoda::R + Onset::from(pi),
                (_, FusionConsonant::T) => {
                    fuse_with_eps(stem * Onset::from(pi)).inject(Consonant::D)
                }
                // (Cl-NoCoda-N)
                (Epf::D, FusionConsonant::N) => {
                    stem.last_tile_mut().3 = SimpleCoda::R;
                    stem + Onset::from(Consonant::Ng)
                }
                (Epf::C, FusionConsonant::N) => {
                    stem.last_tile_mut().3 = SimpleCoda::S;
                    stem + Onset::from(Consonant::Ng)
                }
                (_, FusionConsonant::N) => fuse(stem * Onset::from(pi), FusionConsonant::N),
                // (Cl-NoCoda-Þ)
                (Epf::Th | Epf::Dh, FusionConsonant::Th) => {
                    stem.peelx() + SimpleCoda::R + Onset::from(pi)
                }
                (_, FusionConsonant::Th) => {
                    fuse_with_eps(stem * Onset::from(pi)).inject(Consonant::Dh)
                }
            };
        } else {
            let mut stem = stem;
            stem.remainder.0 = pi.into();
            return match c {
                // (Cl-N)
                FusionConsonant::N => stem + f!("ê") + c_ons,
                // (Cl-TÞ)
                _ => stem + f!("ô") + c_ons,
            };
        }
    }

    match initial {
        Initial::Single(Consonant::L | Consonant::R) => match c {
            // (L-N)
            FusionConsonant::N => {
                if coda == SimpleCoda::N {
                    let stem = stem.peel();
                    stem + c_ons
                } else {
                    stem + f!("ô") + c_ons
                }
            }
            // (L-TÞ)
            _ => stem + f!("ê") + c_ons,
        },
        // (Šłč)
        Initial::Single(Consonant::Sh | Consonant::Lh | Consonant::Ch) | Initial::Csh => {
            let mut stem = stem;
            let (coda1, initial1) = i_function(coda, initial);
            stem.remainder.0 = initial1;
            stem.last_tile_mut().3 = coda1;
            stem * f!("i") + c_ons
        }
        // (Cþ)
        Initial::Single(Consonant::C) => {
            stem.peel()
                + Onset::of(match c {
                    FusionConsonant::T => Initial::Single(Consonant::T),
                    FusionConsonant::N => Initial::Single(Consonant::Ng),
                    FusionConsonant::Th => Initial::Cth,
                })
        }
        // (Gð)
        Initial::Single(Consonant::G) => {
            if ends_with_voiceless_consonant(coda) {
                stem.peel() + y!("i") + c_ons
            } else {
                stem.peel()
                    + Onset::of(match c {
                        FusionConsonant::T => Initial::Single(Consonant::D),
                        FusionConsonant::N => Initial::Single(Consonant::Ng),
                        FusionConsonant::Th => Initial::Gdh,
                    })
            }
        }
        Initial::Single(Consonant::P) => match coda {
            // (P-Nil)
            SimpleCoda::Empty => match stem.last_tile().2 {
                Vowel::E | Vowel::EHat => stem.peel().peelx() * SimpleCoda::F + c_ons,
                _ => stem.peel() * y!("e") + c_ons,
            },
            // (P)
            SimpleCoda::N => stem.peel().peelx() + SimpleCoda::Empty + y!("me") + c_ons,
            _ => stem.peel() + y!("e") + c_ons,
        },
        // (H)
        Initial::Single(Consonant::H) => stem.peel() + Onset::from(Consonant::T),
        // (Ħ)
        Initial::Single(Consonant::Hst) => match coda {
            SimpleCoda::C => {
                let mut stem = stem;
                stem.last_tile_mut().3 = SimpleCoda::Empty;
                stem.remainder.0 = Initial::Single(Consonant::G);
                fuse(stem * Glide::None, c)
            }
            _ => stem.peel() + c_ons,
        },
        Initial::Single(Consonant::Ng) => {
            match coda {
                // (Ŋ-N)
                SimpleCoda::Empty | SimpleCoda::N => {
                    replace_last_nucleus_with_xi(stem.peel().peelx()) * SimpleCoda::R + c_ons
                }
                // (Ŋ-Rl)
                SimpleCoda::R | SimpleCoda::L => {
                    let mut stem = stem.peel();
                    stem.last_tile_mut().3 = SimpleCoda::L;
                    stem + c_ons
                }
                _ => unreachable!("only ε, ⟦r⟧, ⟦n⟧, and ⟦l⟧ are legal codas before ⟦ŋ⟧"),
            }
        }
        _ => {
            // (Ccc)
            if let &[i1, i2] = initial.as_consonants().as_slice() {
                if coda == SimpleCoda::Empty {
                    return stem * f!("î") + c_ons;
                }
                return fuse(
                    stem.peel() * Onset::from(i1) * Vowel::I * SimpleCoda::Empty * Onset::from(i2),
                    c,
                );
            }

            match coda {
                // (Ts)
                SimpleCoda::T => {
                    debug_assert_eq!(initial, Initial::Single(Consonant::S));
                    stem.peel().peelx() * SimpleCoda::S + c_ons
                }
                // (Coda-Rþ), (Coda-Sþf)
                SimpleCoda::RTh | SimpleCoda::S | SimpleCoda::Th | SimpleCoda::F => {
                    let mut stem = stem.peel();
                    let last_tile = stem.last_tile_mut();
                    last_tile.2 = last_tile.2.invert();
                    last_tile.3 = if coda == SimpleCoda::RTh {
                        SimpleCoda::R
                    } else {
                        SimpleCoda::Empty
                    };
                    fuse(stem + Onset::from(initial), c)
                }
                // (Coda-N)
                SimpleCoda::N => {
                    stem.peel().peelx()
                        * match initial {
                            Initial::Single(Consonant::F) => SimpleCoda::F,
                            _ => SimpleCoda::N,
                        }
                        + c_ons
                }
                _ => panic_any(FusionTodo {
                    stem,
                    fusion_consonant: Some(c),
                }),
            }
        }
    }
}

/// Performs [stem fusion] between a stem and a null consonant.
///
/// The stem must contain at least one syllable, and the final initial must not be lenited. All bridges in `stem` must be valid.
///
/// # Panics
///
/// This function panics if the stem does not contain at least one full syllable, or if the final onset is lenited.
///
/// This function may panic when `stem` contains invalid bridges.
///
/// [stem fusion]: https://ncv9.flirora.xyz/grammar/phonology/layer0.html#stem-fusion
pub fn fuse_with_eps(stem: AStem) -> ParsedWord {
    // TODO: narrow type of `stem` to rule these cases out
    assert!(
        !stem.remainder.0.is_lenited(),
        "stem must not end with a lenited onset"
    );
    if stem.tiles.is_empty() {
        panic!(
            "stem `{}` too short; must have at least one complete syllable",
            stem.tokens_to_string()
        );
    }

    let (stem, glide) = stem.pop();

    // (FinalJ)
    if glide == Glide::J {
        return stem + x!("i");
    }

    let last_tile = stem.tiles.last().unwrap();
    let coda = last_tile.3;
    let initial = stem.remainder.0;

    // (Alias)
    if coda != SimpleCoda::Lh && matches!(initial, Initial::Single(Consonant::T | Consonant::D)) {
        return fuse_with_eps(stem.peel() + Onset::from(Consonant::S));
    }

    // (ValidCoda)
    if let Some(coda) = try_bridge_to_coda(coda, initial) {
        return stem.peel().peelx() + coda;
    }

    // (Degeminate)
    let coda_mgcs = Coda::from(coda).as_consonant_slice();
    if coda_mgcs.len() == 1 && Some(&coda_mgcs[0]) == initial.as_consonants().first() {
        return stem.peel().peelx()
            + match coda_mgcs[0] {
                Consonant::R => Coda::L,
                Consonant::S => Coda::Th,
                _ => Coda::from(coda),
            };
    }

    // (NasalMerge1)
    if coda == SimpleCoda::N {
        match &initial.as_consonants()[..] {
            [Consonant::C | Consonant::G] => {
                let mut stem = stem;
                stem.last_tile_mut().3 = SimpleCoda::Empty;
                stem.remainder.0 = Initial::Single(Consonant::Ng);
                return fuse_with_eps(stem * Glide::None);
            }
            [Consonant::D] => {
                let mut stem = stem;
                stem.last_tile_mut().3 = SimpleCoda::Empty;
                stem.remainder.0 = Initial::Single(Consonant::N);
                return fuse_with_eps(stem * Glide::None);
            }
            [Consonant::C | Consonant::G, i2] => {
                return fuse_with_eps(replace_final_bridge_with_ng_c(stem, Initial::Single(*i2)))
            }
            [Consonant::D, i2] => {
                let mut stem = stem;
                stem.last_tile_mut().3 = SimpleCoda::N;
                stem.remainder.0 = Initial::Single(*i2);
                return fuse_with_eps(stem * Glide::None);
            }
            _ => (),
        }
    }

    // (FricMerge1)
    if let Some(new_coda) = try_fricative_merge(coda, initial) {
        return fuse_with_eps(stem.peel().peelx() * new_coda + Onset::of(initial));
    }

    // (FinalDevoice)
    if let Some(new_init) = final_devoice(initial) {
        let mut stem = stem;
        stem.remainder.0 = new_init;
        return fuse_with_eps(stem * Glide::None);
    }

    // (Cl-Meta)
    if let Some((iota, pi, nu, rho)) = check_clmeta_condition(stem.view()) {
        let SyllabicToGlide {
            mut tiles,
            remainder: _,
        } = stem;
        tiles.pop().unwrap();
        tiles.last_mut().unwrap().3 = iota;
        let stem = SyllabicToSyllabic {
            tiles,
            remainder: (),
        };
        return stem + Onset::of(pi) + nu + Coda::from(rho);
    }

    // (Epenthesis-LC)
    if let Some(kappa_iota) = rl_coda_to_onset(coda)
        && let Some(iota_kappa) = onset_to_coda(initial, true)
    {
        let mut stem = stem.peel();
        stem.last_tile_mut().3 = SimpleCoda::Empty;
        return stem * kappa_iota * Glide::None + Vowel::E + Coda::from(iota_kappa);
    }

    // (Fvm1)
    /*if coda == SimpleCoda::Empty && is_fvm_initial(initial) {
        return stem.peel().peelx() * Coda::F;
    }*/

    // (Cl-Nil)
    if let Some((pi, rho)) = get_pi_rho_pair(initial) {
        let mut stem = stem;
        stem.remainder.0 = Initial::from(pi);
        return stem
            + match rho {
                SimpleCoda::R => x!("ôr"),
                SimpleCoda::L => x!("êl"),
                _ => unreachable!(),
            };
    }

    match initial {
        // (L-Nil)
        Initial::Single(Consonant::R) => stem.peel() + Initial::Empty + x!("ôr"),
        Initial::Single(Consonant::L) => stem.peel() + Initial::Empty + x!("êl"),
        // (Šłč)
        Initial::Single(Consonant::Sh | Consonant::Lh | Consonant::Ch) | Initial::Csh => {
            let mut stem = stem;
            let (coda1, initial1) = i_function(coda, initial);
            stem.remainder.0 = initial1;
            stem.last_tile_mut().3 = coda1;
            stem * x!("i")
        }
        // (Cþ)
        Initial::Single(Consonant::C) => {
            stem.peel().peelx()
                + match coda {
                    SimpleCoda::S | SimpleCoda::Th => Coda::CTh,
                    _ => coda.into(),
                }
        }
        Initial::Single(Consonant::G) => match coda {
            // (G-Nil)
            SimpleCoda::Empty => match stem.last_tile().2 {
                Vowel::A | Vowel::E | Vowel::AHat | Vowel::EHat => stem.peel() * w!("i"),
                _ => stem.peel().peelx() * Coda::S,
            },
            // (G)
            _ => stem.peel() + w!("i"),
        },
        Initial::Single(Consonant::P) => match coda {
            // (P-Nil)
            SimpleCoda::Empty => match stem.last_tile().2 {
                Vowel::E | Vowel::EHat => stem.peel().peelx() * Coda::F,
                _ => stem.peel() * w!("e"),
            },
            // (P)
            SimpleCoda::N => stem.peel().peelx() + SimpleCoda::Empty + w!("me"),
            _ => stem.peel() + w!("e"),
        },
        // (H)
        Initial::Single(Consonant::H) => match coda {
            SimpleCoda::N => stem.peel().peelx() * Coda::Ns,
            SimpleCoda::L => stem.peel().peelx() * Coda::Ls,
            _ => {
                replace_last_nucleus_with_xi(stem.peel().peelx())
                    * Coda::from(if coda == SimpleCoda::Empty {
                        SimpleCoda::S
                    } else {
                        coda
                    })
            }
        },
        // (Ħ)
        Initial::Single(Consonant::Hst) => match coda {
            SimpleCoda::C => {
                let mut stem = stem;
                stem.last_tile_mut().3 = SimpleCoda::Empty;
                stem.remainder.0 = Initial::Single(Consonant::G);
                fuse_with_eps(stem * Glide::None)
            }
            _ => {
                stem.peel().peelx()
                    * Coda::from(if coda == SimpleCoda::Empty {
                        SimpleCoda::S
                    } else {
                        coda
                    })
            }
        },
        Initial::Single(Consonant::Ng) => match coda {
            // (Ŋ-N)
            SimpleCoda::Empty | SimpleCoda::N => {
                replace_last_nucleus_with_xi(stem.peel().peelx()) * Coda::R
            }
            // (Ŋ-Rl)
            SimpleCoda::R | SimpleCoda::L => {
                let mut stem = stem.peel();
                stem.last_tile_mut().3 = SimpleCoda::Empty;
                stem * w!("lôr")
            }
            _ => unreachable!("only ε, ⟦r⟧, ⟦n⟧, and ⟦l⟧ are legal codas before ⟦ŋ⟧"),
        },
        _ => {
            // (Ccc)
            if let &[i1, i2] = initial.as_consonants().as_slice() {
                return fuse_with_eps(
                    stem.peel() * Onset::from(i1) * Vowel::I * SimpleCoda::Empty * Onset::from(i2),
                );
            }

            match coda {
                // (Ts)
                SimpleCoda::T => {
                    debug_assert_eq!(initial, Initial::Single(Consonant::S));
                    stem.peel().peelx() * Coda::S
                }
                // (Coda-Rþ), (Coda-Sþf)
                SimpleCoda::RTh | SimpleCoda::S | SimpleCoda::Th | SimpleCoda::F => {
                    let mut stem = stem.peel();
                    let last_tile = stem.last_tile_mut();
                    last_tile.2 = last_tile.2.invert();
                    last_tile.3 = if coda == SimpleCoda::RTh {
                        SimpleCoda::R
                    } else {
                        SimpleCoda::Empty
                    };
                    fuse_with_eps(stem + Onset::from(initial))
                }
                // (Coda-N)
                SimpleCoda::N => {
                    stem.peel().peelx()
                        * match initial {
                            Initial::Single(Consonant::F) => Coda::F,
                            _ => Coda::N,
                        }
                }
                _ => panic_any(FusionTodo {
                    stem,
                    fusion_consonant: None,
                }),
            }
        }
    }
}

fn try_bridge_to_coda(coda: SimpleCoda, initial: Initial) -> Option<Coda> {
    match (coda, initial) {
        (SimpleCoda::Empty, Initial::Empty) => Some(Coda::Empty),
        (SimpleCoda::Empty, Initial::Single(Consonant::S)) => Some(Coda::S),
        (SimpleCoda::Empty, Initial::Single(Consonant::R)) => Some(Coda::R),
        (SimpleCoda::Empty, Initial::Single(Consonant::N)) => Some(Coda::N),
        (SimpleCoda::Empty, Initial::Single(Consonant::Th)) => Some(Coda::Th),
        (SimpleCoda::Empty, Initial::Single(Consonant::L)) => Some(Coda::L),
        (SimpleCoda::Empty, Initial::Single(Consonant::T)) => Some(Coda::T),
        (SimpleCoda::Empty, Initial::Single(Consonant::C)) => Some(Coda::C),
        (SimpleCoda::Empty, Initial::Single(Consonant::F)) => Some(Coda::F),
        (SimpleCoda::Empty, Initial::Single(Consonant::Lh)) => Some(Coda::Lh),
        (SimpleCoda::R, Initial::Single(Consonant::Th)) => Some(Coda::RTh),
        (SimpleCoda::N, Initial::Single(Consonant::Th)) => Some(Coda::NTh),
        (SimpleCoda::Empty, Initial::Cth) => Some(Coda::CTh),
        (SimpleCoda::S, Initial::Single(Consonant::T)) => Some(Coda::St),
        (SimpleCoda::L, Initial::Single(Consonant::T)) => Some(Coda::Lt),
        (SimpleCoda::Lh, Initial::Single(Consonant::T)) => Some(Coda::Lht),
        (SimpleCoda::N, Initial::Single(Consonant::S)) => Some(Coda::Ns),
        (SimpleCoda::L, Initial::Single(Consonant::S)) => Some(Coda::Ls),
        _ => None,
    }
}

fn try_bridge_to_simple_coda(coda: SimpleCoda, initial: Initial) -> Option<SimpleCoda> {
    try_bridge_to_coda(coda, initial).and_then(|coda| SimpleCoda::try_from(coda).ok())
}

fn clmeta_initial_to_coda(initial: Initial) -> Option<SimpleCoda> {
    match initial {
        Initial::Empty => Some(SimpleCoda::Empty),
        Initial::Single(Consonant::S) => Some(SimpleCoda::S),
        Initial::Single(Consonant::N) => Some(SimpleCoda::N),
        Initial::Single(Consonant::L) => Some(SimpleCoda::L),
        _ => None,
    }
}

fn get_pi_rho_pair(initial: Initial) -> Option<(Epf, SimpleCoda)> {
    match initial {
        Initial::R(c) => Some((c, SimpleCoda::R)),
        Initial::L(c) => Some((c, SimpleCoda::L)),
        _ => None,
    }
}

// (iota, pi, nu, rho)
fn check_clmeta_condition(
    stem: SyllabicToGlideRef,
) -> Option<(SimpleCoda, Initial, Vowel, SimpleCoda)> {
    match stem {
        SyllabicToGlideRef {
            tiles: &[.., (_, _, _, SimpleCoda::Empty), (iota, Glide::None, nu, SimpleCoda::Empty)],
            remainder: (initial,),
        } => {
            if let Some((pi, rho)) = get_pi_rho_pair(initial)
                && let Some(iota_kappa) = clmeta_initial_to_coda(iota)
            {
                Some((iota_kappa, Initial::Single(pi.into()), nu, rho))
            } else {
                None
            }
        }
        _ => None,
    }
}

fn i_function(coda: SimpleCoda, initial: Initial) -> (SimpleCoda, Initial) {
    match (coda, initial) {
        (SimpleCoda::RTh, Initial::Single(Consonant::Sh)) => {
            (SimpleCoda::R, Initial::Single(Consonant::Th))
        }
        (SimpleCoda::RTh, Initial::Single(Consonant::Ch)) => {
            (SimpleCoda::R, Initial::Single(Consonant::T))
        }
        (SimpleCoda::CTh, Initial::Single(Consonant::Sh)) => {
            (SimpleCoda::Th, Initial::Single(Consonant::Ch))
        }
        (SimpleCoda::L, Initial::Single(Consonant::Lh)) => {
            (SimpleCoda::Empty, Initial::Single(Consonant::Lh))
        }
        (SimpleCoda::T, Initial::Single(Consonant::Ch)) => {
            (SimpleCoda::Empty, Initial::Single(Consonant::Ch))
        }
        _ => (coda, initial),
    }
}

fn replace_last_nucleus_with_xi(mut stem: SyllabicToNuclear) -> SyllabicToNuclear {
    let nucleus = Nucleus {
        glide: stem.remainder.1,
        vowel: stem.remainder.2,
    };
    let nucleus = nucleus.xi();
    stem.remainder.1 = nucleus.glide;
    stem.remainder.2 = nucleus.vowel;
    stem
}

fn onset_to_coda(initial: Initial, is_null_fuse: bool) -> Option<SimpleCoda> {
    match initial {
        Initial::Empty => Some(SimpleCoda::Empty),
        Initial::Single(Consonant::S) => Some(SimpleCoda::S),
        Initial::Single(Consonant::R) => Some(SimpleCoda::R),
        Initial::Single(Consonant::N) => Some(SimpleCoda::N),
        Initial::Single(Consonant::Th) => Some(SimpleCoda::Th),
        Initial::Single(Consonant::L) => Some(SimpleCoda::L),
        Initial::Single(Consonant::T) => Some(SimpleCoda::T),
        Initial::Single(Consonant::C) if is_null_fuse => Some(SimpleCoda::CTh),
        Initial::Single(Consonant::C) => Some(SimpleCoda::C),
        Initial::Single(Consonant::F) => Some(SimpleCoda::F),
        Initial::Cth => Some(SimpleCoda::CTh),
        // -rþ does not correspond to any initial
        _ => None,
    }
}

fn rl_coda_to_onset(coda: SimpleCoda) -> Option<Initial> {
    match coda {
        SimpleCoda::R => Some(Initial::Single(Consonant::R)),
        SimpleCoda::L => Some(Initial::Single(Consonant::L)),
        SimpleCoda::Lh => Some(Initial::Single(Consonant::Lh)),
        _ => None,
    }
}

fn in_f(kappa: Consonant, iota: Consonant) -> bool {
    match (kappa, iota) {
        (Consonant::F | Consonant::T | Consonant::Th, Consonant::Th | Consonant::D) => true,
        (Consonant::F | Consonant::V | Consonant::Th, Consonant::F) => true,
        (Consonant::T, Consonant::T | Consonant::N) => true,
        _ => false,
    }
}

fn phi(kappa: Consonant, iota: Initial) -> bool {
    match iota {
        Initial::Single(iota) => in_f(kappa, iota),
        Initial::R(iota) => in_f(kappa, iota.into()),
        Initial::L(iota) => in_f(kappa, iota.into()),
        _ => false,
    }
}

fn try_fricative_merge(kappa: SimpleCoda, iota: Initial) -> Option<SimpleCoda> {
    match kappa.as_consonant_slice() {
        [] => None,
        [a] => phi(*a, iota).then_some(SimpleCoda::Empty),
        [Consonant::R, Consonant::Th] => phi(Consonant::Th, iota).then_some(SimpleCoda::R),
        [Consonant::C, Consonant::Th] => phi(Consonant::Th, iota).then_some(SimpleCoda::C),
        _ => unreachable!(),
    }
}

fn replace_final_bridge_with_ng_c(mut stem: SyllabicToGlide, c: Initial) -> AStem {
    stem.remainder.0 = c;
    let last_tile = stem.last_tile_mut();
    last_tile.3 = SimpleCoda::R;
    let nucleus = Nucleus {
        glide: last_tile.1,
        vowel: last_tile.2,
    };
    let nucleus = nucleus.xi();
    last_tile.1 = nucleus.glide;
    last_tile.2 = nucleus.vowel;
    stem * Glide::None
}

fn final_devoice(i: Initial) -> Option<Initial> {
    match i {
        Initial::Single(Consonant::V | Consonant::M) => Some(Initial::Single(Consonant::F)),
        Initial::Single(Consonant::D) => Some(Initial::Single(Consonant::L)),
        Initial::Single(Consonant::Dh) => Some(Initial::Single(Consonant::Th)),
        _ => None,
    }
}

fn ends_with_voiceless_consonant(c: SimpleCoda) -> bool {
    match c.as_consonant_slice() {
        [] => false,
        [.., c] => !Mgc::from(*c).get_mgc_properties().voiced,
    }
}

pub fn fuse_d(dw: Decorated<AStem>, c: FusionConsonant) -> Decorated<AStem> {
    Decorated {
        frag: fuse(dw.frag, c),
        decoration: dw.decoration,
    }
}

pub fn fuse_with_eps_d(dw: Decorated<AStem>) -> Decorated<ParsedWord> {
    Decorated {
        frag: fuse_with_eps(dw.frag),
        decoration: dw.decoration,
    }
}
