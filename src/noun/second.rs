//! The second declension in Ŋarâþ Crîþ.

use crate::headword::Headword;
use crate::morphophonology::fuse::fuse_d;
use crate::morphophonology::gencons::GenCons;
use crate::morphophonology::vc2x;
use f9i_core::assemblage::{AStem, Decorated, ParsedWord};
use f9i_core::category::{Case, Clareth, NNumber};
use f9i_core::mgc::{Coda, FusionConsonant, Glide, Phrase, SimpleCoda, Vowel};
use f9i_ivm_macro::{b, nn, nw, phr, x};

use super::base::*;

#[non_exhaustive]
#[derive(Copy, Clone, PartialEq, Eq, Debug)]
pub enum DeclensionIIpKind {
    // ⟦-in⟧ nouns.
    In,
    // ⟦-is⟧ nouns.
    Is,
    // ⟦-Vr⟧ nouns.
    Vxr,
}

impl DeclensionIIpKind {
    fn modify_vowel(self, v: Vowel) -> Vowel {
        match self {
            DeclensionIIpKind::Vxr => v.hat(),
            _ => v,
        }
    }

    fn coda(self) -> SimpleCoda {
        match self {
            DeclensionIIpKind::In => SimpleCoda::N,
            DeclensionIIpKind::Is => SimpleCoda::S,
            DeclensionIIpKind::Vxr => SimpleCoda::R,
        }
    }

    fn nom_pl_vowel(&self) -> Vowel {
        match self {
            DeclensionIIpKind::In | DeclensionIIpKind::Is => Vowel::A,
            DeclensionIIpKind::Vxr => Vowel::IHat,
        }
    }
}

#[non_exhaustive]
#[derive(Copy, Clone, PartialEq, Eq, Debug)]
pub enum DeclensionIIuKind {
    // ⟦-Vr⟧ nouns.
    Vr,
    // ⟦-Vl⟧ nouns.
    Vl,
    // ⟦-Vþ⟧ nouns.
    Vth,
    // ⟦-Vrþ⟧ nouns.
    Vrth,
}

impl DeclensionIIuKind {
    fn coda(self) -> SimpleCoda {
        match self {
            DeclensionIIuKind::Vr => SimpleCoda::R,
            DeclensionIIuKind::Vl => SimpleCoda::L,
            DeclensionIIuKind::Vth => SimpleCoda::Th,
            DeclensionIIuKind::Vrth => SimpleCoda::RTh,
        }
    }
}

/// Describes the nominative ending to use for a second-declension noun.
#[derive(Clone, PartialEq, Eq, Debug)]
pub enum DeclensionIIKind {
    /// Penultimate stress.
    P(DeclensionIIpKind, Decorated<AStem>),
    /// Ultimate stress.
    U(DeclensionIIuKind),
}

impl DeclensionIIKind {
    // nothing yet
}

/// Data to describe a first-declension noun.
#[derive(Clone, PartialEq, Eq, Debug)]
pub struct DeclensionII {
    /// The N stem.
    pub n: Decorated<AStem>,
    /// The L stem.
    pub l: Decorated<AStem>,
    /// The S stem.
    pub s: Decorated<AStem>,
    /// The main thematic vowel.
    pub theta: Vowel,
    /// The locative vowel.
    pub lambda: Vowel,
    /// The ending type.
    pub kind: DeclensionIIKind,
}

impl Declension for DeclensionII {
    type Callback<'a> = impl (Fn(Case, NNumber) -> Decorated<ParsedWord>) + 'a;

    fn decline_inner(&self) -> Self::Callback<'_> {
        let kind = &self.kind;
        let n_n = fuse_d(self.n.clone(), FusionConsonant::N);
        let n_t = fuse_d(self.n.clone(), FusionConsonant::T);
        let n_th = fuse_d(self.n.clone(), FusionConsonant::Th);
        let phi_l = GenCons::for_stem(&self.l);
        move |case, number| match (case, number) {
            (Case::Nominative, NNumber::Direct) => match kind {
                DeclensionIIKind::P(k, _) => {
                    self.n.clone() + vc2x(k.modify_vowel(self.theta), k.coda().into())
                }
                DeclensionIIKind::U(k) => self.n.clone() + vc2x(self.theta, k.coda().into()),
            },
            (Case::Nominative, NNumber::Dual) => match kind {
                DeclensionIIKind::P(_, _) => self.n.clone() + x!("jor"),
                DeclensionIIKind::U(_) => self.n.clone() + vc2x(self.theta, Coda::C),
            },
            (Case::Nominative, NNumber::Plural) => match kind {
                DeclensionIIKind::P(k, _) => {
                    self.n.clone() + vc2x(k.nom_pl_vowel(), k.coda().into())
                }
                DeclensionIIKind::U(DeclensionIIuKind::Vl | DeclensionIIuKind::Vr) => {
                    self.n.clone() + vc2x(self.theta.gamma(), Coda::R)
                }
                DeclensionIIKind::U(k @ (DeclensionIIuKind::Vth | DeclensionIIuKind::Vrth)) => {
                    self.n.clone() + vc2x(Vowel::O, k.coda().into())
                }
            },
            (Case::Nominative, NNumber::Singulative) => {
                n_n.clone() + vc2x(self.theta.lambda(), Coda::N)
            }
            (Case::Nominative, NNumber::Generic) => n_th.clone() + vc2x(self.theta.tau(), Coda::S),
            (Case::Accusative, NNumber::Direct) => match kind {
                DeclensionIIKind::P(DeclensionIIpKind::Vxr, _) => n_n.clone() + x!("el"),
                DeclensionIIKind::P(_, _) => n_n.clone() + x!("e"),
                DeclensionIIKind::U(_) => self.n.clone() + self.theta + nw!("rin"),
            },
            (Case::Accusative, NNumber::Dual) => match kind {
                DeclensionIIKind::P(_, _) => n_n.clone() + x!("ec"),
                DeclensionIIKind::U(_) => {
                    let mut stem = self.n.clone();
                    stem.frag.remainder.1 = Glide::J;
                    stem + self.theta.kappa().hat() + Coda::R
                }
            },
            (Case::Accusative, NNumber::Plural) => self.n.clone() + x!("eri"),
            (Case::Accusative, NNumber::Singulative) => match kind {
                DeclensionIIKind::P(_, _) => self.n.clone() + x!("eħin"),
                DeclensionIIKind::U(_) => n_n.clone() + vc2x(self.theta, Coda::RTh),
            },
            (Case::Accusative, NNumber::Generic) => n_th.clone() + vc2x(self.theta.tau(), Coda::Ns),
            (Case::Dative, NNumber::Direct) => match kind {
                DeclensionIIKind::P(_, _) => n_t.clone() + x!("ês"),
                DeclensionIIKind::U(_) => self.n.clone() + vc2x(self.theta, Coda::Ls),
            },
            (Case::Dative, NNumber::Dual) => match kind {
                DeclensionIIKind::P(_, _) => self.n.clone() + x!("ecþ"),
                DeclensionIIKind::U(_) => n_t.clone() + x!("el"),
            },
            (Case::Dative, NNumber::Plural) => match kind {
                DeclensionIIKind::P(_, _) => self.n.clone() + x!("erþ"),
                DeclensionIIKind::U(_) => self.n.clone() + x!("ari"),
            },
            (Case::Dative, NNumber::Singulative) => match kind {
                DeclensionIIKind::P(_, _) => self.n.clone() + x!("erin"),
                DeclensionIIKind::U(_) => n_n.clone() + vc2x(self.theta, Coda::Ls),
            },
            (Case::Dative, NNumber::Generic) => n_th.clone() + vc2x(self.theta.tau(), Coda::Th),
            (Case::Genitive, NNumber::Direct) => match kind {
                DeclensionIIKind::P(DeclensionIIpKind::Vxr, g) => g.clone() + x!("il"),
                DeclensionIIKind::P(_, g) => g.clone() + x!("en"),
                DeclensionIIKind::U(_) => self.n.clone() + self.theta.tau() + nw!("i"),
            },
            (Case::Genitive, NNumber::Dual) => match kind {
                DeclensionIIKind::P(DeclensionIIpKind::Vxr, g) => {
                    fuse_d(g.clone(), FusionConsonant::T) + x!("il")
                }
                DeclensionIIKind::P(_, g) => g.clone() + x!("jor"),
                DeclensionIIKind::U(_) => self.n.clone() + self.theta.tau() + nw!("ci"),
            },
            (Case::Genitive, NNumber::Plural) => match kind {
                DeclensionIIKind::P(DeclensionIIpKind::Vxr, g) => g.clone() + x!("evi"),
                DeclensionIIKind::P(_, g) => g.clone() + x!("eþ"),
                DeclensionIIKind::U(_) => self.n.clone() + self.theta.tau() + nw!("vi"),
            },
            (Case::Genitive, NNumber::Singulative) => match kind {
                DeclensionIIKind::P(DeclensionIIpKind::Vxr, g) => {
                    fuse_d(g.clone(), FusionConsonant::N) + x!("il")
                }
                DeclensionIIKind::P(_, g) => fuse_d(g.clone(), FusionConsonant::N) + x!("es"),
                DeclensionIIKind::U(_) => self.n.clone() + self.theta.tau() + nw!("ħin"),
            },
            (Case::Genitive, NNumber::Generic) => n_th.clone() + vc2x(self.theta.tau(), Coda::St),
            (Case::Locative, NNumber::Direct) => self.l.clone() + vc2x(self.lambda, Coda::Lt),
            (Case::Locative, NNumber::Dual) => {
                self.l.clone() + self.lambda + b!("lt") + self.theta + Coda::C
            }
            (Case::Locative, NNumber::Plural) => {
                (match kind {
                    DeclensionIIKind::P(_, g) => g,
                    DeclensionIIKind::U(_) => &self.n,
                })
                .clone()
                    + vc2x(self.lambda.ex_e_i(), Coda::Lt)
            }
            (Case::Locative, NNumber::Singulative) => {
                (match kind {
                    DeclensionIIKind::P(_, g) => g,
                    DeclensionIIKind::U(_) => &self.n,
                })
                .clone()
                    + self.lambda.ex_e_i()
                    + nw!("lten")
            }
            (Case::Locative, NNumber::Generic) => self.l.clone() + vc2x(self.lambda, phi_l.into()),
            (Case::Instrumental, NNumber::Direct) => self.l.clone() + self.lambda + nw!("lca"),
            (Case::Instrumental, NNumber::Dual) => self.l.clone() + self.lambda + nw!("lhac"),
            (Case::Instrumental, NNumber::Plural) => self.l.clone() + self.lambda + nw!("lco"),
            (Case::Instrumental, NNumber::Singulative) => {
                self.l.clone() + self.lambda + nw!("lcen")
            }
            (Case::Instrumental, NNumber::Generic) => {
                self.l.clone() + self.lambda + nn!("lca") + Coda::from(phi_l)
            }
            (Case::Abessive, NNumber::Direct) => self.l.clone() + self.lambda + nw!("lþa"),
            (Case::Abessive, NNumber::Dual) => self.l.clone() + self.lambda + nw!("lþac"),
            (Case::Abessive, NNumber::Plural) => self.l.clone() + self.lambda + nw!("lþo"),
            (Case::Abessive, NNumber::Singulative) => self.l.clone() + self.lambda + nw!("lþen"),
            (Case::Abessive, NNumber::Generic) => {
                self.l.clone() + self.lambda + nn!("lþa") + Coda::from(phi_l)
            }
            (Case::Semblative, _) => semblative_c(&self.s, self.theta, number),
        }
    }

    fn get_headword_inner(
        &self,
        _clareth: Clareth,
        decl: impl Fn(Case, NNumber) -> Phrase,
    ) -> Headword {
        Headword::Enumerated {
            items: vec![
                decl(Case::Nominative, NNumber::Direct),
                match self.kind {
                    DeclensionIIKind::P(_, _) => decl(Case::Genitive, NNumber::Direct),
                    DeclensionIIKind::U(_) => phr!("-"),
                },
                decl(Case::Locative, NNumber::Direct),
                decl(Case::Semblative, NNumber::Direct),
            ],
            exhaustive: true,
        }
    }
}
