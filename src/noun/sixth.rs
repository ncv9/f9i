//! The sixth declension in Ŋarâþ Crîþ.

use crate::headword::Headword;
use crate::morphophonology::fuse::fuse_d;
use crate::morphophonology::gencons::GenCons;
use crate::morphophonology::{v2x, vc2x};
use f9i_core::assemblage::{AStem, Decorated, ParsedWord};
use f9i_core::category::{Case, Clareth, NNumber};
use f9i_core::mgc::{Coda, FusionConsonant, Phrase, Vowel};
use f9i_ivm_macro::{b, nw, phr, x};

use super::base::*;

/// Data to describe a first-declension noun.
#[derive(Clone, PartialEq, Eq, Debug)]
pub struct DeclensionVI {
    /// The N stem.
    pub n: Decorated<AStem>,
    /// The L stem.
    pub l: Decorated<AStem>,
    /// The I stem.
    pub i: Decorated<AStem>,
    /// The I′ stem.
    pub i_prime: Decorated<AStem>,
    /// The S stem.
    pub s: Decorated<AStem>,
    /// The main thematic vowel.
    pub theta: Vowel,
    /// The locative vowel.
    pub lambda: Vowel,
}

impl Declension for DeclensionVI {
    type Callback<'a> = impl (Fn(Case, NNumber) -> Decorated<ParsedWord>) + 'a;

    fn decline_inner(&self) -> Self::Callback<'_> {
        let n_n = fuse_d(self.n.clone(), FusionConsonant::N);
        let n_t = fuse_d(self.n.clone(), FusionConsonant::T);

        let phi_l = GenCons::for_stem(&self.l);

        let in_ = match self.theta {
            Vowel::I => x!("ien"),
            _ => x!("in"),
        };
        let (cja_i, thja_i) = istems(&self.i);
        let (cja_ip, thja_ip) = istems(&self.i_prime);
        move |case, number| match (case, number) {
            (Case::Nominative, NNumber::Direct) => self.n.clone() + vc2x(self.theta, Coda::N),
            (Case::Nominative, NNumber::Dual) => self.n.clone() + x!("jor"),
            (Case::Nominative, NNumber::Plural) => self.n.clone() + in_,
            (Case::Nominative, NNumber::Singulative) => {
                self.n.clone() + vc2x(self.theta.gamma(), Coda::L)
            }
            (Case::Nominative, NNumber::Generic) => self.n.clone() + x!("u"),
            (Case::Accusative, NNumber::Direct) => {
                self.n.clone()
                    + self.theta
                    + b!("n")
                    + v2x(match self.theta {
                        Vowel::I => Vowel::E,
                        _ => Vowel::A,
                    })
            }
            (Case::Accusative, NNumber::Dual) => n_n.clone() + vc2x(self.theta, Coda::R),
            (Case::Accusative, NNumber::Plural) => self.n.clone() + vc2x(self.theta, Coda::R),
            (Case::Accusative, NNumber::Singulative) => {
                self.n.clone()
                    + self.theta
                    + b!("n")
                    + vc2x(
                        match self.theta {
                            Vowel::I => Vowel::E,
                            _ => Vowel::A,
                        },
                        Coda::N,
                    )
            }
            (Case::Accusative, NNumber::Generic) => self.n.clone() + x!("an"),
            (Case::Dative, NNumber::Direct) => self.n.clone() + vc2x(self.theta, Coda::Ns),
            (Case::Dative, NNumber::Dual) => self.n.clone() + self.theta + nw!("ŋa"),
            (Case::Dative, NNumber::Plural) => self.n.clone() + x!("eri"),
            (Case::Dative, NNumber::Singulative) => n_n.clone() + vc2x(self.theta, Coda::S),
            (Case::Dative, NNumber::Generic) => self.n.clone() + x!("as"),
            (Case::Genitive, NNumber::Direct) => self.n.clone() + x!("il"),
            (Case::Genitive, NNumber::Dual) => n_t.clone() + x!("il"),
            (Case::Genitive, NNumber::Plural) => self.n.clone() + x!("evi"),
            (Case::Genitive, NNumber::Singulative) => n_n.clone() + in_,
            (Case::Genitive, NNumber::Generic) => n_n.clone() + x!("e"),
            (Case::Locative, _) => locative_i(&self.l, self.lambda, number, phi_l),
            (Case::Instrumental, NNumber::Direct) => cja_i.clone() + x!("es"),
            (Case::Instrumental, NNumber::Dual) => cja_i.clone() + x!("ecþ"),
            (Case::Instrumental, NNumber::Plural) => cja_ip.clone() + x!("o"),
            (Case::Instrumental, NNumber::Singulative) => cja_ip.clone() + x!("ans"),
            (Case::Instrumental, NNumber::Generic) => cja_i.clone() + vc2x(Vowel::E, phi_l.into()),
            (Case::Abessive, NNumber::Direct) => thja_i.clone() + x!("es"),
            (Case::Abessive, NNumber::Dual) => thja_i.clone() + x!("ecþ"),
            (Case::Abessive, NNumber::Plural) => thja_ip.clone() + x!("o"),
            (Case::Abessive, NNumber::Singulative) => thja_ip.clone() + x!("ans"),
            (Case::Abessive, NNumber::Generic) => thja_i.clone() + vc2x(Vowel::E, phi_l.into()),
            (Case::Semblative, _) => semblative_c(&self.s, self.theta, number),
        }
    }

    fn get_headword_inner(
        &self,
        clareth: Clareth,
        decl: impl Fn(Case, NNumber) -> Phrase,
    ) -> Headword {
        Headword::Enumerated {
            items: vec![
                decl(Case::Nominative, NNumber::Direct),
                decl(Case::Locative, NNumber::Direct),
                decl(Case::Instrumental, NNumber::Direct),
                match clareth {
                    Clareth::Singular | Clareth::Universal => {
                        decl(Case::Instrumental, NNumber::Plural)
                    }
                    Clareth::Collective => decl(Case::Instrumental, NNumber::Singulative),
                    Clareth::Mass => phr!("-"),
                },
                decl(Case::Semblative, NNumber::Direct),
            ],
            exhaustive: true,
        }
    }
}
