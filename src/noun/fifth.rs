//! The fifth declension in Ŋarâþ Crîþ.

use crate::headword::Headword;
use crate::morphophonology::fuse::fuse_d;
use crate::morphophonology::gencons::GenCons;
use crate::morphophonology::vc2x;
use f9i_core::assemblage::{AStem, Decorated, ParsedWord};
use f9i_core::category::{Case, Clareth, NNumber};
use f9i_core::mgc::{
    Coda, Consonant, FusionConsonant, Glide, Initial, Nucleus, Phrase, SimpleCoda, Vowel,
};
use f9i_ivm_macro::{gn, phr, x};

use super::base::*;

fn zhe(sigma: SimpleCoda) -> (SimpleCoda, Initial) {
    match sigma {
        SimpleCoda::Empty => (SimpleCoda::Empty, Initial::Single(Consonant::Hst)),
        SimpleCoda::S | SimpleCoda::R => (SimpleCoda::Empty, Initial::Single(Consonant::R)),
        SimpleCoda::N => (SimpleCoda::N, Initial::Single(Consonant::Th)),
        SimpleCoda::Th => (SimpleCoda::Empty, Initial::Single(Consonant::S)),
        SimpleCoda::RTh => (SimpleCoda::R, Initial::Single(Consonant::S)),
        SimpleCoda::L => (SimpleCoda::Empty, Initial::Single(Consonant::Lh)),
        SimpleCoda::F => (SimpleCoda::Empty, Initial::Single(Consonant::M)),
        SimpleCoda::CTh => (SimpleCoda::C, Initial::Single(Consonant::S)),
        SimpleCoda::T | SimpleCoda::C | SimpleCoda::Lh => {
            unimplemented!("Σ must not be c, t, or ł")
        }
    }
}

const CATER_ONSETS: [Initial; 3] = [
    Initial::Single(Consonant::G),
    Initial::Single(Consonant::D),
    Initial::Single(Consonant::V),
];
const CINQUE_NUCLEI: [Nucleus; 14] = [
    Nucleus::E,
    Nucleus::A,
    Nucleus::I,
    Nucleus::U,
    Nucleus::A,
    Nucleus::E,
    Nucleus::O,
    Nucleus::I,
    Nucleus::JAHAT,
    Nucleus::JEHAT,
    Nucleus::JOHAT,
    Nucleus::JEHAT,
    Nucleus::JAHAT,
    Nucleus::O,
];

/// Data to describe a fifth-declension noun.
#[derive(Clone, PartialEq, Eq, Debug)]
pub struct DeclensionV {
    /// The N stem.
    pub n: Decorated<AStem>,
    /// The S stem.
    pub s: Decorated<AStem>,
    /// The main thematic vowel.
    pub theta: Nucleus,
    /// The thematic coda.
    pub sigma: SimpleCoda,
}

impl Declension for DeclensionV {
    type Callback<'a> = impl (Fn(Case, NNumber) -> Decorated<ParsedWord>) + 'a;

    fn decline_inner(&self) -> Self::Callback<'_> {
        let sigma_zhe = zhe(self.sigma);
        let nthszh = self.n.clone() + self.theta + sigma_zhe.0 + sigma_zhe.1 + Glide::None;
        let nthszh_t = fuse_d(nthszh.clone(), FusionConsonant::T);

        let nom_dir = self.n.clone() + self.theta + Coda::from(self.sigma);
        let ace = [
            Vowel::E,
            Vowel::A,
            Vowel::I,
            Vowel::OHat,
            Vowel::U,
            Vowel::O,
            Vowel::IHat,
            Vowel::EHat,
            Vowel::A,
        ][nom_dir.to_dw().letter_sum().rem_euclid(9) as usize];

        let nom_gc = nthszh.clone() + x!("u");
        let deuce = [Vowel::O, Vowel::OHat, Vowel::OHat, Vowel::O]
            [nom_gc.to_dw().letter_sum().rem_euclid(4) as usize];

        let nom_du = if self.n.frag.tiles.is_empty() {
            self.n.clone() + deuce + Coda::T
        } else {
            let n_t = fuse_d(self.n.clone(), FusionConsonant::T);
            n_t + deuce + Coda::C
        };
        let (trey, treyx) = match nom_du.to_dw().letter_sum().rem_euclid(4) / 2 {
            0 => (Vowel::I, Vowel::E),
            1 => (Vowel::E, Vowel::I),
            _ => unreachable!(),
        };

        let acc_dir = nthszh.clone() + trey + Coda::N;
        let idx = acc_dir
            .to_dw()
            .letter_sum()
            .rem_euclid(CATER_ONSETS.len() as u32) as usize;
        // we only need to bump the index at most once,
        // since the same consonant never occurs twice in a row
        let cater = if CATER_ONSETS[idx] != self.n.frag.remainder.0 {
            CATER_ONSETS[idx]
        } else {
            CATER_ONSETS[(idx + 1) % CATER_ONSETS.len()]
        };

        let acc_pl =
            self.n.clone() + ace + sigma_zhe.0 + sigma_zhe.1 + Nucleus::of(treyx) + Coda::N;
        let idx = acc_pl
            .to_dw()
            .letter_sum()
            .rem_euclid(CINQUE_NUCLEI.len() as u32) as usize;
        // we only need to bump the index at most once,
        // since the same nucleus never occurs twice in a row
        let cinque = if CINQUE_NUCLEI[idx] != self.theta {
            CINQUE_NUCLEI[idx]
        } else {
            CINQUE_NUCLEI[(idx + 1) % CINQUE_NUCLEI.len()]
        };
        let l = self.n.clone() + cinque + sigma_zhe.0 + sigma_zhe.1 + Glide::None;

        let phi_l = GenCons::for_stem(&l);

        move |case, number| match (case, number) {
            (Case::Nominative, NNumber::Direct) => nom_dir.clone(),
            (Case::Nominative, NNumber::Dual) => nom_du.clone(),
            (Case::Nominative, NNumber::Plural) => self.n.clone() + ace + Coda::from(self.sigma),
            (Case::Nominative, NNumber::Singulative) => nthszh_t.clone() + x!("e"),
            (Case::Nominative, NNumber::Generic) => nom_gc.clone(),
            (Case::Accusative, NNumber::Direct) => acc_dir.clone(),
            (Case::Accusative, NNumber::Dual) => nthszh.clone() + x!("jor"),
            (Case::Accusative, NNumber::Plural) => acc_pl.clone(),
            (Case::Accusative, NNumber::Singulative) => nthszh_t.clone() + x!("en"),
            (Case::Accusative, NNumber::Generic) => nthszh.clone() + x!("an"),
            (Case::Dative, NNumber::Direct) => nthszh.clone() + x!("er"),
            (Case::Dative, NNumber::Dual) => {
                self.n.clone()
                    + self.theta
                    + SimpleCoda::Empty
                    + cater
                    + Nucleus::A
                    + Coda::from(self.sigma)
            }
            (Case::Dative, NNumber::Plural) => nthszh.clone() + x!("ir"),
            (Case::Dative, NNumber::Singulative) => nthszh_t.clone() + x!("es"),
            (Case::Dative, NNumber::Generic) => nthszh.clone() + x!("as"),
            (Case::Genitive, NNumber::Direct) => nthszh.clone() + x!("es"),
            (Case::Genitive, NNumber::Dual) => nthszh.clone() + x!("ec"),
            (Case::Genitive, NNumber::Plural) => nthszh.clone() + x!("eris"),
            (Case::Genitive, NNumber::Singulative) => nthszh_t.clone() + x!("el"),
            (Case::Genitive, NNumber::Generic) => nthszh.clone() + x!("e"),
            (Case::Locative, NNumber::Direct) => l.clone() + x!("a"),
            (Case::Locative, NNumber::Dual) => l.clone() + x!("ac"),
            (Case::Locative, NNumber::Plural) => l.clone() + x!("o"),
            (Case::Locative, NNumber::Singulative) => l.clone() + x!("en"),
            (Case::Locative, NNumber::Generic) => l.clone() + vc2x(Vowel::A, phi_l.into()),
            (Case::Instrumental, NNumber::Direct) => l.clone() + x!("eca"),
            (Case::Instrumental, NNumber::Dual) => l.clone() + x!("ehac"),
            (Case::Instrumental, NNumber::Plural) => l.clone() + x!("ego"),
            (Case::Instrumental, NNumber::Singulative) => l.clone() + x!("egen"),
            (Case::Instrumental, NNumber::Generic) => l.clone() + gn!("eca") + Coda::from(phi_l),
            (Case::Abessive, NNumber::Direct) => l.clone() + x!("eþa"),
            (Case::Abessive, NNumber::Dual) => l.clone() + x!("eþac"),
            (Case::Abessive, NNumber::Plural) => l.clone() + x!("eðo"),
            (Case::Abessive, NNumber::Singulative) => l.clone() + x!("eðen"),
            (Case::Abessive, NNumber::Generic) => l.clone() + gn!("eþa") + Coda::from(phi_l),
            (Case::Semblative, _) => semblative_t(&self.s, number),
        }
    }

    fn get_headword_inner(
        &self,
        clareth: Clareth,
        decl: impl Fn(Case, NNumber) -> Phrase,
    ) -> Headword {
        Headword::Enumerated {
            items: vec![
                decl(Case::Nominative, NNumber::Direct),
                match clareth {
                    Clareth::Singular | Clareth::Universal => {
                        decl(Case::Nominative, NNumber::Plural)
                    }
                    _ => phr!("-"),
                },
                decl(Case::Accusative, NNumber::Direct),
                decl(Case::Locative, NNumber::Direct),
                decl(Case::Semblative, NNumber::Direct),
            ],
            exhaustive: true,
        }
    }
}
