use base64::{
    alphabet,
    engine::{GeneralPurpose, GeneralPurposeConfig},
    Engine,
};

const ENGINE: GeneralPurpose =
    GeneralPurpose::new(&alphabet::URL_SAFE, GeneralPurposeConfig::new());

pub fn word_label_to_element_id(label: &str) -> String {
    format!("zet-{}", ENGINE.encode(label.as_bytes()))
}
