use std::{collections::HashMap, io};

use crate::{
    collate::CollationKey,
    lexical::{EntryRecord, Inflection},
    output::{html, regex::TRANSLATIONS_HTML},
};
use custom_error::custom_error;
use f9i_core::mgc::Phrase;
use itertools::Itertools;
use rusqlite::{params, Connection};
use smallvec::SmallVec;

use super::regex::create_reverse_index_lookup;

custom_error! {pub SqliteOutputError
    SQLite { source: rusqlite::Error } = "Error with SQLite: {source}",
    IO { source: io::Error } = "Error with I/O: {source}",
}

pub fn write_entry_records(
    conn: &mut Connection,
    records: &[EntryRecord],
    forms: &HashMap<Phrase, SmallVec<[(Inflection, usize); 1]>>,
) -> Result<(), SqliteOutputError> {
    let reverse_lookup = create_reverse_index_lookup(records);
    // We have to make an explicit transaction or each statement will implicitly start a new transaction, tanking performance.
    let tx = conn.transaction()?;
    {
        let mut rowids = Vec::<i64>::with_capacity(records.len());
        // We use FTS4 instead of FTS5 because sql.js is built with FTS4 but not FTS5 enabled by default.
        tx.execute(
            "CREATE VIRTUAL TABLE Entry USING fts4 (
                collation_key1 BLOB NOT NULL,
                collation_key2 BLOB NOT NULL,
                headword TEXT NOT NULL,
                translations TEXT NOT NULL,
                search_body TEXT NOT NULL,
                body TEXT NOT NULL,
                notindexed=body
            );",
            [],
        )?;
        tx.execute(
            "CREATE TABLE Form (
                eid INTEGER NOT NULL,
                form TEXT NOT NULL,
                inflection BLOB NOT NULL,
                PRIMARY KEY (form, eid, inflection),
                FOREIGN KEY (eid) REFERENCES Entry (rowid)
            ) WITHOUT ROWID;",
            [],
        )?;
        let mut insert_entry_stmt = tx.prepare(
            "INSERT INTO Entry (collation_key1, collation_key2, headword, search_body, body, translations)
                VALUES (?1, ?2, ?3, ?4, ?5, ?6);",
        )?;
        for record in records {
            let collation_key = CollationKey::of_headword(&record.headword);
            let hw_s = record.headword.to_string();
            let mut body = Vec::<u8>::new();
            html::write_entry_record(records, &reverse_lookup, record, &mut body)?;
            let mut search_body = Vec::<u8>::new();
            html::write_entry_record_main(records, &reverse_lookup, record, &mut search_body)?;
            let search_body = String::from_utf8(search_body).unwrap();
            let translations = TRANSLATIONS_HTML
                .captures_iter(&search_body)
                .map(|m| m.get(1).unwrap().as_str())
                .join(", ");
            insert_entry_stmt.execute(params![
                collation_key.primary,
                collation_key.secondary,
                hw_s,
                search_body,
                String::from_utf8(body).unwrap(),
                translations,
            ])?;
            rowids.push(tx.last_insert_rowid());
        }
        let mut insert_form_stmt = tx.prepare(
            "INSERT INTO Form (eid, form, inflection)
                VALUES (?1, ?2, ?3);",
        )?;
        for (form, redirects) in forms {
            let form_str = form.to_string();
            for (inflection, redirect_idx) in redirects {
                let inflection_bytes = inflection.to_bytes();
                insert_form_stmt.execute(params![
                    rowids[*redirect_idx],
                    &form_str,
                    &inflection_bytes
                ])?;
            }
        }
    }
    tx.commit()?;
    conn.execute("VACUUM;", [])?;
    Ok(())
}
