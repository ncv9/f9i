use std::collections::HashMap;

use lazy_static::lazy_static;
use regex::Regex;

use crate::lexical::EntryRecord;

lazy_static! {
    pub static ref PERCENT_PATTERN: Regex = Regex::new("%%([A-Z]+)%%").unwrap();
    pub static ref ARK_PATTERN: Regex = Regex::new("\\|%%ARK ([^%]+)%%\\|").unwrap();
    pub static ref PERCENT_PATTERN_HTML: Regex = Regex::new("&%%([A-Z]+)%%;").unwrap();
    pub static ref ARK_PATTERN_HTML: Regex = Regex::new("&%%ARK ([^%]+)%%;").unwrap();
    pub static ref TRANSLATIONS_HTML: Regex =
        Regex::new(r##"<span class="translations">([^<]*)</span>"##).unwrap();
}

pub fn create_reverse_index_lookup(records: &[EntryRecord]) -> HashMap<&str, usize> {
    records
        .iter()
        .enumerate()
        .map(|(i, r)| (r.label.as_str(), i))
        .collect()
}
