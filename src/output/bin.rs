use byteorder::{LittleEndian, WriteBytesExt};
use f9i_core::mgc::Phrase;
use std::convert::TryFrom;
use std::io::{Error as IOError, ErrorKind, Result as IOResult, Seek, SeekFrom, Write};

use crate::lexical::EntryRecord;
use crate::table::Table;

pub fn vw_int(out: &mut [u8; 5], sz: u32) -> &mut [u8] {
    if sz < (1 << 7) {
        // 0xxx_xxxx
        out[0] = sz as u8;
        &mut out[0..1]
    } else if sz < (1 << 14) {
        // 1xxx_xxxx 0xxx_xxxx
        out[0] = ((sz >> 7) as u8) | 0x80;
        out[1] = (sz & 0x7f) as u8;
        &mut out[0..2]
    } else if sz < (1 << 21) {
        // 1xxx_xxxx 0xxx_xxxx 0xxx_xxxx
        out[0] = ((sz >> 14) as u8) | 0x80;
        out[1] = ((sz >> 7) as u8) | 0x80;
        out[2] = (sz & 0x7f) as u8;
        &mut out[0..3]
    } else if sz < (1 << 28) {
        // 1xxx_xxxx 0xxx_xxxx 0xxx_xxxx 0xxx_xxxx
        out[0] = ((sz >> 21) as u8) | 0x80;
        out[1] = ((sz >> 14) as u8) | 0x80;
        out[2] = ((sz >> 7) as u8) | 0x80;
        out[3] = (sz & 0x7f) as u8;
        &mut out[0..4]
    } else {
        // 1xxx_xxxx 0xxx_xxxx 0xxx_xxxx 0xxx_xxxx
        out[0] = ((sz >> 28) as u8) | 0x80;
        out[1] = ((sz >> 21) as u8) | 0x80;
        out[2] = ((sz >> 14) as u8) | 0x80;
        out[3] = ((sz >> 7) as u8) | 0x80;
        out[4] = (sz & 0x7f) as u8;
        &mut out[0..5]
    }
}

pub fn write_vw_prefix_str<W: Write>(fh: &mut W, s: &str) -> IOResult<()> {
    let size = u32::try_from(s.len()).map_err(|e| IOError::new(ErrorKind::FileTooLarge, e))?;
    let mut b = [0u8; 5];
    let slice = vw_int(&mut b, size);
    fh.write_all(slice)?;
    fh.write_all(s.as_bytes())?;
    Ok(())
}

pub fn write_table<W: Write>(fh: &mut W, table: &Table<Option<Phrase>>) -> IOResult<()> {
    for axis in &table.axes {
        match *axis {
            Some(axis) => fh.write_u8(axis as u8)?,
            None => fh.write_u8(255)?,
        }
    }
    for entry in &table.entries {
        match entry {
            Some(entry) => {
                fh.write_u8(1)?;
                write_vw_prefix_str(fh, &entry.to_string())?;
            }
            None => fh.write_u8(0)?,
        }
    }
    Ok(())
}

pub fn write_entry_record<W: Write>(fh: &mut W, record: &EntryRecord) -> IOResult<()> {
    write_vw_prefix_str(fh, &record.headword.to_string())?;
    write_vw_prefix_str(fh, &record.pos)?;
    write_vw_prefix_str(fh, &record.extra)?;
    for table in &record.tables {
        write_table(fh, table)?;
    }
    Ok(())
}

pub fn write_entry_records<W: Write + Seek>(fh: &mut W, records: &[EntryRecord]) -> IOResult<()> {
    let size =
        u32::try_from(records.len()).map_err(|e| IOError::new(ErrorKind::FileTooLarge, e))?;
    fh.write_u32::<LittleEndian>(size)?;
    let location_offset = fh.stream_position()?;
    fh.seek(SeekFrom::Current(4 * records.len() as i64))?;
    let mut offsets: Vec<u32> = Vec::with_capacity(records.len());
    for record in records {
        let offset = u32::try_from(fh.stream_position()?)
            .map_err(|e| IOError::new(ErrorKind::FileTooLarge, e))?;
        offsets.push(offset);
        write_entry_record(fh, record)?;
    }
    fh.seek(SeekFrom::Start(location_offset))?;
    for offset in offsets {
        fh.write_u32::<LittleEndian>(offset)?;
    }
    fh.seek(SeekFrom::End(0))?;
    Ok(())
}
