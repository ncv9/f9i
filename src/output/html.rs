use std::{
    borrow::Cow,
    collections::HashMap,
    io::{Error as IOError, Result as IOResult, Write},
};

use f9i_core::{category::AxisType, mgc::Phrase};
use html_escape::encode_text;
use regex::Captures;

use crate::{
    lexical::EntryRecord,
    output::{
        helper::word_label_to_element_id,
        regex::{ARK_PATTERN_HTML, PERCENT_PATTERN_HTML},
        tablewalker::TableWalker,
    },
    table::Table,
};

use super::regex::create_reverse_index_lookup;

fn interpolate_extra(
    record: &EntryRecord,
    all_records: &[EntryRecord],
    reverse_lookup: &HashMap<&str, usize>,
) -> String {
    let pp_replaced =
        PERCENT_PATTERN_HTML.replace_all(&record.extra, |caps: &Captures| match &caps[1] {
            "POS" => encode_text(&record.pos),
            "PP" => "%%".into(),
            x => Cow::Owned(format!("%%{x}%%")),
        });
    ARK_PATTERN_HTML
        .replace_all(&pp_replaced, |caps: &Captures| {
            let label = &caps[1];
            encode_text(
                &reverse_lookup
                    .get(label)
                    .map(|i| all_records[*i].headword.to_string())
                    .unwrap_or_else(|| format!("LABEL {label}")),
            )
            .to_string()
        })
        .to_string()
}

struct Walker<'a, W: Write> {
    fh: &'a mut W,
    hw_s: &'a str,
}

impl<'a, W: Write> Walker<'a, W> {
    fn w(&mut self, s: &str) -> IOResult<()> {
        self.fh.write_all(s.as_bytes())
    }
}

impl<'a, W: Write> TableWalker<Option<Phrase>, IOError> for Walker<'a, W> {
    fn begin_table(&mut self, _prior_axes: &[AxisType], _values: &[u8]) -> Result<(), IOError> {
        self.w(r##"<figure><table><thead><tr><th>"##)
    }

    fn visit_first_cell(&mut self, row_type: AxisType, col_type: AxisType) -> Result<(), IOError> {
        self.w(row_type.get_display_name())?;
        self.w(" \\ ")?;
        self.w(col_type.get_display_name())
    }

    fn visit_col_header(&mut self, label: &str) -> Result<(), IOError> {
        self.w("</th><th>")?;
        self.w(label)
    }

    fn begin_body(&mut self, header: bool) -> Result<(), IOError> {
        self.w(if header {
            "</th></tr></thead><tbody>"
        } else {
            "<tbody>"
        })
    }

    fn begin_row(&mut self, label: &str) -> Result<(), IOError> {
        self.w(r##"<tr><th class="rowhead">"##)?;
        self.w(label)?;
        self.w("</th>")
    }

    fn visit_body_cell(&mut self, value: &Option<Phrase>) -> Result<(), IOError> {
        match value {
            Some(value) => {
                self.w(r##"<td><span lang="art-x-ncs-v9">"##)?;
                self.w(&encode_text(&value.to_string()))?;
                self.w(r##"</span></td>"##)
            }
            None => self.w(r##"<td></td>"##),
        }
    }

    fn end_row(&mut self) -> Result<(), IOError> {
        self.w("</tr>")
    }

    fn end_table(&mut self, prior_axes: &[AxisType], values: &[u8]) -> Result<(), IOError> {
        self.w("</tbody></table><figcaption>Inflections for ")?;
        self.w(r##"<span lang="art-x-ncs-v9">"##)?;
        self.w(&encode_text(self.hw_s))?;
        self.w(r##"</span>"##)?;
        if !values.is_empty() {
            self.w(" – ")?;
            let mut first = true;
            for (prior_axis, prefix_val) in prior_axes.iter().zip(values) {
                if !first {
                    self.w(", ")?;
                }
                first = false;
                if prior_axis.wants_label() {
                    self.w(prior_axis.get_display_name())?;
                    self.w(" = ")?;
                }
                self.w(prior_axis.get_axis_labels()[*prefix_val as usize])?;
            }
        }
        self.w("</figcaption></figure>")
    }
}

pub fn write_table<W: Write>(
    fh: &mut W,
    hw_s: &str,
    table: &Table<Option<Phrase>>,
) -> IOResult<()> {
    let mut walker = Walker { fh, hw_s };
    walker.walk_table(table)
}

pub fn write_entry_record_main<W: Write>(
    all_records: &[EntryRecord],
    reverse_lookup: &HashMap<&str, usize>,
    record: &EntryRecord,
    fh: &mut W,
) -> Result<(), IOError> {
    fh.write_all(interpolate_extra(record, all_records, reverse_lookup).as_bytes())
}

pub fn write_entry_record<W: Write>(
    all_records: &[EntryRecord],
    reverse_lookup: &HashMap<&str, usize>,
    record: &EntryRecord,
    fh: &mut W,
) -> Result<(), IOError> {
    let hw_s = record.headword.to_string();
    fh.write_all(br##"<h2 id=""##)?;
    fh.write_all(word_label_to_element_id(&record.label).as_bytes())?;
    fh.write_all(br##""><span lang="art-x-ncs-v9">"##)?;
    fh.write_all(encode_text(&hw_s).as_bytes())?;
    fh.write_all(br##"</span></h2><div class="f9t-input">"##)?;
    fh.write_all(encode_text(&record.input).as_bytes())?;
    fh.write_all(br##"</div>"##)?;
    write_entry_record_main(all_records, reverse_lookup, record, fh)?;
    if !record.tables.is_empty() {
        fh.write_all(
            concat!(
                r##"<div class="declension-tables"><details>"##,
                r##"<summary>Show inflections</summary>"##
            )
            .as_bytes(),
        )?;
        for table in &record.tables {
            write_table(fh, &hw_s, table)?;
        }
        fh.write_all(br##"</details></div>"##)?;
    }
    Ok(())
}

pub fn write_entry_records<W: Write>(fh: &mut W, records: &[EntryRecord]) -> IOResult<()> {
    let reverse_lookup = create_reverse_index_lookup(records);
    for record in records {
        write_entry_record(records, &reverse_lookup, record, fh)?;
    }
    Ok(())
}
