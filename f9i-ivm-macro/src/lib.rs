//! Procedural macros for f9i's codebase.

use f9i_core::assemblage::{
    GlideToGlide, GlideToGlideRef, GlideToNuclear, GlideToNuclearRef, GlideToOnset,
    GlideToOnsetRef, GlideToSyllabic, GlideToSyllabicRef, GlideToTerminal, GlideToTerminalRef,
    NuclearToNuclear, NuclearToNuclearRef, NuclearToOnset, NuclearToOnsetRef, NuclearToSyllabic,
    NuclearToSyllabicRef, NuclearToTerminal, NuclearToTerminalRef, Parse as AsmParse,
    SyllabicToOnset, SyllabicToOnsetRef, SyllabicToSyllabic, SyllabicToSyllabicRef,
    SyllabicToTerminal, SyllabicToTerminalRef,
};
use f9i_core::mgc::{DecoratedWord, Mgc, NameMarker, Phrase};
use paste::paste;
use proc_macro::TokenStream;
use proc_macro2::TokenStream as TokenStream2;
use quote::{format_ident, quote};
use self_rust_tokenize::SelfRustTokenize;
use std::convert::TryFrom;
use syn::parse::{Error, Parse, ParseStream, Result};
use syn::token::Comma;
use syn::{
    parse_macro_input, Expr, ExprArray, ExprLit, ExprTuple, Ident, Lit, LitStr, Token, Visibility,
};

enum Scl {
    Static,
    Const,
    Let,
}

impl Parse for Scl {
    fn parse(input: ParseStream) -> Result<Self> {
        if input.parse::<Token![static]>().is_ok() {
            return Ok(Scl::Static);
        };
        if input.parse::<Token![const]>().is_ok() {
            return Ok(Scl::Const);
        };
        if input.parse::<Token![let]>().is_ok() {
            return Ok(Scl::Let);
        };
        Err(Error::new(input.span(), "expected static, const, or let"))
    }
}

struct DeclareIvmArrayParse {
    visibility: Visibility,
    scl: Scl,
    id: Ident,
    tokens: ExprTuple,
    array: Expr,
}

impl Parse for DeclareIvmArrayParse {
    fn parse(input: ParseStream) -> Result<Self> {
        let visibility: Visibility = input.parse()?;
        let scl: Scl = input.parse()?;
        let id: Ident = input.parse()?;
        input.parse::<Comma>()?;
        let tokens: ExprTuple = input.parse()?;
        input.parse::<Comma>()?;
        let array: Expr = input.parse()?;
        Ok(DeclareIvmArrayParse {
            visibility,
            scl,
            id,
            tokens,
            array,
        })
    }
}

struct IvmArrayParse {
    tokens: ExprTuple,
    array: Expr,
}

impl Parse for IvmArrayParse {
    fn parse(input: ParseStream) -> Result<Self> {
        let tokens: ExprTuple = input.parse()?;
        input.parse::<Comma>()?;
        let array: Expr = input.parse()?;
        Ok(IvmArrayParse { tokens, array })
    }
}

const MGC_DISPLAY: &[&str] = &[
    "-", "e", "o", "a", "i", "u", "ê", "ô", "â", "î", "j", "c", "n", "ŋ", "v", "s", "þ", "š", "r",
    "l", "ł", "m", "f", "g", "p", "t", "č", "d", "ð", "h", "ħ", "p·", "t·", "d·", "č·", "c·", "g·",
    "m·", "f·", "v·", "ð·",
];

// Probably could be optimimzed to not use a linear search over all
// tokens and MGCs
fn transform_ivm_string(s: &str, keys: &[String]) -> TokenStream2 {
    let mut t = s;
    let mut out: Vec<TokenStream2> = vec![];
    while !t.is_empty() {
        match keys
            .iter()
            .enumerate()
            .filter(|(_, l)| t.starts_with(*l))
            .max_by_key(|(_, l)| l.len())
        {
            Some((i, k)) => {
                t = &t[k.len()..];
                let iu8 = u8::try_from(i).unwrap();
                out.push(quote! {#iu8 + (f9i_core::mgc::Mgc::COUNT as u8)});
            }
            None => {
                let (i, k) = MGC_DISPLAY
                    .iter()
                    .enumerate()
                    .filter(|(_, l)| t.starts_with(*l))
                    .max_by_key(|(_, l)| l.len())
                    .unwrap_or_else(|| panic!("expected token or MGC at the start of {}", t));
                let iu8 = u8::try_from(i).unwrap();
                out.push(quote! {#iu8});
                t = &t[k.len()..];
            }
        }
    }
    quote! {&[#(#out),*]}
}

fn transform_array_expr_and_get_dims(e: Expr, keys: &[String]) -> (TokenStream2, Vec<usize>) {
    match e {
        Expr::Lit(ExprLit {
            lit: Lit::Str(str), ..
        }) => {
            let transformed = transform_ivm_string(str.value().as_str(), keys);
            (
                quote! {
                    unsafe {
                        crate::ivm::IvmExpr::from_raw(#transformed)
                    }
                },
                vec![],
            )
        }
        Expr::Array(ExprArray { elems, .. }) => {
            let (subentries, subdims): (Vec<TokenStream2>, Vec<Vec<usize>>) = elems
                .into_iter()
                .map(|elem| transform_array_expr_and_get_dims(elem, keys))
                .unzip();
            // Check that all of the subentry dimensions agree
            if !subdims.is_empty() && subdims.iter().any(|d| *d != subdims[0]) {
                panic!("subelements have differing dimensions");
            }
            let mut dim = subdims[0].clone();
            dim.push(subentries.len());
            (
                quote! {
                    [#(#subentries),*]
                },
                dim,
            )
        }
        _ => panic!("expected string or array"),
    }
}

/// Declares an IVM table, inferring its type for top-level use.
///
/// This macro takes in an extra parameter before the ones expected by [ivm_array!]: the part of the variable declaration that you would write before the equals sign, excluding the type annotation.
///
/// The example should compile but does not under the documentation tests because it depends on a type in a downstream crate.
///
/// ```ignore
/// # use f9i_ivm_macro::declare_ivm_array;
/// declare_ivm_array! {
///     static NOUN_PARADIGM0,
///     ("N", "L", "S", "0", "1", "2", "3", "4"),
///     [
///         ["N-0", "N-0c", "N-1", "N-0f"],
///         ["N-0n", "N-4te", "N-2r", "N-0fen"],
///         ["N-0s", "N-0s", "N-4i", "N-0fes"],
///         ["N-2n", "N-2c", "N-3n", "N-3f"],
///         ["L-0s", "L-esac", "L-1s", "L-0f"],
///         ["L-eca", "L-ehac", "L-ecta", "L-ec2f"],
///         ["L-eþa", "L-eþac", "L-4þa", "L-eþ2f"],
///         ["S-it", "S-ict0", "S-et", "S-icþ"],
///     ]
/// }
/// ```
#[proc_macro]
pub fn declare_ivm_array(input: TokenStream) -> TokenStream {
    let DeclareIvmArrayParse {
        visibility,
        scl,
        id,
        tokens,
        array,
    } = parse_macro_input!(input as DeclareIvmArrayParse);

    let tokens: Vec<String> = tokens
        .elems
        .into_iter()
        .map(|expr| match expr {
            Expr::Lit(ExprLit {
                lit: Lit::Str(str), ..
            }) => str.value(),
            _ => panic!("expected string"),
        })
        .collect();

    let num_tokens = tokens.len();

    let (array, mut dims) = transform_array_expr_and_get_dims(array, &tokens);
    dims.reverse();

    let ivm_table_type_name = format_ident!("IvmTable{}D", dims.len());

    let scl = match scl {
        Scl::Static => quote! { static },
        Scl::Const => quote! { const },
        Scl::Let => quote! { let },
    };

    let declaration = quote! {
        #visibility #scl #id: crate::ivm::#ivm_table_type_name<#num_tokens, #(#dims),*> =
            crate::ivm::#ivm_table_type_name::from_array(#array);
    };

    // eprintln!("Le array:\n\n{}", declaration);

    declaration.into()
}

/// Constructs an instance of an IVM table.
///
/// This macro takes in two parameters:
///
/// * A tuple of strings. Each string refers to a nonterminal symbol that will be passed in during inflection.
/// * An array expression listing the entries of the table in string form. Note that no eclipsed letters will be parsed here.
///
/// The example should compile but does not under the documentation tests because it depends on a type in a downstream crate.
///
/// ```ignore
/// # use f9i_ivm_macro::ivm_array;
/// let noun_paradigm0 = ivm_array! {
///     ("N", "L", "S", "0", "1", "2", "3", "4"),
///     [
///         ["N-0", "N-0c", "N-1", "N-0f"],
///         ["N-0n", "N-4te", "N-2r", "N-0fen"],
///         ["N-0s", "N-0s", "N-4i", "N-0fes"],
///         ["N-2n", "N-2c", "N-3n", "N-3f"],
///         ["L-0s", "L-esac", "L-1s", "L-0f"],
///         ["L-eca", "L-ehac", "L-ecta", "L-ec2f"],
///         ["L-eþa", "L-eþac", "L-4þa", "L-eþ2f"],
///         ["S-it", "S-ict0", "S-et", "S-icþ"],
///     ]
/// };
/// ```
#[proc_macro]
pub fn ivm_array(input: TokenStream) -> TokenStream {
    let IvmArrayParse { tokens, array } = parse_macro_input!(input as IvmArrayParse);

    let tokens: Vec<String> = tokens
        .elems
        .into_iter()
        .map(|expr| match expr {
            Expr::Lit(ExprLit {
                lit: Lit::Str(str), ..
            }) => str.value(),
            _ => panic!("expected string"),
        })
        .collect();

    let (array, mut dims) = transform_array_expr_and_get_dims(array, &tokens);
    dims.reverse();

    let ivm_table_type_name = format_ident!("IvmTable{}D", dims.len());

    let declaration = quote! {
            crate::ivm::#ivm_table_type_name::from_array(
                #array
            )
    };

    // eprintln!("Le array:\n\n{}", declaration);

    declaration.into()
}

fn mgc_to_token(mgc: Mgc) -> TokenStream2 {
    let id = format_ident!("{}", format!("{mgc:?}"));
    quote! { ::f9i_core::mgc::Mgc::#id }
}

fn decorated_word_to_tokens(dw: DecoratedWord) -> TokenStream2 {
    let (main, decoration) = dw.into_parts();

    let main_tokens: Vec<_> = main.into_iter().map(mgc_to_token).collect();

    let dt = match decoration.name_marker {
        NameMarker::None => quote! { ::f9i_core::mgc::NameMarker::None },
        NameMarker::Carth => quote! { ::f9i_core::mgc::NameMarker::Carth },
        NameMarker::Tor => quote! { ::f9i_core::mgc::NameMarker::Tor },
        NameMarker::Njor => quote! { ::f9i_core::mgc::NameMarker::Njor },
        NameMarker::Es => quote! { ::f9i_core::mgc::NameMarker::Es },
    };

    let flag_val = decoration.flags.bits();

    quote! {
            ::f9i_core::mgc::DecoratedWord::new(
                ::smallvec::smallvec![#(#main_tokens),*],
                ::f9i_core::mgc::Decoration {
                    flags: ::f9i_core::mgc::DecorationFlags::from_bits_retain(#flag_val),
                    name_marker: #dt,
                }
            )
    }
}

/// Parses a decorated word as a [f9i_core::mgc::DecoratedWord] object.
///
/// This will fail to compile if parsing fails.
#[proc_macro]
pub fn decw(input: TokenStream) -> TokenStream {
    let word = parse_macro_input!(input as LitStr);
    let dw = DecoratedWord::parse(word.value().as_str()).unwrap();

    decorated_word_to_tokens(dw).into()
}

/// Parses a sequence of manifested grapheme phrases as a slice of [f9i_core::mgc::Mgc] objects.
///
/// This will fail to compile if parsing fails.
#[proc_macro]
pub fn frag(input: TokenStream) -> TokenStream {
    let word = parse_macro_input!(input as LitStr);
    let frag = DecoratedWord::parse_fragment(word.value().as_str()).unwrap();

    let frag_tokens: Vec<_> = frag.into_iter().map(mgc_to_token).collect();

    let a = (quote! {
               &[#(#frag_tokens),*]
    })
    .into();

    a
}

/// Parses a sequence of manifested grapheme phrases as a vector of [f9i_core::mgc::Mgc] objects.
///
/// This will fail to compile if parsing fails.
#[proc_macro]
pub fn fragx(input: TokenStream) -> TokenStream {
    let word = parse_macro_input!(input as LitStr);
    let frag = DecoratedWord::parse_fragment(word.value().as_str()).unwrap();

    let frag_tokens: Vec<_> = frag.into_iter().map(mgc_to_token).collect();

    let a = (quote! {
               ::smallvec::smallvec![#(#frag_tokens),*]
    })
    .into();

    a
}

fn phrase_to_tokens(phr: Phrase) -> TokenStream2 {
    match phr {
        Phrase::Word(w) => {
            let word_token = decorated_word_to_tokens(w);
            quote! {
                ::f9i_core::mgc::Phrase::Word(
                    #word_token
                )
            }
        }
        Phrase::Append(phs) => {
            let phr_tokens: Vec<TokenStream2> = phs.into_iter().map(phrase_to_tokens).collect();
            quote! {
                ::f9i_core::mgc::Phrase::Append(vec![
                    #(#phr_tokens),*
                ])
            }
        }
    }
}

/// Parses a sequence of decorated words as a [f9i_core::mgc::Phrase] object.
///
/// This will fail to compile if parsing fails.
#[proc_macro]
pub fn phr(input: TokenStream) -> TokenStream {
    let word = parse_macro_input!(input as LitStr);
    let phr = Phrase::parse(word.value().as_str()).expect("Failed to parse phrase");

    phrase_to_tokens(phr).into()
}

fn assemblage_ref<T: Assemblage>(s: &str, t_id: &str) -> TokenStream2 {
    let parsed = match DecoratedWord::parse_assemblage::<T>(s) {
        Ok(o) => o,
        Err(e) => {
            let e = e.to_string();
            return quote! {
                compile_error!(#e)
            };
        }
    };
    let tokens = T::View::from(&parsed).to_tokens();
    let t2_id = format_ident!("{}Ref", t_id);
    quote! {
        {
            use f9i_core::mgc::*;
            use f9i_core::assemblage::#t2_id;
            #tokens
        }
    }
}

trait Assemblage: AsmParse {
    type View<'a>: From<&'a Self> + SelfRustTokenize
    where
        Self: 'a;
}

macro_rules! generate_assemblage_macros {
    ($($(#[$meta:meta])* $id:ident = $path:ident;)+) => {
        $(
            paste! {
                impl Assemblage for $path {
                    type View<'a> = [<$path Ref>]<'a>;
                }
                $(#[$meta])*
                #[proc_macro]
                pub fn $id(input: TokenStream) -> TokenStream {
                    let word = parse_macro_input!(input as LitStr);
                    assemblage_ref::<$path>(word.value().as_str(), stringify!($path)).into()
                }
            }
        )+
    };
}

// TODO: look into supporting interpolation; e.g.
// `x!("{}n", vowel)`
generate_assemblage_macros! {
    /// Expands to a series of parsed nonterminal syllables without decoration.
    y = SyllabicToSyllabic;
    /// Expands to a parsed word without decoration.
    w = SyllabicToTerminal;
    /// Expands to a parsed stem.
    a = SyllabicToOnset;
    /// Expands to a parsed glide-to-syllabic morpheme.
    f = GlideToSyllabic;
    /// Expands to a parsed glide-to-terminal morpheme.
    x = GlideToTerminal;
    /// Expands to a parsed glide-to-glide morpheme.
    gg = GlideToGlide;
    /// Expands to a parsed glide-to-onset morpheme.
    go = GlideToOnset;
    /// Expands to a parsed nuclear-to-terminal morpheme.
    nw = NuclearToTerminal;
    /// Expands to a parsed nuclear-to-syllabic morpheme.
    ns = NuclearToSyllabic;
    /// Expands to a parsed nuclear-to-nuclear morpheme.
    nn = NuclearToNuclear;
    /// Expands to a parsed glide-to-nuclear morpheme.
    gn = GlideToNuclear;
    /// Expands to a parsed nuclear-to-onset morpheme.
    b = NuclearToOnset;
}
