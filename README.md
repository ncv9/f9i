# f9i

A *fast Ŋarâþ Crîþ v9* inflecter written in Rust.

## Status

This program is now in **maintenance mode**. It will be replaced with an FST-based inflecter with Ŋarâþ Crîþ v9n.

## Building

This program requires nightly Rust to build.

## Input

This section assumes knowledge of Ŋarâþ Crîþ v9 morphology, which is explained in the [grammar].

The f9t input consists of pairs of lines. The first line in each pair consists of a label followed by a colon and a description of the word to inflect:

    eltes: noun celestial 1 elt ilt eld e

The second line is “extra data” – additional data to associate with each entry that is ignored by the inflecter but appears in generated output. Some output formats might manipulate the extra data to fill in information computed by the inflector.

`f9i file` requires a file in the f9t format. `f9i repl` is an interactive program that accept label lines.

### Uninflected words

    <label>: !<pos tag> <lemma>

### Nouns

    # Include this when asked for an <ndsub>
    #                               ~~~~~~~~~~~~~~
    <label>: noun <gender> <clareþ> <paradigm> ...

    <label>: noun <gender> <clareþ> I <N> <L> <S> <ΘΣ> <Λ>
    <label>: noun <gender> <clareþ> IIp <N> <G> <L> <S> <ΘΣ> <Λ>
    <label>: noun <gender> <clareþ> IIu <N> <L> <S> <ΘΣ> <Λ>
    <label>: noun <gender> <clareþ> III <N> <L> <S> <ΘΣ>
    <label>: noun <gender> <clareþ> III <N> <A> <G> <L> <S> el
    <label>: noun <gender> <clareþ> IV <N> <L> <S> <Θ> <Λ>
    <label>: noun <gender> <clareþ> V <N> <S> <Θ> <Σ>

Compound nouns:

    <label>: noun <gender> (<ndsub> | <ndsub>)
    <label>: noun <gender> (<phrase> < <ndsub>)
    <label>: noun <gender> (<ndsub> > <phrase>)

### Verbs

    <label>: verb <transitivity> / <material> <I> <N> <P> <Θ> / <R> <Q> <species> / <L>
    <species>: I1 <ΛΣ> <oΓ> <I/A>
    <species>: I3 <w/x> <I/A>
    <species>: II4 <ΛΣ>
    <species>: II3 <s/v>
    <species>: III2

### Relationals

    <label>: relational <I|II> <case> [~<ancilliary case>] <A> <V> <N> <C>

[grammar]: https://ncv9.flirora.xyz/grammar/index.html
