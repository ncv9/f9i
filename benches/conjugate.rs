use criterion::{black_box, criterion_group, criterion_main, Criterion};
use f9i::parse;
use f9i_core::category::Clareth;

pub fn criterion_benchmark(c: &mut Criterion) {
    c.bench_function("conjugate ecljat", |b| {
        let verb_line =
            "ecljat: verb semitransitive / vitreous eclj eclj eclj a / eclj eclj I1 a or a / oclj";
        let verb = parse::verb_line(verb_line).unwrap().1;
        b.iter(|| {
            let verb = black_box(&verb.form);
            verb.conjugate()
        })
    });
    c.bench_function("conjugate eristat", |b| {
        let verb_line =
            "eristat: verb semitransitive / vitreous erist erist erist a / erist erist I3 x a / erest";
        let verb = parse::verb_line(verb_line).unwrap().1;
        b.iter(|| {
            let verb = black_box(&verb.form);
            verb.conjugate()
        })
    });
    c.bench_function("decline my name", |b| {
        let noun_line =
            "noun human singular (VI +merl +mirł +mirł +mirł +merl an e | I #fliror #flier #flirol a a)";
        let noun = parse::noun_line(noun_line).unwrap().1;
        b.iter(|| {
            let noun = black_box(&noun.form);
            noun.decline(Clareth::Singular)
        })
    });
}
criterion_group! {
    name = benches;
    // This can be any expression that returns a `Criterion` object.
    config = Criterion::default()
        .confidence_level(0.995)
        .significance_level(0.005)
        .noise_threshold(0.001)
        .sample_size(200);
    targets = criterion_benchmark
}
criterion_main!(benches);
