use std::fmt::Display;

use gridlock::Gridlock;
use typesets::Subtype;

use crate::mgc::simple::*;

/// Represents a (possibly null) consonant with which a stem can be fused.
///
/// ε is intentionally omitted here, as fusion with ε yields a structurally
/// different result from fusion with another consonant.
#[derive(Copy, Clone, Eq, PartialEq, Debug, Gridlock, Subtype)]
#[repr(u8)]
#[subtype_of(Consonant)]
pub enum FusionConsonant {
    T,
    N,
    Th,
    // DO NOT include c or l, or you will make Zephyrus sad.
}

impl FusionConsonant {
    /// Returns a sequence of [Mgc]s consisting of the consonant with a leading hyphen.
    ///
    /// If the consonant is null, then there is no leading hyphen.
    pub const fn as_mgcs_prehyphen(self) -> &'static [Mgc] {
        match self {
            FusionConsonant::T => &[Mgc::Hyphen, Mgc::T],
            FusionConsonant::N => &[Mgc::Hyphen, Mgc::N],
            FusionConsonant::Th => &[Mgc::Hyphen, Mgc::Th],
        }
    }
}

impl Display for FusionConsonant {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.write_str(match self {
            FusionConsonant::T => "t",
            FusionConsonant::N => "n",
            FusionConsonant::Th => "þ",
        })
    }
}

impl Tokenize for FusionConsonant {
    #[inline]
    fn append_to_tokens(&self, s: &mut Fragment) {
        s.push(Mgc::from(Consonant::from(*self)))
    }

    fn peek(&self) -> Option<Mgc> {
        Some(Mgc::from(Consonant::from(*self)))
    }
}
