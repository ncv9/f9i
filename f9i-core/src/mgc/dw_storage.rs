use std::{
    alloc::{self, Layout},
    fmt::Debug,
    hash::Hash,
    mem::{self, MaybeUninit},
    ptr::{self, NonNull},
    slice,
};

use static_assert_macro::static_assert;

use crate::mgc::simple::*;

#[derive(Copy, Clone)]
pub struct DwpStorage {
    pub data: DwPtr,
    pub size: u16,
    pub capacity: u16,
    pub decoration: Decoration,
    pub dw_count: u8,
}

#[derive(Copy, Clone)]
pub union DwPtr {
    pub ptr: NonNull<Mgc>,
    pub ptr2: NonNull<DecoratedWordStorage>,
    pub direct: [MaybeUninit<Mgc>; 8],
}

#[repr(transparent)]
pub struct DecoratedWordStorage {
    pub s: DwpStorage,
}

static_assert!(mem::size_of::<DecoratedWordStorage>() == 16);

impl DecoratedWordStorage {
    const MAX_SIZE: usize = u16::MAX as usize;

    pub fn new(mut main: Fragment, decoration: Decoration) -> Self {
        let p = main.as_mut_ptr();
        let len = main.len();
        let cap = main.capacity();
        let spilled = main.spilled();
        assert!(len <= Self::MAX_SIZE && cap <= Self::MAX_SIZE);
        mem::forget(main);

        DecoratedWordStorage {
            s: DwpStorage {
                data: if spilled {
                    DwPtr {
                        ptr: unsafe { NonNull::new_unchecked(p) },
                    }
                } else {
                    let mut direct: [MaybeUninit<Mgc>; 8] =
                        unsafe { MaybeUninit::uninit().assume_init() };
                    unsafe {
                        ptr::copy_nonoverlapping(p, direct.as_mut_ptr().cast(), len);
                    }
                    DwPtr { direct }
                },
                size: len as u16,
                capacity: if spilled { cap as u16 } else { 8 },
                decoration,
                dw_count: 1,
            },
        }
    }

    pub fn empty(decoration: Decoration) -> Self {
        DecoratedWordStorage {
            s: DwpStorage {
                data: DwPtr {
                    direct: unsafe { MaybeUninit::uninit().assume_init() },
                },
                size: 0,
                capacity: 8,
                decoration,
                dw_count: 1,
            },
        }
    }

    #[inline]
    pub fn letters(&self) -> &[Mgc] {
        unsafe { slice::from_raw_parts(self.get_data_ptr(), self.s.size as usize) }
    }

    #[inline]
    pub fn letters_mut(&mut self) -> &mut [Mgc] {
        unsafe { slice::from_raw_parts_mut(self.get_data_ptr_mut(), self.s.size as usize) }
    }

    #[inline]
    pub fn decoration(&self) -> Decoration {
        self.s.decoration
    }

    pub fn spilled(&self) -> bool {
        self.s.capacity > 8
    }

    #[inline]
    pub fn get_data_ptr(&self) -> *const Mgc {
        if self.spilled() {
            unsafe { self.s.data.ptr.as_ptr() }
        } else {
            unsafe { self.s.data.direct.as_ptr().cast() }
        }
    }

    #[inline]
    pub fn get_data_ptr_mut(&mut self) -> *mut Mgc {
        if self.spilled() {
            unsafe { self.s.data.ptr.as_ptr() }
        } else {
            unsafe { self.s.data.direct.as_mut_ptr().cast() }
        }
    }

    pub fn into_fragment(self) -> Fragment {
        let frag = if self.spilled() {
            unsafe {
                Fragment::from_raw_parts(
                    self.s.data.ptr.as_ptr(),
                    self.s.size as usize,
                    self.s.capacity as usize,
                )
            }
        } else {
            unsafe {
                Fragment::from_buf_and_len_unchecked(
                    mem::transmute::<_, MaybeUninit<[Mgc; 8]>>(self.s.data.direct),
                    self.s.size as usize,
                )
            }
        };
        mem::forget(self);
        frag
    }

    fn expand_to(&mut self, new_size: usize) {
        if new_size < self.s.capacity as usize || new_size <= 8 {
            return;
        }
        assert!(new_size <= Self::MAX_SIZE);
        let mut new_capacity = self.s.capacity as usize;
        while new_capacity < new_size {
            new_capacity *= 2;
        }
        if new_capacity > Self::MAX_SIZE {
            new_capacity = Self::MAX_SIZE;
        }
        assert!(new_capacity > 8);
        let new_ptr = NonNull::new(if self.spilled() {
            let new_layout = layout_for_array::<Mgc>(new_capacity);
            unsafe {
                alloc::realloc(
                    self.s.data.ptr.as_ptr() as *mut u8,
                    layout_for_array::<Mgc>(self.s.capacity as usize),
                    new_layout.size(),
                )
            }
        } else {
            unsafe { alloc::alloc(layout_for_array::<Mgc>(new_capacity)) }
        })
        .expect("allocation failed")
        .cast();
        if !self.spilled() {
            unsafe {
                ptr::copy_nonoverlapping(
                    self.s.data.direct.as_ptr().cast(),
                    new_ptr.as_ptr(),
                    self.s.size as usize,
                );
            }
        }
        self.s.capacity = new_capacity as u16;
        self.s.data = DwPtr { ptr: new_ptr };
    }

    fn expand_by(&mut self, n: usize) {
        self.expand_to(n + self.s.size as usize);
    }

    unsafe fn store(&mut self, i: u16, mgc: Mgc) {
        ptr::write(self.get_data_ptr_mut().add(i as usize), mgc);
    }

    pub fn push(&mut self, mgc: Mgc) {
        self.expand_by(1);
        unsafe {
            self.store(self.s.size, mgc);
        }
        self.s.size += 1;
    }

    pub fn prepend(&mut self, mgc: Mgc) {
        self.expand_by(1);
        unsafe {
            ptr::copy(
                self.get_data_ptr(),
                self.get_data_ptr_mut().add(1),
                self.s.size as usize,
            );
            self.store(0, mgc);
        }
        self.s.size += 1;
    }

    pub fn prepend_frag(&mut self, mgc: &[Mgc]) {
        self.expand_by(mgc.len());
        unsafe {
            ptr::copy(
                self.get_data_ptr(),
                self.get_data_ptr_mut().add(mgc.len()),
                self.s.size as usize,
            );
            ptr::copy_nonoverlapping(mgc.as_ptr(), self.get_data_ptr_mut(), mgc.len());
        }
        self.s.size += mgc.len() as u16;
    }

    pub fn extend(&mut self, mgcs: &[Mgc]) {
        self.expand_by(mgcs.len());
        unsafe {
            ptr::copy_nonoverlapping(
                mgcs.as_ptr(),
                self.get_data_ptr_mut().add(self.s.size as usize),
                mgcs.len(),
            );
        }
        self.s.size += mgcs.len() as u16;
    }

    pub fn remove_hyphens(&mut self) {
        let letters = self.letters_mut();
        let mut read_idx = 0;
        let mut write_idx = 0;
        while read_idx < letters.len() {
            let m = letters[read_idx];
            if m != Mgc::Hyphen {
                letters[write_idx] = m;
                write_idx += 1;
            }
            read_idx += 1;
        }
        self.s.size = write_idx as u16;
    }
}

unsafe impl Send for DecoratedWordStorage {}
unsafe impl Sync for DecoratedWordStorage {}

impl Drop for DecoratedWordStorage {
    fn drop(&mut self) {
        if self.spilled() {
            unsafe {
                alloc::dealloc(
                    self.s.data.ptr.as_ptr() as *mut u8,
                    layout_for_array::<Mgc>(self.s.capacity as usize),
                );
            }
        }
    }
}

impl Clone for DecoratedWordStorage {
    fn clone(&self) -> Self {
        let data = if self.spilled() {
            let ptr = unsafe {
                NonNull::new(alloc::alloc(layout_for_array::<Mgc>(
                    self.s.capacity as usize,
                )))
                .expect("allocation failed")
                .cast()
            };
            unsafe {
                ptr::copy_nonoverlapping(
                    self.s.data.ptr.as_ptr(),
                    ptr.as_ptr(),
                    self.s.size as usize,
                );
            }
            DwPtr { ptr }
        } else {
            unsafe {
                DwPtr {
                    direct: self.s.data.direct,
                }
            }
        };
        DecoratedWordStorage {
            s: DwpStorage {
                data,
                size: self.s.size,
                capacity: self.s.capacity,
                decoration: self.s.decoration,
                dw_count: 1,
            },
        }
    }
}

impl PartialEq for DecoratedWordStorage {
    fn eq(&self, other: &Self) -> bool {
        self.s.decoration == other.s.decoration && self.letters() == other.letters()
    }
}
impl Eq for DecoratedWordStorage {}

impl Debug for DecoratedWordStorage {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.write_str("DecoratedWord { main: ")?;
        Debug::fmt(self.letters(), f)?;
        f.write_str(", decoration: ")?;
        Debug::fmt(&self.s.decoration, f)?;
        f.write_str(" }")
    }
}

impl Hash for DecoratedWordStorage {
    fn hash<H: std::hash::Hasher>(&self, state: &mut H) {
        self.letters().hash(state);
        self.s.decoration.hash(state);
    }
}

impl Default for DecoratedWordStorage {
    fn default() -> Self {
        DecoratedWordStorage::empty(Decoration::EMPTY)
    }
}

pub fn layout_for_array<T>(n: usize) -> Layout {
    let size = mem::size_of::<T>().checked_mul(n).expect("2big4u");
    let align = mem::align_of::<T>();
    Layout::from_size_align(size, align).unwrap()
}
