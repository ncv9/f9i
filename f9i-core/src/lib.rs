#![feature(assert_matches)]
#![feature(try_trait_v2)]

//! Basic vocabulary types for f9i.
//!
//! Some of these types are used by both the main f9i crate and the f9i-ivm-macro crate, which provides procedural macros for f9i's codebase.

pub mod analysis;
pub mod assemblage;
pub mod category;
pub mod mgc;
pub mod parse;

use typesets::TypesetsError;

#[cfg(test)]
mod tests {
    use std::assert_matches::assert_matches;

    use super::analysis::*;
    use super::assemblage::*;
    use super::mgc::*;
    use smallvec::smallvec;
    #[test]
    fn mgc_traits_is_vowel() {
        assert!(Mgc::E.is_vowel());
        assert!(Mgc::AHat.is_vowel());
        assert!(!Mgc::J.is_vowel());
        assert!(!Mgc::C.is_vowel());
        assert!(!Mgc::VDot.is_vowel());
        assert!(!Mgc::Dhth.is_vowel());
    }
    #[test]
    fn mgc_traits_is_hatless_vowel() {
        assert!(Mgc::E.is_hatless_vowel());
        assert!(!Mgc::AHat.is_hatless_vowel());
        assert!(!Mgc::J.is_hatless_vowel());
        assert!(!Mgc::C.is_hatless_vowel());
        assert!(!Mgc::VDot.is_hatless_vowel());
        assert!(!Mgc::Dhth.is_hatless_vowel());
    }
    #[test]
    fn mgc_traits_is_hatted_vowel() {
        assert!(!Mgc::E.is_hatted_vowel());
        assert!(Mgc::AHat.is_hatted_vowel());
        assert!(!Mgc::J.is_hatted_vowel());
        assert!(!Mgc::C.is_hatted_vowel());
        assert!(!Mgc::VDot.is_hatted_vowel());
        assert!(!Mgc::Dhth.is_hatted_vowel());
    }
    #[test]
    fn mgc_traits_invert_tone_opt() {
        assert_eq!(Mgc::E.invert_tone_opt(), Some(Mgc::EHat));
        assert_eq!(Mgc::U.invert_tone_opt(), Some(Mgc::U));
        assert_eq!(Mgc::AHat.invert_tone_opt(), Some(Mgc::A));
        assert_eq!(Mgc::J.invert_tone_opt(), None);
        assert_eq!(Mgc::C.invert_tone_opt(), None);
        assert_eq!(Mgc::VDot.invert_tone_opt(), None);
        assert_eq!(Mgc::Dhth.invert_tone_opt(), None);
    }
    #[test]
    fn mgc_traits_is_consonant() {
        assert!(!Mgc::E.is_consonant());
        assert!(!Mgc::AHat.is_consonant());
        assert!(!Mgc::J.is_consonant()); // is a semivowel, not a consonant
        assert!(Mgc::C.is_consonant());
        assert!(Mgc::VDot.is_consonant());
        assert!(Mgc::Dhth.is_consonant());
    }
    #[test]
    fn mgc_traits_is_regular_consonant() {
        assert!(!Mgc::E.is_regular_consonant());
        assert!(!Mgc::AHat.is_regular_consonant());
        assert!(!Mgc::J.is_regular_consonant());
        assert!(Mgc::C.is_regular_consonant());
        assert!(!Mgc::VDot.is_regular_consonant());
        assert!(!Mgc::Dhth.is_regular_consonant());
    }
    #[test]
    fn mgc_traits_is_lenited_consonant() {
        assert!(!Mgc::E.is_lenited_consonant());
        assert!(!Mgc::AHat.is_lenited_consonant());
        assert!(!Mgc::J.is_lenited_consonant());
        assert!(!Mgc::C.is_lenited_consonant());
        assert!(Mgc::VDot.is_lenited_consonant());
        assert!(!Mgc::Dhth.is_lenited_consonant());
    }
    #[test]
    fn mgc_traits_is_eclipsed_consonant() {
        assert!(!Mgc::E.is_eclipsed_consonant());
        assert!(!Mgc::AHat.is_eclipsed_consonant());
        assert!(!Mgc::J.is_eclipsed_consonant());
        assert!(!Mgc::C.is_eclipsed_consonant());
        assert!(!Mgc::VDot.is_eclipsed_consonant());
        assert!(Mgc::Dhth.is_eclipsed_consonant());
    }
    #[test]
    fn mgc_traits_effective_class() {
        assert_eq!(
            Mgc::E.get_mgc_properties().effective_class,
            EffectiveClass::Other
        );
        assert_eq!(
            Mgc::AHat.get_mgc_properties().effective_class,
            EffectiveClass::Other
        );
        assert_eq!(
            Mgc::J.get_mgc_properties().effective_class,
            EffectiveClass::Other
        );
        assert_eq!(
            Mgc::C.get_mgc_properties().effective_class,
            EffectiveClass::Plosive
        );
        assert_eq!(
            Mgc::VDot.get_mgc_properties().effective_class,
            EffectiveClass::Fricative
        );
        assert_eq!(
            Mgc::Dhth.get_mgc_properties().effective_class,
            EffectiveClass::Fricative
        );
    }
    #[test]
    fn mgc_parse_word() {
        assert_eq!(
            DecoratedWord::parse("#flirora"),
            Ok(DecoratedWord::new(
                smallvec![Mgc::F, Mgc::L, Mgc::I, Mgc::R, Mgc::O, Mgc::R, Mgc::A,],
                Decoration {
                    flags: DecorationFlags::empty(),
                    name_marker: NameMarker::Carth,
                }
            ))
        );
        assert_eq!(
            DecoratedWord::parse("v#flirora"),
            Ok(DecoratedWord::new(
                smallvec![Mgc::Vf, Mgc::L, Mgc::I, Mgc::R, Mgc::O, Mgc::R, Mgc::A,],
                Decoration {
                    flags: DecorationFlags::empty(),
                    name_marker: NameMarker::Carth,
                }
            ))
        );
        assert_eq!(
            DecoratedWord::parse("cevoc·oþ"),
            Ok(DecoratedWord::new(
                smallvec![Mgc::C, Mgc::E, Mgc::V, Mgc::O, Mgc::CDot, Mgc::O, Mgc::Th,],
                Decoration {
                    flags: DecorationFlags::empty(),
                    name_marker: NameMarker::None,
                }
            ))
        );
        assert_eq!(
            DecoratedWord::parse("g@eleþris"),
            Ok(DecoratedWord::new(
                smallvec![
                    Mgc::GEclipse,
                    Mgc::E,
                    Mgc::L,
                    Mgc::E,
                    Mgc::Th,
                    Mgc::R,
                    Mgc::I,
                    Mgc::S,
                ],
                Decoration {
                    flags: DecorationFlags::empty(),
                    name_marker: NameMarker::Es,
                }
            ))
        );
        assert_eq!(
            DecoratedWord::parse("Gogogo"),
            Ok(DecoratedWord::new(
                smallvec![Mgc::GEclipse, Mgc::O, Mgc::G, Mgc::O, Mgc::G, Mgc::O],
                Decoration {
                    flags: DecorationFlags::empty(),
                    name_marker: NameMarker::None,
                }
            ))
        );
        assert_eq!(
            DecoratedWord::parse("*+*&aâa"),
            Ok(DecoratedWord::new(
                smallvec![Mgc::A, Mgc::AHat, Mgc::A,],
                Decoration {
                    flags: DecorationFlags::NEF | DecorationFlags::SEN,
                    name_marker: NameMarker::Njor,
                }
            ))
        );
        assert_eq!(
            DecoratedWord::parse("renda"),
            Ok(DecoratedWord::new(
                smallvec![Mgc::R, Mgc::E, Mgc::N, Mgc::D, Mgc::A],
                Decoration {
                    flags: DecorationFlags::empty(),
                    name_marker: NameMarker::None,
                }
            ))
        );
        assert_eq!(
            DecoratedWord::parse("gašida"),
            Ok(DecoratedWord::new(
                smallvec![Mgc::G, Mgc::A, Mgc::Sh, Mgc::I, Mgc::D, Mgc::A],
                Decoration {
                    flags: DecorationFlags::empty(),
                    name_marker: NameMarker::None,
                }
            ))
        );
    }
    #[test]
    fn mgc_parse_word_bad() {
        assert!(DecoratedWord::parse("").is_err());
        assert!(DecoratedWord::parse("@").is_err());
        assert!(DecoratedWord::parse("###").is_err());
        // This actually gets tokenized, but not into what you might expect (at least, if you're not experienced with Ŋarâþ Crîþ)
        // assert!(DecoratedWord::parse("#vflirora").is_err());
        assert!(DecoratedWord::parse("dumb memes").is_err());
        assert!(DecoratedWord::parse("lâri!").is_err());
        assert!(DecoratedWord::parse("l·âri").is_err());
        assert!(DecoratedWord::parse("lâri'pe").is_err());
    }
    #[test]
    fn mgc_parse_phrase() {
        assert_eq!(
            Phrase::parse("+merlan #flirora"),
            Ok(Phrase::Append(vec![
                Phrase::Word(DecoratedWord::new(
                    smallvec![Mgc::M, Mgc::E, Mgc::R, Mgc::L, Mgc::A, Mgc::N,],
                    Decoration {
                        flags: DecorationFlags::empty(),
                        name_marker: NameMarker::Tor,
                    }
                )),
                Phrase::Word(DecoratedWord::new(
                    smallvec![Mgc::F, Mgc::L, Mgc::I, Mgc::R, Mgc::O, Mgc::R, Mgc::A,],
                    Decoration {
                        flags: DecorationFlags::empty(),
                        name_marker: NameMarker::Carth,
                    }
                )),
            ]))
        );
        assert_eq!(
            Phrase::parse("*srela’pe"),
            Ok(Phrase::Append(vec![
                Phrase::Word(DecoratedWord::new(
                    smallvec![Mgc::S, Mgc::R, Mgc::E, Mgc::L, Mgc::A,],
                    Decoration {
                        flags: DecorationFlags::NEF,
                        name_marker: NameMarker::None,
                    }
                )),
                Phrase::Word(DecoratedWord::new(
                    smallvec![Mgc::P, Mgc::E,],
                    Decoration {
                        flags: DecorationFlags::CLITIC,
                        name_marker: NameMarker::None,
                    }
                )),
            ]))
        );
    }
    #[test]
    fn mgc_lenition() {
        assert_eq!(
            DecoratedWord::parse("#flirora").unwrap().lenite(),
            DecoratedWord::parse("#f·lirora").unwrap()
        );
        assert_eq!(
            DecoratedWord::parse("tara").unwrap().lenite(),
            DecoratedWord::parse("t·ara").unwrap()
        );
        assert_eq!(
            DecoratedWord::parse("ŋarâþ").unwrap().lenite(),
            DecoratedWord::parse("ŋarâþ").unwrap()
        );
        assert_eq!(
            DecoratedWord::parse("avona").unwrap().lenite(),
            DecoratedWord::parse("avona").unwrap()
        );
        assert_eq!(
            DecoratedWord::parse("cþeris").unwrap().lenite(),
            DecoratedWord::parse("cþeris").unwrap()
        );
    }
    #[test]
    fn word_manipulation() {
        let mut word = DecoratedWord::parse("#flirora").unwrap();
        word.push(Mgc::S);
        assert_eq!(word, DecoratedWord::parse("#fliroras").unwrap());
        word.extend(&[Mgc::A, Mgc::C, Mgc::Hyphen, Mgc::A]);
        assert_eq!(word, DecoratedWord::parse("#flirorasac-a").unwrap());
        word.prepend(Mgc::E);
        assert_eq!(word, DecoratedWord::parse("#eflirorasac-a").unwrap());
        word.remove_hyphens();
        assert_eq!(word, DecoratedWord::parse("#eflirorasaca").unwrap());
        let word2 = word.clone();
        assert_eq!(word, word2);
    }
    #[test]
    fn test_syllabify() {
        let word = DecoratedWord::parse("šidranþa").unwrap();
        let syllabification = syllabify(&word);
        assert_eq!(
            syllabification,
            Ok(vec![
                Syllable {
                    initial: Initial::Single(Consonant::Sh),
                    nucleus: Nucleus::I,
                    coda: Coda::Empty
                },
                Syllable {
                    initial: Initial::R(Epf::D),
                    nucleus: Nucleus::A,
                    coda: Coda::N
                },
                Syllable {
                    initial: Initial::Single(Consonant::Th),
                    nucleus: Nucleus::A,
                    coda: Coda::Empty
                },
            ])
        )
    }
    #[test]
    fn test_syllabify_invalid() {
        let word = DecoratedWord::parse("scare").unwrap();
        let syllabification = syllabify(&word);
        assert_matches!(syllabification, Err(SyllabifyError { .. }));
    }
    #[test]
    fn test_resolve_bridge() {
        assert_eq!(
            resolve_bridge(SimpleCoda::R, Initial::Single(Consonant::Th)),
            (
                ResolvedCoda::Coda(SimpleCoda::R),
                Initial::Single(Consonant::Th)
            )
        );
        assert_eq!(
            resolve_bridge(SimpleCoda::S, Initial::Single(Consonant::H)),
            (
                ResolvedCoda::Coda(SimpleCoda::S),
                Initial::Single(Consonant::C)
            )
        );
        assert_eq!(
            resolve_bridge(SimpleCoda::T, Initial::R(Epf::C)),
            (ResolvedCoda::Coda(SimpleCoda::C), Initial::R(Epf::T))
        );
        assert_eq!(
            resolve_bridge(SimpleCoda::C, Initial::Single(Consonant::M)),
            (ResolvedCoda::Ng, Initial::Single(Consonant::M))
        );
        assert_eq!(
            resolve_bridge(SimpleCoda::C, Initial::L(Epf::H)),
            (ResolvedCoda::Coda(SimpleCoda::Empty), Initial::L(Epf::C))
        );
    }
    #[test]
    fn test_parse_stem_without_decoration() {
        let word = DecoratedWord::parse_fragment("rasc").unwrap();
        let stem = AStem::parse(&word);
        assert_eq!(
            stem,
            Ok(AStem {
                tiles: smallvec![(
                    Initial::Single(Consonant::R),
                    Glide::None,
                    Vowel::A,
                    SimpleCoda::S
                ),],
                remainder: (Initial::Single(Consonant::C), Glide::None,)
            })
        );
    }
    #[test]
    fn test_parse_stem() {
        let word = DecoratedWord::parse("rasc").unwrap();
        let stem = Decorated::<AStem>::parse(&word);
        assert_eq!(
            stem,
            Ok(Decorated {
                frag: AStem {
                    tiles: smallvec![(
                        Initial::Single(Consonant::R),
                        Glide::None,
                        Vowel::A,
                        SimpleCoda::S
                    )],
                    remainder: (Initial::Single(Consonant::C), Glide::None),
                },
                decoration: Decoration {
                    flags: DecorationFlags::empty(),
                    name_marker: NameMarker::None
                },
            })
        );
    }
    #[test]
    fn test_parse_bridge() {
        let word = DecoratedWord::parse_fragment("scþ").unwrap();
        let stem = <(SimpleCoda, Onset) as Parse>::parse(&word);
        assert_eq!(stem, Ok((SimpleCoda::S, Onset::from(Initial::Cth))));
    }
    #[test]
    fn test_parse_word_medial_nd() {
        // make sure not to confuse medial ⟦nd⟧ with an eclipsed consonant
        let word = DecoratedWord::parse_fragment("renda").unwrap();
        let stem = ParsedWord::parse(&word);
        assert_eq!(
            stem,
            Ok(ParsedWord {
                tiles: smallvec![(
                    Initial::Single(Consonant::R),
                    Glide::None,
                    Vowel::E,
                    SimpleCoda::N
                ),],
                remainder: (
                    Initial::Single(Consonant::D),
                    Glide::None,
                    Vowel::A,
                    Coda::Empty
                )
            })
        );
    }
    #[test]
    fn test_parse_word_starting_with_eclipsed_letter() {
        // DecoratedWord::parse_fragment does not parse
        // eclipsed letters at the beginning, since
        // eclipsed letters interact with markers in weird
        // ways
        let word = DecoratedWord::parse("gcerecel").unwrap();
        let stem = ParsedWord::parse(word.letters());
        assert_eq!(
            stem,
            Ok(ParsedWord {
                tiles: smallvec![
                    (
                        Initial::Single(Consonant::Gc),
                        Glide::None,
                        Vowel::E,
                        SimpleCoda::Empty
                    ),
                    (
                        Initial::Single(Consonant::R),
                        Glide::None,
                        Vowel::E,
                        SimpleCoda::Empty
                    ),
                ],
                remainder: (
                    Initial::Single(Consonant::C),
                    Glide::None,
                    Vowel::E,
                    Coda::L
                )
            })
        );
    }
    #[test]
    #[allow(clippy::unit_cmp)]
    fn test_assemblage_parse_and_tokenize() {
        let word = DecoratedWord::parse_fragment("celsan").unwrap();
        let word_s = Sylls::parse(&word).unwrap();
        assert_eq!(word_s.remainder, ());
        assert_eq!(word_s.tiles.len(), 2);
        assert_eq!(word_s.to_tokens(), word);
    }

    // TODO: rewrite these

    #[test]
    fn test_assemblage_parse_and_tokenize_with_superblunt_end() {
        let word = DecoratedWord::parse_fragment("celsan").unwrap();
        let word_s = ParsedWord::parse(&word).unwrap();
        assert_eq!(
            word_s.remainder,
            (
                Initial::Single(Consonant::S),
                Glide::None,
                Vowel::A,
                Coda::N
            )
        );
        assert_eq!(word_s.tiles.len(), 1);
        assert_eq!(word_s.to_tokens(), word);
    }
    #[test]
    fn test_assemblage_parse_and_tokenize_with_superblunt_end_2() {
        let word = DecoratedWord::parse_fragment("vels").unwrap();
        let word_s = ParsedWord::parse(&word).unwrap();
        assert_eq!(
            word_s.remainder,
            (
                Initial::Single(Consonant::V),
                Glide::None,
                Vowel::E,
                Coda::Ls
            )
        );
        assert_eq!(word_s.tiles.len(), 0);
        assert_eq!(word_s.to_tokens(), word);
    }
    #[test]
    fn test_assemblage_parse_and_tokenize_with_sticky_end() {
        let word = DecoratedWord::parse_fragment("gašid").unwrap();
        let word_s = AStem::parse(&word).unwrap();
        assert_eq!(
            word_s.remainder,
            (Initial::Single(Consonant::D), Glide::None)
        );
        assert_eq!(word_s.tiles.len(), 2);
        assert_eq!(word_s.to_tokens(), word);
    }
    #[test]
    fn test_assemblage_parse_and_tokenize_with_sticky_start() {
        let word = DecoratedWord::parse_fragment("as").unwrap();
        let word_s = SimpleSuffix::parse(&word).unwrap();
        assert_eq!(word_s.remainder, (Glide::None, Vowel::A, SimpleCoda::S));
        assert_eq!(word_s.tiles.len(), 0);
        assert_eq!(word_s.to_tokens(), word);
    }
    #[test]
    fn test_assemblage_append() {
        let stem = DecoratedWord::parse_fragment("gašid").unwrap();
        let stem = AStem::parse(&stem).unwrap();
        let simple_suffix = DecoratedWord::parse_fragment("as").unwrap();
        let simple_suffix = SimpleSuffix::parse(&simple_suffix).unwrap();
        let complex_suffix = DecoratedWord::parse_fragment("als").unwrap();
        let complex_suffix = Suffix::parse(&complex_suffix).unwrap();

        {
            let expected = DecoratedWord::parse_fragment("gašidas").unwrap();
            let expected = Sylls::parse(&expected).unwrap();
            let actual = stem.clone() + &simple_suffix;
            assert_eq!(expected, actual);
        }
        {
            let expected = DecoratedWord::parse_fragment("gašidals").unwrap();
            let expected = ParsedWord::parse(&expected).unwrap();
            let actual = stem + &complex_suffix;
            assert_eq!(expected, actual);
        }
    }
    #[test]
    fn test_assemblage_append_elide_j() {
        let stem = DecoratedWord::parse_fragment("pafj").unwrap();
        let stem = AStem::parse(&stem).unwrap();
        let suffix = DecoratedWord::parse_fragment("is").unwrap();
        let suffix = SimpleSuffix::parse(&suffix).unwrap();

        let expected = DecoratedWord::parse_fragment("pafis").unwrap();
        let expected = Sylls::parse(&expected).unwrap();
        let actual = stem + &suffix;
        assert_eq!(expected, actual);
    }
    #[test]
    fn test_assemblage_append_keep_j() {
        let stem = DecoratedWord::parse_fragment("caj").unwrap();
        let stem = AStem::parse(&stem).unwrap();
        let suffix = DecoratedWord::parse_fragment("os").unwrap();
        let suffix = SimpleSuffix::parse(&suffix).unwrap();

        let expected = DecoratedWord::parse_fragment("cajos").unwrap();
        let expected = Sylls::parse(&expected).unwrap();
        let actual = stem + &suffix;
        assert_eq!(expected, actual);
    }
    #[test]
    fn test_assemblage_append_deduplicate() {
        let stem = DecoratedWord::parse_fragment("feva").unwrap();
        let stem = Sylls::parse(&stem).unwrap();
        let suffix = DecoratedWord::parse_fragment("ve").unwrap();
        let suffix = Sylls::parse(&suffix).unwrap();

        let expected = DecoratedWord::parse_fragment("fenave").unwrap();
        let expected = Sylls::parse(&expected).unwrap();
        let actual = stem + &suffix;
        assert_eq!(expected, actual);
    }
    #[test]
    fn test_assemblage_append_deduplicate_2() {
        let stem = DecoratedWord::parse_fragment("va").unwrap();
        let stem = SyllabicToNuclear::parse(&stem).unwrap();
        let suffix = DecoratedWord::parse_fragment("va").unwrap();
        let suffix = NuclearToSyllabic::parse(&suffix).unwrap();

        let expected = DecoratedWord::parse_fragment("nava").unwrap();
        let expected = Sylls::parse(&expected).unwrap();
        let actual = stem + &suffix;
        assert_eq!(expected, actual);
    }
    #[test]
    fn test_assemblage_append_deduplicate_3() {
        let stem = DecoratedWord::parse_fragment("v").unwrap();
        let stem = SyllabicToGlide::parse(&stem).unwrap();
        let suffix = DecoratedWord::parse_fragment("ava").unwrap();
        let suffix = GlideToSyllabic::parse(&suffix).unwrap();

        let expected = DecoratedWord::parse_fragment("nava").unwrap();
        let expected = Sylls::parse(&expected).unwrap();
        let actual = stem + &suffix;
        assert_eq!(expected, actual);
    }
    #[test]
    fn test_assemblage_append_deduplicate_4() {
        let stem = DecoratedWord::parse_fragment("seva").unwrap();
        let stem = SyllabicToNuclear::parse(&stem).unwrap();
        let suffix = DecoratedWord::parse_fragment("va").unwrap();
        let suffix = NuclearToSyllabic::parse(&suffix).unwrap();

        let expected = DecoratedWord::parse_fragment("senava").unwrap();
        let expected = Sylls::parse(&expected).unwrap();
        let actual = stem + &suffix;
        assert_eq!(expected, actual);
    }
    #[test]
    fn test_assemblage_append_deduplicate_5() {
        let stem = DecoratedWord::parse_fragment("va").unwrap();
        let stem = SyllabicToNuclear::parse(&stem).unwrap();
        let suffix = DecoratedWord::parse_fragment("vanas").unwrap();
        let suffix = NuclearToSyllabic::parse(&suffix).unwrap();

        let expected = DecoratedWord::parse_fragment("navanas").unwrap();
        let expected = Sylls::parse(&expected).unwrap();
        let actual = stem + &suffix;
        assert_eq!(expected, actual);
    }
    #[test]
    fn test_assemblage_append_deduplicate_6() {
        let stem = DecoratedWord::parse_fragment("þ").unwrap();
        let stem = AStem::parse(&stem).unwrap();
        let suffix = DecoratedWord::parse_fragment("aþe").unwrap();
        let suffix = SimpleSuffix::parse(&suffix).unwrap();

        let expected = DecoratedWord::parse_fragment("taþe").unwrap();
        let expected = Sylls::parse(&expected).unwrap();
        let actual = stem + &suffix;
        assert_eq!(expected, actual);
    }
    #[test]
    fn test_assemblage_append_deduplicate_7() {
        let stem = DecoratedWord::parse_fragment("þ").unwrap();
        let stem = AStem::parse(&stem).unwrap();
        let suffix = DecoratedWord::parse_fragment("aþ").unwrap();
        let suffix = Suffix::parse(&suffix).unwrap();

        let expected = DecoratedWord::parse_fragment("taþ").unwrap();
        let expected = ParsedWord::parse(&expected).unwrap();
        let actual = stem + &suffix;
        assert_eq!(expected, actual);
    }
    #[test]
    fn test_assemblage_append_deduplicate_by_coda() {
        let stem = DecoratedWord::parse_fragment("þaþ").unwrap();
        let stem = Sylls::parse(&stem).unwrap();
        let suffix = DecoratedWord::parse_fragment("ca").unwrap();
        let suffix = Sylls::parse(&suffix).unwrap();

        let expected = DecoratedWord::parse_fragment("taþca").unwrap();
        let expected = Sylls::parse(&expected).unwrap();
        let actual = stem + &suffix;
        assert_eq!(expected, actual);
    }
    #[test]
    fn test_assemblage_append_deduplicate_by_coda_2() {
        let stem = DecoratedWord::parse_fragment("þa").unwrap();
        let stem = SyllabicToNuclear::parse(&stem).unwrap();
        let suffix = DecoratedWord::parse_fragment("þca").unwrap();
        let suffix = NuclearToSyllabic::parse(&suffix).unwrap();

        let expected = DecoratedWord::parse_fragment("taþca").unwrap();
        let expected = Sylls::parse(&expected).unwrap();
        let actual = stem + &suffix;
        assert_eq!(expected, actual);
    }
    #[test]
    fn test_assemblage_append_deduplicate_final() {
        let stem = DecoratedWord::parse_fragment("vârpoþ").unwrap();
        let stem = AStem::parse(&stem).unwrap();
        let suffix = DecoratedWord::parse_fragment("eþ").unwrap();
        let suffix = Suffix::parse(&suffix).unwrap();

        let expected = DecoratedWord::parse_fragment("vârpoteþ").unwrap();
        let expected = ParsedWord::parse(&expected).unwrap();
        let actual = stem + &suffix;
        assert_eq!(expected, actual);
    }
    #[test]
    fn test_assemblage_append_deduplicate_with_prev_coda() {
        let stem = DecoratedWord::parse_fragment("leþþ").unwrap();
        let stem = AStem::parse(&stem).unwrap();
        let suffix = DecoratedWord::parse_fragment("eþer").unwrap();
        let suffix = SimpleSuffix::parse(&suffix).unwrap();

        let expected = DecoratedWord::parse_fragment("lesteþer").unwrap();
        let expected = Sylls::parse(&expected).unwrap();
        let actual = stem + &suffix;
        assert_eq!(expected, actual);
    }
    #[test]
    fn test_assemblage_append_deduplicate_with_prev_coda_at_terminal() {
        let stem = DecoratedWord::parse_fragment("leþþ").unwrap();
        let stem = AStem::parse(&stem).unwrap();
        let suffix = DecoratedWord::parse_fragment("eþ").unwrap();
        let suffix = Suffix::parse(&suffix).unwrap();

        let expected = DecoratedWord::parse_fragment("lesteþ").unwrap();
        let expected = ParsedWord::parse(&expected).unwrap();
        let actual = stem + &suffix;
        assert_eq!(expected, actual);
    }
    #[test]
    fn test_assemblage_append_1() {
        {
            let stem = DecoratedWord::parse_fragment("feva").unwrap();
            let stem = Sylls::parse(&stem).unwrap();

            let expected = DecoratedWord::parse_fragment("fenav").unwrap();
            let expected = SyllabicToGlide::parse(&expected).unwrap();
            let actual = stem + Initial::Single(Consonant::V);
            assert_eq!(expected, actual);
        }
        {
            let stem = DecoratedWord::parse_fragment("sanac").unwrap();
            let stem = Sylls::parse(&stem).unwrap();

            let expected = DecoratedWord::parse_fragment("sanorm").unwrap();
            let expected = SyllabicToGlide::parse(&expected).unwrap();
            let actual = stem + Initial::Single(Consonant::M);
            assert_eq!(expected, actual);
        }
    }
    #[test]
    fn test_assemblage_append_1_filling_tile() {
        {
            let stem = DecoratedWord::parse_fragment("imj").unwrap();
            let stem = NuclearToOnset::parse(&stem).unwrap();

            let expected = DecoratedWord::parse_fragment("imu").unwrap();
            let expected = NuclearToNuclear::parse(&expected).unwrap();
            let actual = stem + Vowel::U;
            assert_eq!(expected, actual);
        }
    }
    #[test]
    fn test_assemblage_append_dont_deduplicate_within_morpheme() {
        let stem = DecoratedWord::parse_fragment("vav").unwrap();
        let stem = AStem::parse(&stem).unwrap();
        let suffix = DecoratedWord::parse_fragment("el").unwrap();
        let suffix = SimpleSuffix::parse(&suffix).unwrap();

        let expected = DecoratedWord::parse_fragment("vavel").unwrap();
        let expected = Sylls::parse(&expected).unwrap();
        let actual = stem + &suffix;
        assert_eq!(expected, actual);
    }
    #[test]
    fn test_assemblage_append_correct_bridge_elaine() {
        let stem = DecoratedWord::parse_fragment("cet").unwrap();
        let stem = Sylls::parse(&stem).unwrap();
        let suffix = DecoratedWord::parse_fragment("nen").unwrap();
        let suffix = Sylls::parse(&suffix).unwrap();

        let expected = DecoratedWord::parse_fragment("cennen").unwrap();
        let expected = Sylls::parse(&expected).unwrap();
        let actual = stem + &suffix;
        assert_eq!(expected, actual);
    }
    #[test]
    fn test_assemblage_append_correct_bridge_elaine_ng() {
        let stem = DecoratedWord::parse_fragment("cec").unwrap();
        let stem = Sylls::parse(&stem).unwrap();
        let suffix = DecoratedWord::parse_fragment("mar").unwrap();
        let suffix = Sylls::parse(&suffix).unwrap();

        let expected = DecoratedWord::parse_fragment("cjormar").unwrap();
        let expected = Sylls::parse(&expected).unwrap();
        let actual = stem + &suffix;
        assert_eq!(expected, actual);
    }
    #[test]
    fn test_assemblage_append_correct_bridge_noncanonical() {
        let stem = DecoratedWord::parse_fragment("vet").unwrap();
        let stem = Sylls::parse(&stem).unwrap();
        let suffix = DecoratedWord::parse_fragment("ra").unwrap();
        let suffix = Sylls::parse(&suffix).unwrap();

        let actual = stem + &suffix;
        assert_eq!(actual.tiles[0].3, SimpleCoda::Empty);
        assert_eq!(actual.tiles[1].0, Initial::R(Epf::T));
    }
    #[test]
    fn test_assemblage_append_deduplication_before_canonicalization() {
        {
            let stem = DecoratedWord::parse_fragment("reþ").unwrap();
            let stem = Sylls::parse(&stem).unwrap();
            let suffix = DecoratedWord::parse_fragment("eþna").unwrap();
            let suffix = Sylls::parse(&suffix).unwrap();

            let expected = DecoratedWord::parse_fragment("reþeþna").unwrap();
            let expected = Sylls::parse(&expected).unwrap();
            let actual = stem + &suffix;
            assert_eq!(expected, actual);
        }
        {
            let stem = DecoratedWord::parse_fragment("reþ").unwrap();
            let stem = AStem::parse(&stem).unwrap();
            let suffix = DecoratedWord::parse_fragment("eþna").unwrap();
            let suffix = SimpleSuffix::parse(&suffix).unwrap();

            let expected = DecoratedWord::parse_fragment("reteþna").unwrap();
            let expected = Sylls::parse(&expected).unwrap();
            let actual = stem + &suffix;
            assert_eq!(expected, actual);
        }
    }

    // TODO: rewrite this test
    // #[test]
    // fn phrase_manipulation() {
    //     let mut phrase = Phrase::parse("+merlan #flirora").unwrap();
    //     assert_eq!(phrase.to_string(), "+merlan #flirora");
    //     assert_eq!(phrase.words()[0], DecoratedWord::parse("+merlan").unwrap());

    //     phrase.append(Phrase::of(DecoratedWord::parse("vvv").unwrap()));
    //     assert_eq!(phrase.words()[0], DecoratedWord::parse("+merlan").unwrap());
    //     assert_eq!(phrase.words()[1], DecoratedWord::parse("#flirora").unwrap());
    //     assert_eq!(phrase.words()[2], DecoratedWord::parse("vvv").unwrap());
    //     assert_eq!(phrase.to_string(), "+merlan #flirora vvv");

    //     let phrase2 = Phrase::Append(vec![])
    //         .appended(Phrase::of(DecoratedWord::parse("+merlan").unwrap()))
    //         .appended(Phrase::of(DecoratedWord::parse("#flirora").unwrap()))
    //         .appended(Phrase::of(DecoratedWord::parse("vvv").unwrap()));
    //     assert_eq!(phrase, phrase2);

    //     let long_word_phrase =
    //         Phrase::parse("lalalalalalalalalalalala lalalalalalalalalalalala").unwrap();
    //     let long_word_phrase_2 = long_word_phrase.clone();
    //     assert_eq!(long_word_phrase, long_word_phrase_2);
    // }
}
